import java.io.Serializable;

public class DHCPOption implements Cloneable, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 695987011122267867L;
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		DHCPOption clonObj = (DHCPOption)super.clone();
		return clonObj;
	}
	
	
	
	public boolean equals(DHCPOption obj) {
	    return (this == obj);
	}
	public String getOption() {
		return Option;
	}
	public void setOption(String option) {
		Option = option;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDataType() {
		return DataType;
	}
	public void setDataType(String dataType) {
		DataType = dataType;
	}
	public String getData() {
		return Data;
	}
	public void setData(String data) {
		Data = data;
	}
	public String getSize() {
		return Size;
	}
	public void setSize(String size) {
		Size = size;
	}
	private String Option="";
	private String Name="";
	private String DataType="";
	private String Data="";
	private String Size="";

	
}
