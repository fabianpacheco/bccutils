
import java.io.Serializable;

/**
 * Object serializable and clonable contains all information about the rules IPv6.
 * @author Fabian Pacheco
 * @version: 31/12/2020
 *  **/
public class RulesIPv6 implements Cloneable, Serializable  {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2540950523561193236L;


	private String Criteria="";


	private String Description="";
	private String Disabled="";

	private String EndIpAddress="";

	private String Lookupkey="";

	private String Name="";

	private String NonSequential="";

	private String PreferredLifeTime="";

	private String RapidCommit="";

	private String StartiIpAddress="";

	private String ValidlifeTime="";

	public void clean(){
		Name="";
		Description="";
		Lookupkey="";
		Disabled="";
		Criteria="";
		EndIpAddress="";
		NonSequential="";
		PreferredLifeTime="";
		RapidCommit="";
		StartiIpAddress="";
		ValidlifeTime="";
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		RulesIPv6 clonObj = (RulesIPv6)super.clone();
		return clonObj;
	}

	public String getCriteria() {
		return Criteria;
	}

	public String getDescription() {
		return Description;
	}

	public String getDisabled() {
		return Disabled;
	}

	public String getEndIpAddress() {
		return EndIpAddress;
	}

	public String getLookupkey() {
		return Lookupkey;
	}

	public String getName() {
		return Name;
	}

	public String getNonSequential() {
		return NonSequential;
	}

	public String getPreferredLifeTime() {
		return PreferredLifeTime;
	}

	public String getRapidCommit() {
		return RapidCommit;
	}

	public String getStartiIpAddress() {
		return StartiIpAddress;
	}

	public String getValidlifeTime() {
		return ValidlifeTime;
	}
	public void setCriteria(String criteria) {
		Criteria = criteria;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public void setDisabled(String disabled) {
		Disabled = disabled;
	}
	public void setEndIpAddress(String endIpAddress) {
		EndIpAddress = endIpAddress;
	}
	public void setLookupkey(String lookupkey) {
		Lookupkey = lookupkey;
	}
	public void setName(String name) {
		Name = name;
	}
	public void setNonSequential(String nonSequential) {
		NonSequential = nonSequential;
	}
	public void setPreferredLifeTime(String preferredLifeTime) {
		PreferredLifeTime = preferredLifeTime;
	}
	public void setRapidCommit(String rapidCommit) {
		RapidCommit = rapidCommit;
	}
	public void setStartiIpAddress(String startiIpAddress) {
		StartiIpAddress = startiIpAddress;
	}
	public void setValidlifeTime(String validlifeTime) {
		ValidlifeTime = validlifeTime;
	}


}

