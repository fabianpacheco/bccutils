import java.io.Serializable;

public class ClientClassGroup implements Cloneable, Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 4324161988218436653L;
	
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		ClientClassGroup clonObj = (ClientClassGroup)super.clone();
		return clonObj;
	}
	
	
	
	public boolean equals(ClientClassGroup obj) {
	    return (this == obj);
	}
	
	private String name="";
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getLookup() {
		return lookup;
	}



	public void setLookup(String lookup) {
		this.lookup = lookup;
	}



	public String getAcclokupkey() {
		return acclokupkey;
	}



	public void setAcclokupkey(String acclokupkey) {
		this.acclokupkey = acclokupkey;
	}



	public String getClientclass() {
		return clientclass;
	}



	public void setClientclass(String clientclass) {
		this.clientclass = clientclass;
	}



	public String getManagmentLookup() {
		return managmentLookup;
	}



	public void setManagmentLookup(String managmentLookup) {
		this.managmentLookup = managmentLookup;
	}



	public String getMoveDuplicates() {
		return MoveDuplicates;
	}



	public void setMoveDuplicates(String moveDuplicates) {
		MoveDuplicates = moveDuplicates;
	}



	public String getQos() {
		return Qos;
	}



	public void setQos(String qos) {
		Qos = qos;
	}



	public String getTemplateLookupKey() {
		return TemplateLookupKey;
	}



	public void setTemplateLookupKey(String templateLookupKey) {
		TemplateLookupKey = templateLookupKey;
	}

	private String description="";
	private String lookup="";
	private String acclokupkey="";
	private String clientclass="";
	private String managmentLookup="";
	private String MoveDuplicates="";
	private String Qos="";
	private String TemplateLookupKey="";
	
}
