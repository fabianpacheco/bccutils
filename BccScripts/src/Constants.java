/**
 * This class contains all constants.
 * @author Fabian Pacheco
 * @version: 31/12/2020
 *  **/
public class Constants {
	public static final int ERROR=0;
    public static final int NO_ERROR=1;
    public static final String LOGFILENAME="./BccUtils.log";
    public static final String LOGON="D";
    public static final int LOGSIZE=10000000; 
    public static final int PARAMSPERMIT=7;
    public static final String EXPORT="E";
    public static final String MAN="0";
    public static final String RULES="1"; 
    public static final String ROUTES="2";
    public static final String STATICIP="3";
    public static final String CLIENTCLASS="4";
    public static final String RULESV6="5";
    public static final String TEMPLATES="6";
    public static final String DFS="7";
    public static final String DC="8";
    public static final String CCG="9";
    public static final String CNS="10";
    public static final String IMPORT="I";
    public static final String READ="R";
    public static final String REPORT="RP";
    public static final String CCStatics="CCS";
    public static final String CCSDetails="CCD";
    public static final String COMPARE="CO";
    public static final int NODATA=0;
    public static final int DELETEALL=3;
    public static final int COMPLEMENT=4;
    public static final int OVERWRITE=2;
    public static final int ADD=1;
    public static final String FILECOMMANDAUX="BccAux.txt";
    public static final String FILELOGAUX="BccAux.log";
    public static final String ipZero="0.0.0.0";
    public static final Long[] SUBNET_MASK = new Long[]{4294934528L, 4294950912L, 4294959104L, 4294963200L, 4294965248L, 4294966272L, 4294966784L, 4294967040L, 4294967168L, 4294967232L, 4294967264L, 4294967280L, 4294967288L, 4294967292L, 4294967294L, 4294967295L};
}
