import java.io.Serializable;

public class DeviceClassifier implements Cloneable, Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2891905021669842594L;
	public DeviceClassifier() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		DeviceClassifier clonObj = (DeviceClassifier)super.clone();
		return clonObj;
	}
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getLookupKey() {
		return LookupKey;
	}
	public void setLookupKey(String lookupKey) {
		LookupKey = lookupKey;
	}
	public String getAclLookupKey() {
		return AclLookupKey;
	}
	public void setAclLookupKey(String aclLookupKey) {
		AclLookupKey = aclLookupKey;
	}
	public String getCopyIdTacs() {
		return CopyIdTacs;
	}
	public void setCopyIdTacs(String copyIdTacs) {
		CopyIdTacs = copyIdTacs;
	}
	public String getCriteria() {
		return Criteria;
	}
	public void setCriteria(String criteria) {
		Criteria = criteria;
	}
	public String getDocsisCompliant() {
		return DocsisCompliant;
	}
	public void setDocsisCompliant(String docsisCompliant) {
		DocsisCompliant = docsisCompliant;
	}
	public String getEnableCsClustering() {
		return EnableCsClustering;
	}
	public void setEnableCsClustering(String enableCsClustering) {
		EnableCsClustering = enableCsClustering;
	}
	public String getManagmentLookUpkey() {
		return ManagmentLookUpkey;
	}
	public void setManagmentLookUpkey(String managmentLookUpkey) {
		ManagmentLookUpkey = managmentLookUpkey;
	}
	public String getPacketcableCompliant() {
		return PacketcableCompliant;
	}
	public void setPacketcableCompliant(String packetcableCompliant) {
		PacketcableCompliant = packetcableCompliant;
	}
	private String Name="";
	private String Description="";
	private String LookupKey="";
	private String AclLookupKey="";
	private String CopyIdTacs="";
	private String Criteria="";
	private String DocsisCompliant="";
	private String EnableCsClustering="";
	private String ManagmentLookUpkey="";
	private String PacketcableCompliant="";
}
