import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;


import com.googlecode.ipv6.*;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.net.util.SubnetUtils;
import org.apache.commons.net.util.SubnetUtils.SubnetInfo;

@SuppressWarnings("deprecation")
public class Main {
	private static String ipBcc="";
	private static String usrBcc="";
	private static String passBcc="";
	private static String ipBcc2="";
	private static String usrBcc2="";
	private static String passBcc2="";
	private static String Option="";
	private static String TypeOption="";
	private static String logOption="";
	private static String ReportType="";
	private static String FileNameSrc="";
	private static String FileNameDst="";
	private static String Directory="";
	private static int Action=0;
	private static String FilterFileName="";
	private static boolean nameOption=false;
	private static boolean templateOpt=false;
	private static boolean ssh=false;

	/**
	 * This function add Gateway in BCC through execution files.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String nameGw: variable contains the gateway name to add
	 * @param String CmdAdd: this string contains all commands to include in the file.
	 * @param List<String> list: this arrray contains all commands 
	 *  **/
	private static void addGwBCC(String ip, String user, String pass,String nameGw, String CmdAdd)
			throws IOException {
		
		try {
		/***Creating File sh for execute file with commands**/
		File file = new File("bccutils.sh");
		file.createNewFile();
		Set<PosixFilePermission> perms = new HashSet<>();
		perms.add(PosixFilePermission.OWNER_EXECUTE);
		perms.add(PosixFilePermission.OWNER_WRITE);
		Files.setPosixFilePermissions(file.toPath(), perms);
		PrintWriter fl = new PrintWriter(file);
		fl.println("ipcli -n " + user + " -p " + pass + " -s " + ip + " < "+ Constants.FILECOMMANDAUX +" > " + Constants.FILELOGAUX);
		/**Creating File with command**/
		File fileAux = new File(Constants.FILECOMMANDAUX);
		PrintWriter fl2 = new PrintWriter(fileAux);
		fl2.println("modify ROUTINGELEMENT " + '"' +  nameGw + '"');
		fl2.println(CmdAdd);
		LogWriter.getLogger().info(CmdAdd);
		fl2.println("exit");
		fl2.close();
		fl.close();
		
		ProcessBuilder builder;
		Process proc;
		builder = new ProcessBuilder(file.getAbsolutePath());
		proc = builder.start();  
		proc.waitFor();
		FileReader fr=new FileReader(Constants.FILELOGAUX);
		BufferedReader bro = new BufferedReader(fr);
		String valor=bro.readLine();
		String logout="";
		while((valor=bro.readLine())!=null)
        	logout=logout+valor;
        LogWriter.getLogger().info(logout);
        fr.close();
        file.delete();
        fileAux.delete();
        File fileAux2 = new File(Constants.FILELOGAUX);
        fileAux2.delete();
		}catch(Exception e) {
			System.out.println("Error adding gateway list to routing element " + nameGw  + " " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * This function add Route in BCC
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String auxString: variable contains the rule name to delete
	 * @param List<String> CmdAdd: this arrray contains all information about the route to add
	 * @param List<String> list: this arrray contains all commands 
	 *  **/
	private static void addRouteBCC(String ip, String user, String pass, List<String> CmdAdd, List<String> list)
			throws IOException {
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("ADD");
		list.add("ROUTINGELEMENT");
		for (int n=0; n < CmdAdd.size(); n++) {
			list.add(CmdAdd.get(n));
		} 
		LogWriter.getLogger().info(String.valueOf(list));
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
		list.clear();
	}	

	/**
	 * This function add Rule IPV6 in BCC
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String auxString: variable contains the rule name to delete
	 * @param List<String> CmdAdd: this arrray contains all information about the rule to add
	 * @param List<String> list: this arrray contains all commands 
	 *  **/
	private static void addRuleV6BCC(String ip, String user, String pass, List<String> CmdAdd, List<String> list)
			throws IOException {
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("ADD");
		list.add("RULEV6");
		for (int n=0; n < CmdAdd.size(); n++) {
			list.add(CmdAdd.get(n));
		} 
		LogWriter.getLogger().info(String.valueOf(list));
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
		list.clear();
	}
/**
 * This function add Rule in BCC
 * @author Fabian Pacheco
 * @version: 31/12/2020
 * @param String ip : Is the BCC server IP and it is used to connect to BCC
 * @param String user: Is the BCC server user and it is used to connect to BCC
 * @param String pass: Is the BCC server Password and it is used to connect to BCC
 * @param String auxString: variable contains the rule name to delete
 * @param List<String> CmdAdd: this arrray contains all information about the rule to add
 * @param List<String> list: this arrray contains all commands 
 *  **/
private static void addRuleBCC(String ip, String user, String pass, List<String> CmdAdd, List<String> list)
		throws IOException {
	ProcessBuilder builder;
	Process proc;
	InputStream is2;
	BufferedReader br2;
	list.add("ipcli");
	list.add("-n");
	list.add(user);
	list.add("-p");
	list.add(pass);
	list.add( "-s");
	list.add(ip);
	list.add("ADD");
	list.add("RULE");
	for (int n=0; n < CmdAdd.size(); n++) {
		list.add(CmdAdd.get(n));
	} 
	LogWriter.getLogger().info(String.valueOf(list));
	builder = new ProcessBuilder(list);
	proc = builder.start();                	
	is2 = proc.getInputStream();
	br2 = new BufferedReader (new InputStreamReader (is2));
	LogWriter.getLogger().info( br2.readLine());
	list.clear();
}

	/**
	 * This function add Static Address in BCC
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String auxString: variable contains the rule name to delete
	 * @param List<String> CmdAdd: this arrray contains all information about the rule to add
	 * @param List<String> list: this arrray contains all commands 
	 *  **/
	private static void addStaticAddressBCC(String ip, String user, String pass, List<String> CmdAdd, List<String> list)
			throws IOException {
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("ADD");
		list.add("STATICADDRESS");
		for (int n=0; n < CmdAdd.size(); n++) {
			list.add(CmdAdd.get(n));
		} 
		LogWriter.getLogger().info(String.valueOf(list));
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
		list.clear();
	}
	
	/**
	 * This function add Rule in BCC
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String auxString: variable contains the rule name to delete
	 * @param List<String> CmdAdd: this arrray contains all information about the rule to add
	 * @param List<String> list: this arrray contains all commands 
	 *  **/
	private static void addTemplateBCC(String ip, String user, String pass, List<String> CmdAdd, List<String> list)
			throws IOException {
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("ADD");
		list.add("TEMPLATE");
		for (int n=0; n < CmdAdd.size(); n++) {
			list.add(CmdAdd.get(n));
		} 
		LogWriter.getLogger().info(String.valueOf(list));
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
		list.clear();
	}
	
	/**
	 * This function delete Rule in BCC
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String auxString: variable contains the rule name to delete
	 *  **/
	private static void delRuleBCC(String ip, String user, String pass, String auxString) throws IOException {
		LogWriter.getLogger().info("DELETE RULE " + auxString) ;
		List<String> list = new ArrayList<>();
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("DELETE");
		list.add("RULE");
		list.add(auxString);
		ProcessBuilder builder = new ProcessBuilder(list);
		Process proc = builder.start();                	
		InputStream is2 = proc.getInputStream();
		BufferedReader br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
	}
	
	/**
	 * This function delete Rule in BCC
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String auxString: variable contains the rule name to delete
	 *  **/
	private static void delRuleV6BCC(String ip, String user, String pass, String auxString) throws IOException {
		LogWriter.getLogger().info("DELETE RULE " + auxString) ;
		List<String> list = new ArrayList<>();
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("DELETE");
		list.add("RULEV6");
		list.add(auxString);
		ProcessBuilder builder = new ProcessBuilder(list);
		Process proc = builder.start();                	
		InputStream is2 = proc.getInputStream();
		BufferedReader br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
	}

	/**
	 * This function delete Route in BCC
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String auxString: variable contains the rule name to delete
	 * @param List<String> CmdAdd: this arrray contains name and lookupkey to delete
	 *  **/
	private static void delRuoteBCC(String ip, String user, String pass, String auxString,List<String> CmdAdd) throws IOException {
		LogWriter.getLogger().info("DELETE ROUTINGELEMENT " + auxString) ;
		List<String> list = new ArrayList<>();
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("DELETE");
		list.add("ROUTINGELEMENT");
		for (int n=0; n < CmdAdd.size(); n++) {
			list.add(CmdAdd.get(n));
		} 
		list.add(auxString);
		LogWriter.getLogger().info(String.valueOf(list));
		ProcessBuilder builder = new ProcessBuilder(list);
		Process proc = builder.start();                	
		InputStream is2 = proc.getInputStream();
		BufferedReader br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
	}
	
	/**
	 * This function delete Static Address in BCC
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String auxString: variable contains the static address name to delete
	 *  **/
	private static void delStaticAddBCC(String ip, String user, String pass, String auxString,List<String> CmdAdd) throws IOException {
		LogWriter.getLogger().info("DELETE STATICADDRESS " + auxString) ;
		List<String> list = new ArrayList<>();
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("DELETE");
		list.add("STATICADDRESS");
		for (int n=0; n < CmdAdd.size(); n++) {
			list.add(CmdAdd.get(n));
		} 
		list.add(auxString);
		ProcessBuilder builder = new ProcessBuilder(list);
		Process proc = builder.start();                	
		InputStream is2 = proc.getInputStream();
		BufferedReader br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
	}
	
	/**
	 * This function delete Templates in BCC
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String auxString: variable contains the template name to delete
	 *  **/
	private static void delTemplateBCC(String ip, String user, String pass, String auxString,List<String> CmdAdd) throws IOException {
		LogWriter.getLogger().info("DELETE TEMPLATE " + auxString) ;
		List<String> list = new ArrayList<>();
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("DELETE");
		list.add("TEMPLATE");
		for (int n=0; n < CmdAdd.size(); n++) {
			list.add(CmdAdd.get(n));
		} 
		list.add(auxString);
		ProcessBuilder builder = new ProcessBuilder(list);
		Process proc = builder.start();                	
		InputStream is2 = proc.getInputStream();
		BufferedReader br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
	}

	/**
	 * This function get the client classes create file with the objects read.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileDest: Is the File Name where the process save the information.
	 * @return Error or not 
	 *  **/
	public static int getClientClass(String ip, String user, String pass, String FileName)  {
		ArrayList<ClientClass> ccs = new ArrayList<ClientClass>();
		FileWriter fileW = null;
		PrintWriter pw = null;
		String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list CLIENTCLASS ";
		try{		    
			getObjectClientClass(ccs,  command);
            Iterator<ClientClass> itrCC = ccs.iterator();
            List<String> list = new ArrayList<>();
            while(itrCC.hasNext()){
            	ClientClass cc = itrCC.next();
            	ProcessBuilder builder;
        		Process proc;
        		InputStream is2;
        		BufferedReader br2;
        		list.clear();
        		list.add("ipcli");
        		list.add("-n");
        		list.add(user);
        		list.add("-p");
        		list.add(pass);
        		list.add( "-s");
        		list.add(ip);
        		list.add("show");
        		list.add("CLIENTCLASS");
        		list.add(cc.getName());
        		builder = new ProcessBuilder(list);
        		proc = builder.start();                	
        		is2 = proc.getInputStream();
        		br2 = new BufferedReader (new InputStreamReader (is2));
        		String aux2 = br2.readLine();
        		LogWriter.getLogger().info( aux2);               
                while (aux2!=null){
                	StringTokenizer tokens2=new StringTokenizer(aux2,":");
                	if (!(!tokens2.hasMoreTokens()&&(aux2.length()==0))) {
                    	String option2 = tokens2.nextToken();
    	                String value = tokens2.nextToken();
    	                switch(option2.trim()){
    		    			case "CLIENTCLASSENTRY_SIZE":  cc.setNoDevices(value.trim());break; 
    	                }
                    }
                    aux2 = br2.readLine();
                }	
            }
            fileW = new FileWriter(FileName);
            pw = new PrintWriter(fileW);
            pw.println("ClientClassName|ClientClassType|ClientClassNoDevices");
            for (int i=0; i < ccs.size();i++) {
            	pw.println(ccs.get(i).getName() +"|" + ccs.get(i).getDateType() + "|" + ccs.get(i).getNoDevices() );
            }
            fileW.close();
            return Constants.NO_ERROR;
            
		}catch(Exception e) {
			System.out.println("EXCEPTION: " + e.getMessage());
			 e.printStackTrace();
			return Constants.ERROR;
		}
		
	}
	
	/**
	 * This function get the client classes create file with the objects read.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileDest: Is the File Name where the process save the information.
	 * @return Error or not 
	 *  **/
	public static int getClientClassDetail(String ip, String user, String pass, String FileName)  {
		ArrayList<ClientClass> ccs = new ArrayList<ClientClass>();
		FileWriter fileW = null;
		PrintWriter pw = null;
		String outputString="";
		String CCoutputString="";
		int LongOption=0;
		String DinText="";
		String Option67="";
		String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list CLIENTCLASS SORTBY NAME ";
		//String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip +" show clientclass lookupkey=49230";
		
		try{	
			fileW = new FileWriter(FileName);
            pw = new PrintWriter(fileW);
            pw.println("PRIORITY,CLIENCLASS,MEMBERS,CNS ID,CNS NAME,CNS PRIORITY,CRITERIA,DHCP OPTION,DYNTEXT,LENGTH");
			getObjectClientClass(ccs,  command);
            for (int i =0; i< ccs.size(); i++) {
            	CCoutputString=ccs.get(i).getPriority();
            	CCoutputString=CCoutputString+","+ccs.get(i).getName();
            	CCoutputString=CCoutputString+","+ccs.get(i).getClientDataType();
            	//CCoutputString=CCoutputString+"|"+String.valueOf(ccs.get(i).getClienClassEntry());
				
				ArrayList<CNS> ruleta = new ArrayList<CNS>();
				ruleta=getCNS(ip, user, pass, ccs.get(i).getLookup());
				for(int j=0; j<  ruleta.size();j++) {
					/*if (j==1) {
						outputString=",,,";
					}*/
					outputString=CCoutputString+","+ ruleta.get(j).getLookupkey();
					outputString=outputString+","+ ruleta.get(j).getName();
					outputString=outputString+","+  ruleta.get(j).getPriority();
					if (!"".equals(ruleta.get(j).getName())){
						String commanddcs="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " show DEVICECLASSIFIER lookupkey=" + ruleta.get(j).getLookupkey().trim();
						ArrayList<DeviceClassifier> dcs = new ArrayList<DeviceClassifier>();
						getObjectDCS(dcs, commanddcs);
						if (dcs.size()>0) {
							outputString=outputString+",\""+ dcs.get(0).getCriteria()+"\"";
						}
						ArrayList<DHCPOption> options = new ArrayList<DHCPOption>();
		    	    	options=getDHCPOptions(ip, user, pass, ccs.get(i).getLookup(),ruleta.get(j).getLookupkey().trim() );
	        			for(int k=0; k< options.size();k++) {
	        				outputString=outputString+",\""+options.get(k).getData()+"\"";
	        				if (options.get(k).getData().indexOf("DYNTXT$(")>0) {
	        					Option67=options.get(k).getData();
	        					DinText=options.get(k).getData().substring(options.get(k).getData().indexOf("DYNTXT$("),options.get(k).getData().length()).replace("DYNTXT$(", "").replace(")", "");
	        					LongOption=DinText.length();
	        					outputString=outputString+",\""+ DinText+"\","+LongOption;
	        					if (LongOption>10) {
	        						outputString=outputString+","+"Modify clientclass \"" + ccs.get(i).getName() + "\" " + " cns lookupkey= " +  ruleta.get(j).getLookupkey() + " set DHCPOPTION 67 DATA "+ 
	        								options.get(k).getData().substring(0,options.get(k).getData().indexOf("DYNTXT$(")) + "DYNTXT$(\"" + ",\")" ; 
	        					}
	        				}
	        				
	        				/**
	        				System.out.println("OPTNUM" + options.get(k).getOption());
	        				System.out.println("NAME" + options.get(k).getName());
	        				System.out.println("DATATYPE" + options.get(k).getDataType());
	        				System.out.println("DATA" + options.get(k).getData());
	        				System.out.println("SIZE" + options.get(k).getSize());
	        				**/
		            	}
					}
					pw.println(outputString);
					outputString="";
					LongOption=0;
	            }
				ArrayList<DHCPOption> options = new ArrayList<DHCPOption>();
    	    	options=getDHCPOptionsCC(ip, user, pass,  ccs.get(i).getLookup() );
    	    	CCoutputString=CCoutputString+",,,,,";
    			for(int k=0; k< options.size();k++) {
    				outputString = outputString + options.get(k).getOption()+ "-" + "\""+options.get(k).getData()+"\"";
    				if (options.get(k).getData().indexOf("DYNTXT$(")>0) {
    					Option67=options.get(k).getData();
    					DinText=options.get(k).getData().substring(options.get(k).getData().indexOf("DYNTXT$("),options.get(k).getData().length()).replace("DYNTXT$(", "").replace(")", "");
    					LongOption=DinText.length();
    					outputString=outputString+",\""+ DinText+"\","+LongOption;
    					if (LongOption>10) {
    						outputString=outputString+","+"Modify clientclass \"" + ccs.get(i).getName() + "\" " +  " set DHCPOPTION 67 DATA "+ 
    								options.get(k).getData().substring(0,options.get(k).getData().indexOf("DYNTXT$(")) + "DYNTXT$(\"" + ",\")" ;  
    					}
    				}
    				/**
    				System.out.println("OPTNUM" + options.get(k).getOption());
    				System.out.println("NAME" + options.get(k).getName());
    				System.out.println("DATATYPE" + options.get(k).getDataType());
    				System.out.println("DATA" + options.get(k).getData());
    				System.out.println("SIZE" + options.get(k).getSize());
    				**/
            	}
    			if (options.size()>0)
    				pw.println(CCoutputString + outputString);
    			outputString="";
				CCoutputString="";
				
            }
            pw.close();
            return Constants.NO_ERROR;
 
		}catch(Exception e) {
			System.out.println("EXCEPTION: " + e.getMessage());
			 e.printStackTrace();
			return Constants.ERROR;
		}	
		
	}
	
	public static int compareClientClass(String ip, String user, String pass,String ip2, String user2, String pass2, String FileName, boolean OptionDebug, boolean sshremote)  {
		ArrayList<ClientClass> ccs = new ArrayList<ClientClass>();
		ArrayList<ClientClass> ccs2 = new ArrayList<ClientClass>();
		int maxlength=0;
		int maxlengthCns=0;
		int maxlengthDhcp=0;
		OptionDebug=false;
		String command2="";
		String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list CLIENTCLASS SORTBY NAME ";
		if (sshremote)
			command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list CLIENTCLASS SORTBY NAME";
		else
			command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list CLIENTCLASS SORTBY NAME";
		try{		    
			getObjectClientClass(ccs,  command);
            getObjectClientClass(ccs2,  command2);
            
            if (ccs.equals(ccs2)) {
            	System.out.println("There is not diferences between the client classes " + ccs.size() + " 2" + ccs2.size());
            }else {
            	System.out.println("There is  diferences between the client classes " + ccs.size() + " 2" + ccs2.size());
            	if (ccs.size()<=ccs2.size()) {
            		maxlength = ccs2.size();
            	}else {
            		maxlength = ccs.size();
            	}
            	for (int i =0; i<maxlength; i++) {
            		if (!(ccs.get(i).equals(ccs2.get(i)))) {            			
            			System.out.println("There are differences between client classes. "  + ccs.get(i).getName() + " and " +  ccs2.get(i).getName());
            			Utils.CompateTwoString("NAME" , ccs.get(i).getName(), ccs2.get(i).getName(), OptionDebug);
            			Utils.CompateTwoString("DESCRIPTION" ,ccs.get(i).getDescription() , ccs2.get(i).getDescription(),OptionDebug);
            			Utils.CompateTwoString("LOOKUP" , ccs.get(i).getLookup(), ccs2.get(i).getLookup(),OptionDebug);
            			Utils.CompateTwoString("ACCLOOKUP", ccs.get(i).getAcclokupkey(),ccs2.get(i).getAcclokupkey(),OptionDebug);
            			Utils.CompateTwoString("ANTIROAMING", ccs.get(i).getAntiroaming(), ccs2.get(i).getAntiroaming(),OptionDebug );
            			Utils.CompateTwoString("ClientCLassEntry", String.valueOf(ccs.get(i).getClienClassEntry()), String.valueOf(ccs2.get(i).getClienClassEntry()),OptionDebug);
            			Utils.CompateTwoString("DataType", ccs.get(i).getClientDataType(), ccs2.get(i).getClientDataType(),OptionDebug);
            			Utils.CompateTwoString("SubDataType", ccs.get(i).getSubDataType(), ccs2.get(i).getSubDataType(),OptionDebug);
            			Utils.CompateTwoString("DDNSV4", ccs.get(i).getDdnsServer4(), ccs2.get(i).getDdnsServer4(),OptionDebug);
            			Utils.CompateTwoString("DDNSV6", ccs.get(i).getDdnsServer6(), ccs2.get(i).getDdnsServer6(),OptionDebug);
            			Utils.CompateTwoString("DEFAULT", ccs.get(i).getDefault_(), ccs2.get(i).getDefault_(),OptionDebug);
            			Utils.CompateTwoString("DEfaultPrefix", ccs.get(i).getDefaultPrefixlength(), ccs2.get(i).getDefaultPrefixlength(),OptionDebug);
            			Utils.CompateTwoString("DEfaultLimit", ccs.get(i).getDefaultIpLimit(), ccs2.get(i).getDefaultIpLimit(),OptionDebug);
            			Utils.CompateTwoString("DHCPOption", ccs.get(i).getDhcpOptionNumber(), ccs2.get(i).getDhcpOptionNumber(),OptionDebug);
            			Utils.CompateTwoString("DHCP Option Vendor", ccs.get(i).getDhcpOptionVendor(), ccs2.get(i).getDhcpOptionVendor(),OptionDebug);
            			Utils.CompateTwoString("DHCP Option VErsion",ccs.get(i).getDhcpOptionVersion(), ccs2.get(i).getDhcpOptionVersion(),OptionDebug);
            			Utils.CompateTwoString("DHCP SubOption", ccs.get(i).getDhcpSubOptionNumber(),ccs2.get(i).getDhcpSubOptionNumber(),OptionDebug);
            			Utils.CompateTwoString("EnableDns4n", ccs.get(i).getEnableDns4(), ccs2.get(i).getEnableDns4(),OptionDebug);
            			Utils.CompateTwoString("EnableDns6", ccs.get(i).getEnableDns6(), ccs2.get(i).getEnableDns6(),OptionDebug);
            			Utils.CompateTwoString("InheritDns4", ccs.get(i).getInheritDns4(), ccs2.get(i).getInheritDns4(),OptionDebug);
            			Utils.CompateTwoString("InheritDns6", ccs.get(i).getInheritDns6(), ccs2.get(i).getInheritDns6(),OptionDebug );
            			Utils.CompateTwoString("IpLimitfield", ccs.get(i).getIpLimitfield(), ccs2.get(i).getIpLimitfield(),OptionDebug);
            			Utils.CompateTwoString("ShuffleIp", ccs.get(i).getShuffleIp(),  ccs2.get(i).getShuffleIp(),OptionDebug);
            			Utils.CompateTwoString("ManagmentLookup", ccs.get(i).getManagmentLookup(), ccs2.get(i).getManagmentLookup(),OptionDebug);
            			Utils.CompateTwoString("MinimunDelegation", ccs.get(i).getMinimunDelegation(), ccs2.get(i).getMinimunDelegation(),OptionDebug);
            			Utils.CompateTwoString("NumberV6Options", ccs.get(i).getNumberV6Options(), ccs2.get(i).getNumberV6Options(),OptionDebug);
            			Utils.CompateTwoString("PerformDnnsU", ccs.get(i).getPerformDnnsUp(), ccs2.get(i).getPerformDnnsUp() ,OptionDebug);
            			Utils.CompateTwoString("PrefixlenghtSize", ccs.get(i).getPrefixlenghtSize(), ccs2.get(i).getPrefixlenghtSize() ,OptionDebug);
            			Utils.CompateTwoString("Priority", ccs.get(i).getPriority(), ccs2.get(i).getPriority(),OptionDebug);
            			Utils.CompateTwoString("SystemPriority", ccs.get(i).getSystemPriority(), ccs2.get(i).getSystemPriority(),OptionDebug);
            			Utils.CompateTwoString("Qos", ccs.get(i).getQos(), ccs2.get(i).getQos(),OptionDebug);
            			Utils.CompateTwoString("ShallPerformanceUpd", ccs.get(i).getShallPerformanceUpd(), ccs2.get(i).getShallPerformanceUpd(),OptionDebug);
            			Utils.CompateTwoString("TemplateLookUpkey", ccs.get(i).getTemplateLookUpkey(), ccs2.get(i).getTemplateLookUpkey(),OptionDebug);
            			Utils.CompateTwoString("UnprovisionedDevicess", ccs.get(i).getUnprovisionedDevicess(), ccs2.get(i).getUnprovisionedDevicess(),OptionDebug);
            			Utils.CompateTwoString("DateType",ccs.get(i).getDateType(), ccs2.get(i).getDateType() ,OptionDebug);
            			
            			ArrayList<CNS> ruleta = new ArrayList<CNS>();
    					ruleta=getCNS(ip, user, pass, ccs.get(i).getName());

    					ArrayList<CNS> ruleta2 = new ArrayList<CNS>();
    					if (sshremote)
    						ruleta2=getCNSRemote(ip2, user2, pass2, ccs2.get(i).getName());
    					else
    						ruleta2=getCNS(ip2, user2, pass2, ccs2.get(i).getName());

            			System.out.println(" Sizes CNS" + ruleta.size() + " + " + ruleta2.size());
            			//if (ruleta.size() != ruleta2.size()) {
            				if (ruleta.size()<=ruleta2.size()) {
            					maxlengthCns = ruleta2.size();
        	            	}else {
        	            		maxlengthCns = ruleta.size();
        	            	}
            				
	            			for(int j=0; j< maxlengthCns;j++) {
	            				if (!(ruleta.get(j).equals(ruleta2.get(j)))) {
	            					System.out.println("There are differences CNS . "  + ruleta.get(j).getName() + " and " +  ruleta2.get(j).getName());
	    	            			Utils.CompateTwoString("LOOKUPKEY" , ruleta.get(j).getLookupkey(), ruleta2.get(j).getLookupkey(), false);
	    	            			Utils.CompateTwoString("NAME" , ruleta.get(j).getName(), ruleta2.get(j).getName(), false);
	    	            			Utils.CompateTwoString("PRIORITY" , ruleta.get(j).getPriority(), ruleta2.get(j).getPriority(), false);
	    	            			Utils.CompateTwoString("NUMBER_OF_V4_OPTIONS" , ruleta.get(j).getNumberOptionsv4(), ruleta2.get(j).getNumberOptionsv4(), false);
	    	            			Utils.CompateTwoString("ENABLEDNSV4" , ruleta.get(j).getEnableDNSv4(), ruleta2.get(j).getEnableDNSv4(), false);
	    	            			Utils.CompateTwoString("DDNSSERVERV4" , ruleta.get(j).getDDNSv4(), ruleta2.get(j).getDDNSv4(), false);
	    	            			Utils.CompateTwoString("INHERITDNSSETTINGSV4" , ruleta.get(j).getInheritDnsSetinv4(), ruleta2.get(j).getInheritDnsSetinv4(), false);
	    	            			Utils.CompateTwoString("NUMBER_OF_V6_OPTIONS" , ruleta.get(j).getNumberOptionsv6(), ruleta2.get(j).getNumberOptionsv6(), false);
	    	            			Utils.CompateTwoString("ENABLEDNSV6" , ruleta.get(j).getEnableDNSv6(), ruleta2.get(j).getEnableDNSv6(), false);
	    	            			Utils.CompateTwoString("DDNSSERVERV6" , ruleta.get(j).getDDNSv6(), ruleta2.get(j).getDDNSv6(), false);
	    	            			Utils.CompateTwoString("INHERITDNSSETTINGSV6" , ruleta.get(j).getInheritDnsSetinv6(), ruleta2.get(j).getInheritDnsSetinv6(), false);
	    	            			Utils.CompateTwoString("PERFORMAAAADNSUPDATES" , ruleta.get(j).getPerformanceUpdates(), ruleta2.get(j).getPerformanceUpdates(), false);
	    	            			Utils.CompateTwoString("TEMPLATE" , ruleta.get(j).getTemplate(), ruleta2.get(j).getTemplate(), false);
	    	            			
	    	            			ArrayList<DHCPOption> options = new ArrayList<DHCPOption>();
	    	    					options=getDHCPOptions(ip, user, pass, ccs.get(i).getName(),ruleta.get(j).getName() );

	    	    					ArrayList<DHCPOption> options2 = new ArrayList<DHCPOption>();
	    	    					if (sshremote)
	    	    						options2=getDHCPOptionsRemote(ip2, user2, pass2, ccs2.get(i).getName(),ruleta.get(j).getName());
	    	    					else
	    	    						options2=getDHCPOptions(ip2, user2, pass2, ccs2.get(i).getName(),ruleta.get(j).getName());

	    	            			System.out.println(" Sizes DHCP" + options.size() + " + " + options2.size());
	    	            			//if (ruleta.size() != ruleta2.size()) {
	    	            				if (options.size()<=options2.size()) {
	    	            					maxlengthDhcp = options2.size();
	    	        	            	}else {
	    	        	            		maxlengthDhcp = options.size();
	    	        	            	}
	    	            				
	    		            			for(int k=0; k< maxlengthDhcp;k++) {
	    		            				if (!(options.get(k).equals(options2.get(k)))) {
	    		            					System.out.println("There are differences DHCP Options . "  + options.get(k).getName() + " and " +  options2.get(k).getName());
	    		    	            			Utils.CompateTwoString("OPTNUM" , options.get(k).getOption(), options2.get(k).getOption(), false);
	    		    	            			Utils.CompateTwoString("NAME" , options.get(k).getName(), options2.get(k).getName(), false);
	    		    	            			Utils.CompateTwoString("DATATYPE" , options.get(k).getDataType(), options2.get(k).getDataType(), false);
	    		    	            			Utils.CompateTwoString("DATA" , options.get(k).getData(), options2.get(k).getData(), false);
	    		    	            			Utils.CompateTwoString("SIZE" , options.get(k).getSize(), options2.get(k).getSize(), false);
	    		    	            			
	    		            				}
	    			                	}
	            				}
		                	}
            		}	
            	}
            }
            
            
            return Constants.NO_ERROR;
            
		}catch(Exception e) {
			System.out.println("EXCEPTION: " + e.getMessage());
			 e.printStackTrace();
			return Constants.ERROR;
		}
	}
	
	/**
	 * This function get the rules and create file with the objects read.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileDest: Is the File Name where the process save the information.
	 * @param String CmdAdd: this string contains all commands to include in the file.
	 * @param String Debug: If the flag Debug is turn on so the process print extra information.
	 * @return Error or not 
	 *  **/
	public static int get_function_rules(String ip, String user, String pass, String FileDest, String Debug)  {
		ArrayList<Rules> rules = new ArrayList<Rules>();
		ArrayList<String> filter = new ArrayList<String>();
		
		String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULE ";
		try{		    
			getObjectRules(rules,  command);
            Iterator<Rules> itrRules = rules.iterator();
            while(itrRules.hasNext()){
            	Rules rulet = itrRules.next();
            	Iterator<Rules> itrRulesSearch = rules.iterator();
            	while(itrRulesSearch.hasNext()) {
            		Rules ruletCopy = itrRulesSearch.next();
            		if (ruletCopy.getLookupkey().equals(rulet.getParentlookupkey())) {
            			rulet.setParentName(ruletCopy.getName());
            		}
            	}
            	
            }
            WriteDiskObjetc WDO = new WriteDiskObjetc();
            if (WDO.WriteObjectListRules(rules,FileDest)==Constants.ERROR) {
            	System.out.println("Error write to disk");
            	return Constants.ERROR;
            }
            if (Debug.equals(Constants.LOGON)) {
            	ArrayList<Rules> rulesAux = new ArrayList<Rules>();
	            ReadDiskObject RDO = new ReadDiskObject();
	            rulesAux = RDO.getObjecRulestDisk(FileDest,filter);
	            if (rulesAux.size()> 0) {
		            Iterator<Rules> itrRulesAux = rulesAux.iterator();
		            while(itrRulesAux.hasNext()){
		            	Rules ruleta = itrRulesAux.next();
		            	System.out.println(ruleta.getName() + " - "  + ruleta.getParentName() + " - "+ ruleta.getIpfrom() 
		            	+" - "+  ruleta.getIpto());
		            	
		            }
	            }else {
	            	System.out.println("Error to obtain object or the file does not contain information");
	            }
            }
            System.out.println("Exporting " + rules.size() + " rules IPV4 to File " + FileDest);
            return Constants.NO_ERROR;
            
		}catch(Exception e) {
			System.out.println("EXCEPTION: " + e.getMessage());
			 e.printStackTrace();
			return Constants.ERROR;
		}
		
	}
	
	
	public static int compare_function_rules(String ip, String user, String pass,String ip2, String user2, String pass2, String File, boolean OptionDebug, boolean remotessh)  {
		ArrayList<Rules> rules = new ArrayList<Rules>();
		ArrayList<Rules> rules2 = new ArrayList<Rules>();
		int maxlength=0;
		OptionDebug=true;
		String command2="";
		String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULE SORTBY NAME ";
		if (remotessh)
			command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list RULE SORTBY NAME";
		else
			command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list RULE SORTBY NAME";
		try {
			getObjectRules(rules,  command);
			getObjectRules(rules2,  command2);
            
            if (rules.equals(rules2)) {
            	System.out.println("There is not diferences between the Rules Ipv4 " + rules.size() + " 2" + rules2.size());
            }else {
            	System.out.println("There is  diferences between Rules Ipv4 " + rules.size() + " 2" + rules2.size());
            	if (rules.size()<=rules2.size()) {
            		maxlength = rules2.size();
            	}else {
            		maxlength = rules.size();
            	}
            	for (int i =0; i<maxlength; i++) {
            		if (!(rules.get(i).equals(rules2.get(i)))) {  
            			System.out.println("There are differences between rules Ipv4. "  + rules.get(i).getName() + " and " +  rules2.get(i).getName());
            			Utils.CompateTwoString("Acllookupkey",rules.get(i).getAcllookupkey(),rules2.get(i).getAcllookupkey(),OptionDebug);
            			Utils.CompateTwoString("Allocable",rules.get(i).getAllocable(),rules2.get(i).getAllocable(),OptionDebug);
            			Utils.CompateTwoString("Brokenpolicy",rules.get(i).getBrokenpolicy(),rules2.get(i).getBrokenpolicy(),OptionDebug);
            			Utils.CompateTwoString("Creationtime",rules.get(i).getCreationtime(),rules2.get(i).getCreationtime(),OptionDebug);
            			Utils.CompateTwoString("Criteria",rules.get(i).getCriteria(),rules2.get(i).getCriteria(),OptionDebug);
            			Utils.CompateTwoString("Criticalhwm",rules.get(i).getCriticalhwm(),rules2.get(i).getCriticalhwm(),OptionDebug);
            			Utils.CompateTwoString("Currenthwm",rules.get(i).getCurrenthwm(),rules2.get(i).getCurrenthwm(),OptionDebug);
            			Utils.CompateTwoString("Ddnsserver",rules.get(i).getDdnsserver(),rules2.get(i).getDdnsserver(),OptionDebug);
            			Utils.CompateTwoString("Defaultgw",rules.get(i).getDefaultgw(),rules2.get(i).getDefaultgw(),OptionDebug);
            			Utils.CompateTwoString("Defaultgwlookupkey",rules.get(i).getDefaultgwlookupkey(),rules2.get(i).getDefaultgwlookupkey(),OptionDebug);
            			Utils.CompateTwoString("Defaultiplimit",rules.get(i).getDefaultiplimit(),rules2.get(i).getDefaultiplimit(),OptionDebug);
            			Utils.CompateTwoString("Description",rules.get(i).getDescription(),rules2.get(i).getDescription(),OptionDebug);
            			Utils.CompateTwoString("Disabled",rules.get(i).getDisabled(),rules2.get(i).getDisabled(),OptionDebug);
            			Utils.CompateTwoString("Disabledbyinheritance",rules.get(i).getDisabledbyinheritance(),rules2.get(i).getDisabledbyinheritance(),OptionDebug);
            			Utils.CompateTwoString("Donotsendhost",rules.get(i).getDonotsendhost(),rules2.get(i).getDonotsendhost(),OptionDebug);
            			Utils.CompateTwoString("Donotsendhostbyinheritance",rules.get(i).getDonotsendhostbyinheritance(),rules2.get(i).getDonotsendhostbyinheritance(),OptionDebug);
            			Utils.CompateTwoString("Enabledns",rules.get(i).getEnabledns(),rules2.get(i).getEnabledns(),OptionDebug);
            			Utils.CompateTwoString("Giaddrreservedtimeremaining",rules.get(i).getGiaddrreservedtimeremaining(),rules2.get(i).getGiaddrreservedtimeremaining(),OptionDebug);
            			Utils.CompateTwoString("Informonly",rules.get(i).getInformonly(),rules2.get(i).getInformonly(),OptionDebug);
            			Utils.CompateTwoString("Informonlybyinheritance",rules.get(i).getInformonlybyinheritance(),rules2.get(i).getInformonlybyinheritance(),OptionDebug);
            			Utils.CompateTwoString("Inheritdnssettings",rules.get(i).getInheritdnssettings(),rules2.get(i).getInheritdnssettings(),OptionDebug);
            			Utils.CompateTwoString("Ipfrom",rules.get(i).getIpfrom(),rules2.get(i).getIpfrom(),OptionDebug);
            			Utils.CompateTwoString("Iplimitselector",rules.get(i).getIplimitselector(),rules2.get(i).getIplimitselector(),OptionDebug);
            			Utils.CompateTwoString("Ipspoofing",rules.get(i).getIpspoofing(),rules2.get(i).getIpspoofing(),OptionDebug);
            			Utils.CompateTwoString("Ipspoofingbyinheritance",rules.get(i).getIpspoofingbyinheritance(),rules2.get(i).getIpspoofingbyinheritance(),OptionDebug);
            			Utils.CompateTwoString("Ipto",rules.get(i).getIpto(),rules2.get(i).getIpto(),OptionDebug);
            			Utils.CompateTwoString("Isgiaddrreservable",rules.get(i).getIsgiaddrreservable(),rules2.get(i).getIsgiaddrreservable(),OptionDebug);
            			Utils.CompateTwoString("Leasetime",rules.get(i).getLeasetime(),rules2.get(i).getLeasetime(),OptionDebug);
            			Utils.CompateTwoString("Leasetimelookupkey",rules.get(i).getLeasetimelookupkey(),rules2.get(i).getLeasetimelookupkey(),OptionDebug);
            			Utils.CompateTwoString("Lookupkey",rules.get(i).getLookupkey(),rules2.get(i).getLookupkey(),OptionDebug);
            			Utils.CompateTwoString("Managementlookupkey",rules.get(i).getManagementlookupkey(),rules2.get(i).getManagementlookupkey(),OptionDebug);
            			Utils.CompateTwoString("Modifiedtime",rules.get(i).getModifiedtime(),rules2.get(i).getModifiedtime(),OptionDebug);
            			Utils.CompateTwoString("Name",rules.get(i).getName(),rules2.get(i).getName(),OptionDebug);
            			Utils.CompateTwoString("Parentlookupkey",rules.get(i).getParentlookupkey(),rules2.get(i).getParentlookupkey(),OptionDebug);
            			Utils.CompateTwoString("ParentName",rules.get(i).getParentName(),rules2.get(i).getParentName(),OptionDebug);
            			Utils.CompateTwoString("Pingbefore",rules.get(i).getPingbefore(),rules2.get(i).getPingbefore(),OptionDebug);
            			Utils.CompateTwoString("Pingbeforebyinheritance",rules.get(i).getPingbeforebyinheritance(),rules2.get(i).getPingbeforebyinheritance(),OptionDebug);
            			Utils.CompateTwoString("Policylookupkey",rules.get(i).getPolicylookupkey(),rules2.get(i).getPolicylookupkey(),OptionDebug);
            			Utils.CompateTwoString("Policymemberlookupkey",rules.get(i).getPolicymemberlookupkey(),rules2.get(i).getPolicymemberlookupkey(),OptionDebug);
            			Utils.CompateTwoString("Requireslegalhostname",rules.get(i).getRequireslegalhostname(),rules2.get(i).getRequireslegalhostname(),OptionDebug);
            			Utils.CompateTwoString("Requireslegalhostnamebyinheritance",rules.get(i).getRequireslegalhostnamebyinheritance(),rules2.get(i).getRequireslegalhostnamebyinheritance(),OptionDebug);
            			Utils.CompateTwoString("Reserved",rules.get(i).getReserved(),rules2.get(i).getReserved(),OptionDebug);
            			Utils.CompateTwoString("Reservedbyinheritance",rules.get(i).getReservedbyinheritance(),rules2.get(i).getReservedbyinheritance(),OptionDebug);
            			Utils.CompateTwoString("Reservedtogiaddr",rules.get(i).getReservedtogiaddr(),rules2.get(i).getReservedtogiaddr(),OptionDebug);
            			Utils.CompateTwoString("Shuffleip",rules.get(i).getShuffleip(),rules2.get(i).getShuffleip(),OptionDebug);
            			Utils.CompateTwoString("Shuffleipbyinheritance",rules.get(i).getShuffleipbyinheritance(),rules2.get(i).getShuffleipbyinheritance(),OptionDebug);
            			Utils.CompateTwoString("Staticaddresscount",rules.get(i).getStaticaddresscount(),rules2.get(i).getStaticaddresscount(),OptionDebug);
            			Utils.CompateTwoString("Subnetmask",rules.get(i).getSubnetmask(),rules2.get(i).getSubnetmask(),OptionDebug);
            			Utils.CompateTwoString("Subnetmasklookupkey",rules.get(i).getSubnetmasklookupkey(),rules2.get(i).getSubnetmasklookupkey(),OptionDebug);
            			Utils.CompateTwoString("Subrulecount",rules.get(i).getSubrulecount(),rules2.get(i).getSubrulecount(),OptionDebug);
            			Utils.CompateTwoString("Suppressnakresponse",rules.get(i).getSuppressnakresponse(),rules2.get(i).getSuppressnakresponse(),OptionDebug);
            			Utils.CompateTwoString("Suppressnakresponsebyinheritance",rules.get(i).getSuppressnakresponsebyinheritance(),rules2.get(i).getSuppressnakresponsebyinheritance(),OptionDebug);
            			Utils.CompateTwoString("Templatelookupkey",rules.get(i).getTemplatelookupkey(),rules2.get(i).getTemplatelookupkey(),OptionDebug);
            			Utils.CompateTwoString("Totalhwm",rules.get(i).getTotalhwm(),rules2.get(i).getTotalhwm(),OptionDebug);
            			Utils.CompateTwoString("Warninghwm",rules.get(i).getWarninghwm(),rules2.get(i).getWarninghwm(),OptionDebug);

            		}
            	}
            }
			return Constants.NO_ERROR;
		}catch(Exception e) {
			System.out.println("EXCEPTION: " + e.getMessage());
			 e.printStackTrace();
			return Constants.ERROR;
		}
		
	}
	
	/**
	 * This function get the rules and create file with the objects read.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileDest: Is the File Name where the process save the information.
	 * @param String CmdAdd: this string contains all commands to include in the file.
	 * @param String Debug: If the flag Debug is turn on so the process print extra information.
	 * @return Error or not 
	 *  **/
	public static int get_function_rulesV6(String ip, String user, String pass, String FileDest, String Debug)  {
		ArrayList<RulesIPv6> rules = new ArrayList<RulesIPv6>();
		ArrayList<String> filter = new ArrayList<String>();
		String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULEV6 ";
		try{		    
			getObjectRulesV6(rules,  command);
            
            WriteDiskObjetc WDO = new WriteDiskObjetc();
            if (WDO.WriteObjectListRulesIPv6(rules,FileDest)==Constants.ERROR) {
            	System.out.println("Error write to disk");
            	return Constants.ERROR;
            }
            if (Debug.equals(Constants.LOGON)) {
            	ArrayList<RulesIPv6> rulesAux = new ArrayList<RulesIPv6>();
	            ReadDiskObject RDO = new ReadDiskObject();
	            rulesAux = RDO.getObjecRulesIPv6tDisk(FileDest,filter);
	            if (rulesAux.size()> 0) {
		            Iterator<RulesIPv6> itrRulesAux = rulesAux.iterator();
		            while(itrRulesAux.hasNext()){
		            	RulesIPv6 ruleta = itrRulesAux.next();
		            	System.out.println(ruleta.getName() + " - "  + ruleta.getStartiIpAddress() + " - "+ ruleta.getEndIpAddress() +
		            			ruleta.getCriteria());
		            	
		            }
	            }else {
	            	System.out.println("Error to obtain object or the file does not contain information");
	            }
            }
            System.out.println("Exporting " + rules.size() + " rules IPV6 to File " + FileDest);
            return Constants.NO_ERROR;
            
		}catch(Exception e) {
			System.out.println("EXCEPTION: " + e.getMessage());
			 e.printStackTrace();
			return Constants.ERROR;
		}
		
	}	
	
	
	public static int compare_function_rulesV6(String ip, String user, String pass,String ip2, String user2, String pass2, String FileDest, boolean OptionDebug, boolean remotessh)  {
		ArrayList<RulesIPv6> rules = new ArrayList<RulesIPv6>();
		ArrayList<RulesIPv6> rules2 = new ArrayList<RulesIPv6>();
		int maxlength=0;
		OptionDebug=false;
		String command2="";
		String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULE6 SORTBY NAME ";
		if (remotessh)
			command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list RULE6 SORTBY NAME";
		else
			command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list RULE6 SORTBY NAME";
		try {
			getObjectRulesV6(rules,  command);
			getObjectRulesV6(rules2,  command2);
            
            if (rules.equals(rules2)) {
            	System.out.println("There is not diferences between the Rules Ipv4 " + rules.size() + " 2" + rules2.size());
            }else {
            	System.out.println("There is  diferences between Rules Ipv4 " + rules.size() + " 2" + rules2.size());
            	if (rules.size()<=rules2.size()) {
            		maxlength = rules2.size();
            	}else {
            		maxlength = rules.size();
            	}
            	for (int i =0; i<maxlength; i++) {
            		if (!(rules.get(i).equals(rules2.get(i)))) {  
            			System.out.println("There are differences between rules Ipv4. "  + rules.get(i).getName() + " and " +  rules2.get(i).getName());
            			Utils.CompateTwoString("Criteria",rules.get(i).getCriteria(),rules2.get(i).getCriteria(),OptionDebug);
            			Utils.CompateTwoString("Description",rules.get(i).getDescription(),rules2.get(i).getDescription(),OptionDebug);
            			Utils.CompateTwoString("Disabled",rules.get(i).getDisabled(),rules2.get(i).getDisabled(),OptionDebug);
            			Utils.CompateTwoString("EndIpAddress",rules.get(i).getEndIpAddress(),rules2.get(i).getEndIpAddress(),OptionDebug);
            			Utils.CompateTwoString("Lookupkey",rules.get(i).getLookupkey(),rules2.get(i).getLookupkey(),OptionDebug);
            			Utils.CompateTwoString("Name",rules.get(i).getName(),rules2.get(i).getName(),OptionDebug);
            			Utils.CompateTwoString("NonSequential",rules.get(i).getNonSequential(),rules2.get(i).getNonSequential(),OptionDebug);
            			Utils.CompateTwoString("PreferredLifeTime",rules.get(i).getPreferredLifeTime(),rules2.get(i).getPreferredLifeTime(),OptionDebug);
            			Utils.CompateTwoString("RapidCommit",rules.get(i).getRapidCommit(),rules2.get(i).getRapidCommit(),OptionDebug);
            			Utils.CompateTwoString("StartiIpAddress",rules.get(i).getStartiIpAddress(),rules2.get(i).getStartiIpAddress(),OptionDebug);
            			Utils.CompateTwoString("ValidlifeTime",rules.get(i).getValidlifeTime(),rules2.get(i).getValidlifeTime(),OptionDebug);
            		}
            	}
            }
			return Constants.NO_ERROR;
		}catch(Exception e) {
			System.out.println("EXCEPTION: " + e.getMessage());
			 e.printStackTrace();
			return Constants.ERROR;
		}
	}
	/**
	 * This function is to get the routing elements from BCC and built a object only basically it's for obtain the object before the export
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String (ArrayList<Routing> routes : ArrayList of Routing where the function save the data
	 * @param String command: Command to execute on BCC.
	 *  **/	
	private static void getObjectRoutes(ArrayList<Routing> routes, String command) throws IOException {
		//String auxString;
		Process pb = Runtime.getRuntime().exec(command);
		InputStream is = pb.getInputStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String aux = br.readLine();
		int bandrow=0;
		int sizerow1=0;
		Routing route = new Routing();
		while (aux!=null){   
		    StringTokenizer tokens=new StringTokenizer(aux,":");
		    if (!tokens.hasMoreTokens()&&(aux.length()==0)) {
		    	sizerow1=1;
		    	
		    }else {
		        String option = tokens.nextToken();
		        String value = tokens.nextToken();
		        
		        switch(option){
					case "NAME":  route.setName(value.trim());  bandrow=1; break; 
					case "LOOKUPKEY": route.setLookUpKey(value.trim()); break;
					case "DESCRIPTION": route.setDescription(value.trim()); break;  
					case "AUTHKEY": route.setAuthkey(value.trim()); break; 
					case "CONFIRMAUTHKEY": route.setConfirmAuthkey(value.trim()); break; 
					case "DOCSISMAJORVERSION": route.setDocsisMajorVersion(value.trim() ); break; 
					case "DOCSISMINORVERSION":  route.setDocsisMinorVersion(value.trim()); break; 
					case "EXTENDEDCMTSMICTYPE": route.setExtendedCmtsmicType(value.trim()); break; 
					case "EXTENDEDCMTSMICSHAREDSECRET": route.setExtendedCmtsmicSharedSecret(value.trim()); break; 
					case "CONFIRMEXTENDEDMICSHAREDSECRET":  route.setConfirmExtendedmicSharedSecret(value.trim()); break; 
					case "MANAGEMENTIP":  route.setManagementIp(value.trim()); break; 
					case "SPREADIPALLOCATION": route.setSpreadipAllocation(value.trim()); break; 
					//default: auxString="";
				
		        }
		    }
		    if ((sizerow1==1) && (bandrow==1)){
		    	Routing routeAux = new Routing();
		    	try {
		    		routeAux = (Routing) route.clone();
		    	}catch (CloneNotSupportedException cnse) {
		            System.out.println("Cannot be cloned");
		        }
		    	routes.add(routeAux);
		    	bandrow=0;
		    	sizerow1=0;
			}
		    aux = br.readLine();
		}
	}
	
	/**
	 * This function is to get the routing elements from BCC and built a object only basically it's for obtain the object before the export
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String (ArrayList<Routing> routes : ArrayList of Routing where the function save the data
	 * @param String command: Command to execute on BCC.
	 *  **/	
	private static void getObjectDFS(ArrayList<DocsisFS> dfs, String command) throws IOException {
		//String auxString;
		Process pb = Runtime.getRuntime().exec(command);
		InputStream is = pb.getInputStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String aux = br.readLine();
		int bandrow=0;
		int sizerow1=0;
		DocsisFS dfst = new DocsisFS();
		while (aux!=null){   
		    StringTokenizer tokens=new StringTokenizer(aux,":");
		    if (!tokens.hasMoreTokens()&&(aux.length()==0)) {
		    	sizerow1=1;
		    	
		    }else {
		        String option = tokens.nextToken();
		        String value = tokens.nextToken();
		        
		        switch(option){
					case "NAME":  dfst.setName(value.trim());  bandrow=1; break; 
					case "LOOKUPKEY": dfst.setLookupkey(value.trim()); break;
					case "DESCRIPTION": dfst.setDescription(value.trim()); break;  
					case "MANAGEMENTLOOKUPKEY": dfst.setManagmentLookupkey(value.trim()); break; 			
		        }
		    }
		    if ((sizerow1==1) && (bandrow==1)){
		    	DocsisFS dfsAux = new DocsisFS();
		    	try {
		    		dfsAux = (DocsisFS) dfst.clone();
		    	}catch (CloneNotSupportedException cnse) {
		            System.out.println("Cannot be cloned");
		        }
		    	dfs.add(dfsAux);
		    	bandrow=0;
		    	sizerow1=0;
			}
		    aux = br.readLine();
		}
	}
	
	
	/**
	 * This function is to get the routing elements from BCC and built a object only basically it's for obtain the object before the export
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String (ArrayList<Routing> routes : ArrayList of Routing where the function save the data
	 * @param String command: Command to execute on BCC.
	 *  **/	
	private static void getObjectDCS(ArrayList<DeviceClassifier> dcs, String command) throws IOException {
		//String auxString;
		Process pb = Runtime.getRuntime().exec(command);
		InputStream is = pb.getInputStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String aux = br.readLine();
		int bandrow=0;
		int sizerow1=0;
		DeviceClassifier dc = new DeviceClassifier();
		while (aux!=null){   
		    StringTokenizer tokens=new StringTokenizer(aux,":");
		    if (!tokens.hasMoreTokens()&&(aux.length()==0)) {
		    	sizerow1=1;
		    	
		    }else {
		        String option = tokens.nextToken();
		        String value = tokens.nextToken();
		        
		        switch(option){
					case "NAME":  dc.setName(value.trim());  bandrow=1; break; 
					case "CRITERIA": dc.setCriteria(value.trim()); break;
					case "DESCRIPTION": dc.setDescription(value.trim()); break; 
					case "COPYIDTOACS": dc.setCopyIdTacs(value.trim()); break;
					case "ENABLEACSCLUSTERING": dc.setEnableCsClustering(value.trim()); break;
					case "LOOKUPKEY": dc.setLookupKey(value.trim()); break;
					case "MANAGEMENTLOOKUPKEY": dc.setManagmentLookUpkey(value.trim()); break;
					case "ACLLOOKUPKEY": dc.setAclLookupKey(value.trim()); break; 			
		        }
		    }
		    if ((sizerow1==1) && (bandrow==1)){
		    	DeviceClassifier dcAux = new DeviceClassifier();
		    	try {
		    		dcAux = (DeviceClassifier) dc.clone();
		    	}catch (CloneNotSupportedException cnse) {
		            System.out.println("Cannot be cloned");
		        }
		    	dcs.add(dcAux);
		    	bandrow=0;
		    	sizerow1=0;
			}
		    aux = br.readLine();
		}
	}
	
	/**
	 * This function is to get the routing elements from BCC and built a object only basically it's for obtain the object before the export
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String (ArrayList<Routing> routes : ArrayList of Routing where the function save the data
	 * @param String command: Command to execute on BCC.
	 *  **/	
	private static void getObjectCCG(ArrayList<ClientClassGroup> ccgs, String command) throws IOException {
		//String auxString;
		Process pb = Runtime.getRuntime().exec(command);
		InputStream is = pb.getInputStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String aux = br.readLine();
		int bandrow=0;
		int sizerow1=0;
		ClientClassGroup ccg = new ClientClassGroup();
		while (aux!=null){   
		    StringTokenizer tokens=new StringTokenizer(aux,":");
		    if (!tokens.hasMoreTokens()&&(aux.length()==0)) {
		    	sizerow1=1;
		    	
		    }else {
		        String option = tokens.nextToken();
		        String value = tokens.nextToken();
		        
		        switch(option){
					case "NAME":  ccg.setName(value.trim());  bandrow=1; break; 
					case "CLIENTCLASS": ccg.setClientclass(value.trim()); break;
					case "DESCRIPTION": ccg.setDescription(value.trim()); break; 
					case "MOVEDUPLICATES": ccg.setMoveDuplicates(value.trim()); break;
					case "QOS": ccg.setQos(value.trim()); break;
					case "LOOKUPKEY": ccg.setLookup(value.trim()); break;
					case "MANAGEMENTLOOKUPKEY": ccg.setManagmentLookup(value.trim()); break;
					case "ACLLOOKUPKEY": ccg.setAcclokupkey(value.trim()); break; 	
					case "TEMPLATELOOKUPKEY": ccg.setTemplateLookupKey(value.trim()); break;
		        }
		    }
		    if ((sizerow1==1) && (bandrow==1)){
		    	ClientClassGroup ccgAux = new ClientClassGroup();
		    	try {
		    		ccgAux = (ClientClassGroup) ccg.clone();
		    	}catch (CloneNotSupportedException cnse) {
		            System.out.println("Cannot be cloned");
		        }
		    	ccgs.add(ccgAux);
		    	bandrow=0;
		    	sizerow1=0;
			}
		    aux = br.readLine();
		}
	}
	/**
	 * This function build the object Client Class through executions in BCC and read the output from BCC.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Rules 
	 * @param String command: Command to execute in BCC
	 *  **/
	private static void getObjectClientClass(ArrayList<ClientClass> ccs, String command) throws IOException {
		Process pb = Runtime.getRuntime().exec(command);
		InputStream is = pb.getInputStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String aux = br.readLine();
		int bandrow=0;
		int sizerow1=0;
		ClientClass cc = new ClientClass();
		while (aux!=null){  
			aux=aux.replace("CMTS: ", "CMTS");
		    StringTokenizer tokens=new StringTokenizer(aux,":");
		    if (!tokens.hasMoreTokens()&&(aux.length()==0)) {
		    	sizerow1=1;
		    	
		    }else {
		    
		        String option = tokens.nextToken();
		        String value = tokens.nextToken();
		        switch(option){
					case "NAME":  cc.setName(value.trim().replace("CMTS", "CMTS:"));  bandrow=1; break;
					case "DESCRIPTION":  cc.setDescription(value.trim()); break;
					case "DATATYPE": cc.setDateType(value.trim()); break;
					case "CLIENTDATATYPE": cc.setClientDataType(value.trim()); break;
					case "SUBDATATYPES": cc.setSubDataType(value.trim()); break;
					case "DHCPOPTIONNUMBER": cc.setDhcpOptionNumber(value.trim()); break;
					case "DHCPSUBOPTIONNUMBER": cc.setDhcpSubOptionNumber(value.trim()); break;
					case "PRIORITY": cc.setPriority(value.trim()); break;
					case "SYSTEMPRIORITY": cc.setSystemPriority(value.trim()); break;
					case "DEFAULTIPLIMIT": cc.setDefaultIpLimit(value.trim()); break;
					case "IPLIMITFIELD": cc.setIpLimitfield(value.trim()); break;
					case "ANTIROAMING": cc.setAntiroaming(value.trim()); break;
					case "IPSHUFFLING": cc.setShuffleIp(value.trim()); break;
					case "QOS": cc.setQos(value.trim()); break;
					case "UNPROVISIONEDDEVICES": cc.setUnprovisionedDevicess(value.trim()); break;
					case "DEFAULT": cc.setDefault_(value.trim()); break;
					case "CLIENTCLASSENTRY_SIZE": cc.setClienClassEntry(Integer.valueOf(value.trim())); break;
					case "LOOKUPKEY": cc.setLookup(value.trim());  break;
					case "ACLLOOKUPKEY": cc.setAcclokupkey(value.trim()); break;
					case "MANAGEMENTLOOKUPKEY": cc.setManagmentLookup(value.trim()); break;
					case "TEMPLATELOOKUPKEY": cc.setTemplateLookUpkey(value.trim()); break;
					case "DEFAULTPREFIXLENGTH": cc.setDefaultPrefixlength(value.trim()); break;
					case "PREFIXLENGTHHINTINGSTEPSIZE": cc.setPrefixlenghtSize(value.trim()); break;
					case "MINIMUMDELEGATIONPREFIXLENGTH": cc.setMinimunDelegation(value.trim()); break;
					case "NUMBER_OF_V6_OPTIONS": cc.setNumberV6Options(value.trim()); break;
					case "ENABLEDNSV6": cc.setEnableDns6(value.trim()); break;
					case "DDNSSERVERV6": cc.setDdnsServer6(value.trim()); break;
					case "INHERITDNSSETTINGSV6": cc.setInheritDns6(value.trim()); break;
					case "PERFORMAAAADNSUPDATES": cc.setPerformDnnsUp(value.trim()); break;
					case "SHALLNOTPERFORMANYDNSUPDATES": cc.setShallPerformanceUpd(value.trim()); break;
		        }
		    }
		    if ((sizerow1==1) && (bandrow==1)){
		    	ClientClass ccAux = new ClientClass();
		    	try {
		    		ccAux = (ClientClass) cc.clone();
		    	}catch (CloneNotSupportedException cnse) {
		            System.out.println("Cannot be cloned");
		        }
		    	ccs.add(ccAux);
		    	bandrow=0;
		    	sizerow1=0;
			}
		    aux = br.readLine();
		}
	}
	
	
	/**
	 * This function build the object Rules through executions in BCC and read the output from BCC.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Rules 
	 * @param String command: Command to execute in BCC
	 *  **/
	private static void getObjectRules(ArrayList<Rules> rules, String command) throws IOException {
		Process pb = Runtime.getRuntime().exec(command);
		InputStream is = pb.getInputStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String aux = br.readLine();
		int bandrow=0;
		int sizerow1=0;
		Rules rule = new Rules();
		while (aux!=null){  
			
		    StringTokenizer tokens=new StringTokenizer(aux,":");
		    if (!tokens.hasMoreTokens()&&(aux.length()==0)) {
		    	sizerow1=1;
		    	
		    }else {
		    
		        String option = tokens.nextToken();
		        String value = tokens.nextToken();
		        switch(option){
					case "NAME":  rule.setName(value.trim());  bandrow=1; break; 
					case "LOOKUPKEY": rule.setLookupkey(value.trim()); break;
					case "DESCRIPTION": rule.setDescription(value.trim()); break;  
					case "IPFROM": rule.setIpfrom(value.trim()); break; 
					case "IPTO": rule.setIpto(value.trim()); break; 
					case "ISGIADDRRESERVABLE": rule.setIsgiaddrreservable(value.trim() ); break; 
					case "GIADDRRESERVEDTIMEREMAINING":  rule.setGiaddrreservedtimeremaining(value.trim()); break; 
					case "REQUIRESLEGALHOSTNAME": rule.setRequireslegalhostname(value.trim()); break; 
					case "DONOTSENDHOST": rule.setDonotsendhost(value.trim()); break; 
					case "DISABLED":  rule.setDisabled(value.trim()); break; 
					case "RESERVED":  rule.setReserved(value.trim()); break; 
					case "PINGBEFORE": rule.setPingbefore(value.trim()); break; 
					case "IPLIMITSELECTOR": rule.setIplimitselector(value.trim()); break; 
					case "DEFAULTIPLIMIT": rule.setDefaultiplimit(value.trim()); break; 
					case "SHUFFLEIP": rule.setShuffleip(value.trim()); break; 
					case "INFORMONLY":  rule.setInformonly( value.trim()); break; 
					case "IPSPOOFING":  rule.setIpspoofing(value.trim()); break; 
					case "SUPPRESSNAKRESPONSE": rule.setSuppressnakresponse(value.trim()); break; 
					case "CRITERIA":  rule.setCriteria( value.trim()); break;  
					case "LEASETIME": rule.setLeasetime(value.trim()); break; 
					case "DEFAULTGW": rule.setDefaultgw(value.trim()); break; 
					case "SUBNETMASK":  rule.setSubnetmask(value.trim()); break; 
					case "ENABLEDNS":  rule.setEnabledns(value.trim()); break; 
					case "DDNSSERVER": rule.setDdnsserver(value.trim()); break; 
					case "INHERITDNSSETTINGS":  rule.setInheritdnssettings(value.trim()); break; 
					case "PARENTLOOKUPKEY": rule.setParentlookupkey(value.trim());break;			
		        }
		    }
		    if ((sizerow1==1) && (bandrow==1)){
		    	Rules ruleAux = new Rules();
		    	try {
		    		ruleAux = (Rules) rule.clone();
		    	}catch (CloneNotSupportedException cnse) {
		            System.out.println("Cannot be cloned");
		        }
		    	rules.add(ruleAux);
		    	bandrow=0;
		    	sizerow1=0;
			}
		    aux = br.readLine();
		}
	}
	
	
	/**
	 * This function build the object Rules through executions in BCC and read the output from BCC.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Rules 
	 * @param String command: Command to execute in BCC
	 *  **/
	private static void getObjectRulesV6(ArrayList<RulesIPv6> rules, String command) throws IOException {
		Process pb = Runtime.getRuntime().exec(command);
		InputStream is = pb.getInputStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String aux = br.readLine();
		int bandrow=0;
		int sizerow1=0;
		RulesIPv6 rule = new RulesIPv6();
		StringTokenizer tokens;

		while (aux!=null){  
			if (aux.indexOf("DESCRIPTION") == -1)
				 tokens=new StringTokenizer(aux," ");
			else {
				 tokens=new StringTokenizer(aux,":");
			}
		    if (!tokens.hasMoreTokens()&&(aux.length()==0)) {
		    	sizerow1=1;		    	
		    }else {
		    	String option="";
		    	String value="";
		    	if (tokens.countTokens()>1) {
		    		option = tokens.nextToken().trim();
		    		value = tokens.nextToken().trim();
		    	}
		        switch(option){
					case "NAME:":  rule.setName(value.trim());  bandrow=1; break; 
					case "LOOKUPKEY:": rule.setLookupkey(value.trim()); break;
					case "DESCRIPTION": rule.setDescription(value.trim()); break;
					case "CRITERIA:":  rule.setCriteria(aux.replace("CRITERIA:", "")); break;
					case "DISABLED:":  rule.setDisabled(value.trim()); break;
					case "ENDIPADDRESS:": rule.setEndIpAddress(value.trim()); break; 
					case "STARTIPADDRESS:": rule.setStartiIpAddress(value.trim()); break; 
					case "NONSEQUENTIAL:": rule.setNonSequential(value.trim() ); break; 
					case "PREFERREDLIFETIME:":  rule.setPreferredLifeTime(value.trim()); break; 
					case "RAPIDCOMMIT:": rule.setRapidCommit(value.trim()); break; 
					case "VALIDLIFETIME:": rule.setValidlifeTime(value.trim()); break; 	
		        }
		    }
		    if ((sizerow1==1) && (bandrow==1)){
		    	RulesIPv6 ruleAux = new RulesIPv6();
		    	try {
		    		ruleAux = (RulesIPv6) rule.clone();
		    	}catch (CloneNotSupportedException cnse) {
		            System.out.println("Cannot be cloned");
		        }
		    	rules.add(ruleAux);
		    	bandrow=0;
		    	sizerow1=0;
			}
		    aux = br.readLine();
		}
	}
	
	/**
	 * This function is to get the static IP from BCC and built a object only basically it's for obtain the object before the export
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String (ArrayList<StaticAddress> templates : ArrayList of StaticAddress where the function save the data
	 * @param String command: Command to execute on BCC.
	 *  **/	
	private static void getObjectStaticAddess(ArrayList<StaticAddress> staticAdds, String command, ArrayList<Rules> rules,ArrayList<Templates> templates) throws IOException {
		Process pb = Runtime.getRuntime().exec(command);
		InputStream is = pb.getInputStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String aux = br.readLine();
		int bandrow=0;
		int sizerow1=0;
		int index=-1;
		StaticAddress staticAdd = new StaticAddress();
		while (aux!=null){     
			String MAC="";
			index=aux.indexOf("CLIENTDATA:");
			if (index==0) {
				MAC = aux.replaceAll("CLIENTDATA:", "");
			}
		    StringTokenizer tokens=new StringTokenizer(aux,":");
		    if (!tokens.hasMoreTokens()&&(aux.length()==0)) {
		    	sizerow1=1;
		    	
		    }else {
		    	String value="";
		        String option = tokens.nextToken();
		        if (index==0) {
		        	value = MAC;
		        	index=-1;
		        }else {
		        	value = tokens.nextToken();
		        }
		        
		        
		        switch(option){
					case "NAME":  staticAdd.setName(value.trim());  bandrow=1; break; 
					case "LOOKUPKEY": staticAdd.setLookupkey(value.trim()); break;
					case "DESCRIPTION": staticAdd.setDescription(value.trim()); break; 
					case "CLIENTDATATYPE": staticAdd.setClientdatatype(value.trim()); break;
					case "STATICIPADDRESS": staticAdd.setStaticipaddress(value.trim()); break;
					case "MANUAL": staticAdd.setManual(value.trim()); break;
					case "TEMPLATELOOKUPKEY": staticAdd.setTemplate(searchTemplateByLookUpKay(templates,value.trim())); break;
					case "PARENTRULELOOKUPKEY": staticAdd.setRule(searchRuleByLookUpKey(rules,value.trim())); break; 
					case "CLIENTDATA": staticAdd.setClientdata(value.trim()); break; 
					case "ENABLEDNS": staticAdd.setEnabledns(value.trim() ); break; 
					case "DDNSSERVER": staticAdd.setDdnsserver(value.trim() ); break;
					case "INHERITDNSSETTINGS":  staticAdd.setInheritdnssettings(value.trim()); break; 
					case "HARDWARETYPE":  staticAdd.setHardwaretype(value.trim()); break; 
		        }
		    }
		    if ((sizerow1==1) && (bandrow==1)){
		    	StaticAddress staticAddAux = new StaticAddress();
		    	try {
		    		staticAddAux = (StaticAddress) staticAdd.clone();
		    	}catch (CloneNotSupportedException cnse) {
		            System.out.println("Cannot be cloned");
		        }
		    	staticAdds.add(staticAddAux);
		    	bandrow=0;
		    	sizerow1=0;
			}
		    aux = br.readLine();
		}
	}

	/**
	 * This function is to get the templates from BCC and built a object only basically it's for obtain the object before the export
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String (ArrayList<Templates> templates : ArrayList of Templates where the function save the data
	 * @param String command: Command to execute on BCC.
	 *  **/	
	private static void getObjectTemplates(ArrayList<Templates> templates, String command) throws IOException {
		Process pb = Runtime.getRuntime().exec(command);
		InputStream is = pb.getInputStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String aux = br.readLine();
		int bandrow=0;
		int sizerow1=0;
		Templates template = new Templates();
		while (aux!=null){          	
		    StringTokenizer tokens=new StringTokenizer(aux,":");
		    if (!tokens.hasMoreTokens()&&(aux.length()==0)) {
		    	sizerow1=1;
		    	
		    }else {
		        String option = tokens.nextToken();
		        String value = tokens.nextToken();
		        
		        switch(option){
					case "NAME":  template.setName(value.trim());  bandrow=1; break; 
					case "LOOKUPKEY": template.setLookupkey(value.trim()); break;
					case "DESCRIPTION": template.setDescription(value.trim()); break;  
					case "ENABLEDNS": template.setEnabledns(value.trim()); break; 
					case "DDNSSERVER": template.setDdnsserver(value.trim()); break; 
					case "INHERITDNSSETTINGS": template.setInheritdnssettings(value.trim() ); break; 
					case "GLOBALTEMPLATE":  template.setGlobaltemplate(value.trim()); break; 
		        }
		    }
		    if ((sizerow1==1) && (bandrow==1)){
		    	Templates templateaux = new Templates();
		    	try {
		    		templateaux = (Templates) template.clone();
		    	}catch (CloneNotSupportedException cnse) {
		            System.out.println("Cannot be cloned");
		        }
		    	templates.add(templateaux);
		    	bandrow=0;
		    	sizerow1=0;
			}
		    aux = br.readLine();
		}
	}
	
	/**
	 * This function is to get the routing elements from BCC and built a object with them and save in a File
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileDest: Is the name of the File where the function will save the export object 
	 * @return int Constant with Error o No Error.
	 *  **/	
		public static int getRoutingElements(String ip, String user, String pass, String FileDest) {
			ArrayList<Routing> routes = new ArrayList<Routing>();
			ArrayList<String> filter = new ArrayList<String>();
			ArrayList<Gateways> gateways = new ArrayList<Gateways>();
			String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list ROUTINGELEMENT ";
			try {
				getObjectRoutes(routes, command);
				Iterator<Routing> itrRules = routes.iterator();
				
				while(itrRules.hasNext()){
					Routing rulet = itrRules.next();
					gateways=getGateways(ip, user, pass, rulet.getName());
					rulet.setGateways(gateways);
				}
	            WriteDiskObjetc WDO = new WriteDiskObjetc();
	            if (WDO.WriteObjectListRoutes(routes,FileDest)==Constants.ERROR) {
	            	System.out.println("Error write to disk");
	            	return Constants.ERROR;
	            }
	
	            
	            ArrayList<Routing> routesAux = new ArrayList<Routing>();
	            ReadDiskObject RDO = new ReadDiskObject();
	            routesAux = RDO.getObjecRoutestDisk(FileDest,filter);
	            if (routesAux.size()> 0) {
		            Iterator<Routing> itrRoutesAux = routesAux.iterator();
		            while(itrRoutesAux.hasNext()){
		            	Routing ruleta = itrRoutesAux.next();
		            	LogWriter.getLogger().info(ruleta.toString());
		            	ArrayList<Gateways> g = ruleta.getGateways();
		            	Iterator<Gateways> itrRoutesAuxg = g.iterator();
			            while(itrRoutesAuxg.hasNext()){
			            	Gateways ga = itrRoutesAuxg.next();
			            	LogWriter.getLogger().info("*" + ga.getGateway() + " - "  + ga.getNetwork());
			            }
		            }
		            
	            }else {
	            	System.out.println("Error al obtener objeto o el arhcivo no contiene informacion");
	            }
	            
	            System.out.println("Importing " + routes.size() + " Routing Elements to File " + FileDest);
				return Constants.NO_ERROR;
			}catch(Exception e) {
				System.out.println("Error getting data of routing elements" + e.getMessage());
				e.printStackTrace();
				return Constants.ERROR;
			}
		}
		
		public static int compareRoutingElements(String ip, String user, String pass,String ip2, String user2, String pass2, String FileDest, boolean OptionDebug, boolean remotessh) {
			ArrayList<Routing> routes = new ArrayList<Routing>();
			ArrayList<Routing> routes2 = new ArrayList<Routing>();
			
			OptionDebug=false;
			int maxlength=0;
			int maxlengthGw=0;
			String command2="";
			String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list ROUTINGELEMENT SORTBY NAME";
			if (remotessh)
				command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list ROUTINGELEMENT SORTBY NAME";
			else
				command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list ROUTINGELEMENT SORTBY NAME";
			try{
				getObjectRoutes(routes,command);
				
				getObjectRoutes(routes2,  command2);

				
	            if (routes.equals(routes2)) {
	            	System.out.println("There is not diferences between the routing elements " + routes.size() + " and  " + routes2.size());
	            }else {
	            	System.out.println("There is  diferences between the routing elements " + routes.size() + " and " + routes2.size());
	            	if (routes.size()<=routes2.size()) {
	            		maxlength = routes2.size();
	            	}else {
	            		maxlength = routes.size();
	            	}
	            	for (int i =0; i<maxlength; i++) {
	            		if (!(routes.get(i).equals(routes2.get(i)))) {            			
	            			System.out.println("There are differences between routing elements "  + routes.get(i).getName() + " and " +  routes2.get(i).getName());
	            			Utils.CompateTwoString("NAME" , routes.get(i).getName(), routes2.get(i).getName(), OptionDebug);
	            			Utils.CompateTwoString("DESCRIPTION" ,routes.get(i).getDescription() , routes2.get(i).getDescription(),OptionDebug);
	            			Utils.CompateTwoString("LOOKUP" , routes.get(i).getLookUpKey(), routes2.get(i).getLookUpKey(),OptionDebug);
	            			Utils.CompateTwoString("Authkey", routes.get(i).getAuthkey(),routes2.get(i).getAuthkey(),OptionDebug);
	            			Utils.CompateTwoString("ConfirmAuthkey", routes.get(i).getConfirmAuthkey(), routes2.get(i).getConfirmAuthkey(),OptionDebug );
	            			Utils.CompateTwoString("DocsisMajorVersion", routes.get(i).getDocsisMajorVersion(),routes2.get(i).getDocsisMajorVersion(),OptionDebug);
	            			Utils.CompateTwoString("DocsisMinorVersion", routes.get(i).getDocsisMinorVersion(), routes2.get(i).getDocsisMinorVersion(),OptionDebug);
	            			Utils.CompateTwoString("ExtendedCmtsmicType", routes.get(i).getExtendedCmtsmicType(), routes2.get(i).getExtendedCmtsmicType(),OptionDebug);
	            			Utils.CompateTwoString("ExtendedCmtsmicSharedSecret", routes.get(i).getExtendedCmtsmicSharedSecret(), routes2.get(i).getExtendedCmtsmicSharedSecret(),OptionDebug);
	            			Utils.CompateTwoString("ConfirmExtendedmicSharedSecret", routes.get(i).getConfirmExtendedmicSharedSecret(), routes2.get(i).getConfirmExtendedmicSharedSecret(),OptionDebug);
	            			Utils.CompateTwoString("ManagementIp", routes.get(i).getManagementIp(), routes2.get(i).getManagementIp(),OptionDebug);
	            			Utils.CompateTwoString("SpreadipAllocation", routes.get(i).getSpreadipAllocation(), routes2.get(i).getSpreadipAllocation(),OptionDebug);
	            			ArrayList<Gateways> ruleta = new ArrayList<Gateways>();
        					ruleta=getGateways(ip, user, pass, routes.get(i).getName());

        					ArrayList<Gateways> ruleta2 = new ArrayList<Gateways>();
        					if (remotessh)
        						ruleta2=getGatewaysRemoto(ip2, user2, pass2, routes2.get(i).getName());
        					else
        						ruleta2=getGateways(ip2, user2, pass2, routes2.get(i).getName());
        					
	            			System.out.println(" Sizes " + ruleta.size() + " + " + ruleta2.size());
	            			//if (ruleta.size() != ruleta2.size()) {
	            				if (ruleta.size()<=ruleta2.size()) {
	            					maxlengthGw = ruleta2.size();
	        	            	}else {
	        	            		maxlengthGw = ruleta.size();
	        	            	}
	            				
		            			for(int j=0; j< maxlengthGw;j++) {
		            				if (!(ruleta.get(j).equals(ruleta2.get(j)))) {
		            					System.out.println("There are differences between gateways and nets. "  + ruleta.get(j).getGateway() + " and " +  ruleta2.get(j).getGateway());
		    	            			Utils.CompateTwoString("GW" , ruleta.get(j).getGateway(), ruleta2.get(j).getGateway(), false);
		    	            			Utils.CompateTwoString("NET" , ruleta.get(j).getNetwork(), ruleta2.get(j).getNetwork(), false);
		            				}
			                	}
	            			//}	
	            		}	
	            	}
	            }
	            
	            
	            return Constants.NO_ERROR;
	            
			}catch(Exception e) {
				System.out.println("EXCEPTION: " + e.getMessage());
				 e.printStackTrace();
				return Constants.ERROR;
			}
		}

	private static ArrayList<Gateways> getGateways(String ip, String user, String pass, String routeName) throws IOException {
		
			List<String> list = new ArrayList<>();
			ProcessBuilder builder;
			Process proc;
			InputStream is2;
			BufferedReader br2;
			list.clear();
			list.add("ipcli");
			list.add("-n");
			list.add(user);
			list.add("-p");
			list.add(pass);
			list.add( "-s");
			list.add(ip);
			list.add("show");
			list.add("ROUTINGELEMENT");
			list.add(routeName);
			
			builder = new ProcessBuilder(list);
			proc = builder.start();                	
			is2 = proc.getInputStream();
			br2 = new BufferedReader (new InputStreamReader (is2));
			String aux2 = br2.readLine();
			LogWriter.getLogger().info( aux2);
		    int bandrow2=0;
		    int sizerow12=0;
		    ArrayList<Gateways> gateways = new ArrayList<Gateways>();
		    Gateways gateway = new Gateways();
		    while (aux2!=null){
		    	StringTokenizer tokens2=new StringTokenizer(aux2,":");
		        if (!tokens2.hasMoreTokens()&&(aux2.length()==0)) {
		        	sizerow12=1;
		        }else {
		        
		            String option2 = tokens2.nextToken();
		            //String value = tokens2.nextToken();
		            switch(option2.trim()){
		    			case "GATEWAY":  gateway.setGateway(aux2.replace("GATEWAY:",""));  bandrow2=1; break; 
		    			case "NETWORK": gateway.setNetwork(aux2.replace(":FALSE","").replace("NETWORK:", "")); break;    	    			
		            }
		        }
		        if ((sizerow12==1) && (bandrow2==1)){
		        	Gateways gAux = new Gateways();
		        	try {
		        		gAux = (Gateways) gateway.clone();
		        	}catch (CloneNotSupportedException cnse) {
		                System.out.println("Cannot be cloned");
		            }
		        	gateways.add(gAux);
		        	bandrow2=0;
		        	sizerow12=0;
				}
		        aux2 = br2.readLine();
		    }
		    return gateways;
	}
	
	private static ArrayList<TVLSettings> getTLVs(String ip, String user, String pass, String dfsName) throws IOException {
		
		List<String> list = new ArrayList<>();
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		list.clear();
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("show");
		list.add("DOCSISFS");
		list.add(dfsName);
		//list.add("list");
		//list.add("tlvsettings");
		
		
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		String aux2 = br2.readLine();
		LogWriter.getLogger().info( aux2);
	    int bandrow2=0;
	    int sizerow12=0;
	    ArrayList<TVLSettings> tvls = new ArrayList<TVLSettings>();
	    TVLSettings tlv = new TVLSettings();
	    while (aux2!=null){
	    	StringTokenizer tokens2=new StringTokenizer(aux2,":");
	        if (!tokens2.hasMoreTokens()&&(aux2.length()==0)) {
	        	sizerow12=1;
	        }else {
	        
	            String option2 = tokens2.nextToken();
	            //String value = tokens2.nextToken();
	            switch(option2.trim()){
	    			case "TLVCODE":  tlv.setTlvCode(aux2.replace("TLVCODE:",""));  bandrow2=1; break; 
	    			case "TLVNAME": tlv.setTlvName(aux2.replace(":TLVNAME","")); break; 
	    			case "TLVFLAG": tlv.setTlvFlav(aux2.replace(":TLVFLAG","")); break;
	    			case "TLVDATA": tlv.setTlvData(aux2.replace(":TLVDATA","")); break;
	    			case "LDAPNAME": tlv.setLdapName(aux2.replace(":LDAPNAME","")); break;
	    			case "LDAPQUERY": tlv.setLdapquery(aux2.replace(":LDAPQUERY","")); break;
	    			case "LDAPATTR": tlv.setLdapAttr(aux2.replace(":LDAPATTR","")); break;
	    			case "EXTERNALNAME": tlv.setExternalName(aux2.replace(":EXTERNALNAME","")); break;
	    			case "EXTERNALQUERY": tlv.setExternalQuery(aux2.replace(":EXTERNALQUERY","")); break;
	    			case "EXTERNALFIELD": tlv.setExternalField(aux2.replace(":EXTERNALFIELD","")); break;
	    			//case "EXTERNALDEFAULT": tlv.setExternalDefault(aux2.replace(":EXTERNALDEFAULT","")); break;
	    			case "TLVVENDOR": tlv.setTlvVendor(aux2.replace(":TLVVENDOR","")); break;
	    			case "TLVENTRY": tlv.setTlvEntry(aux2.replace(":TLVENTRY","")); break;
	            }
	        }
	        if ((sizerow12==1) && (bandrow2==1)){
	        	TVLSettings tlvgAux = new TVLSettings();
	        	try {
	        		tlvgAux = (TVLSettings) tlv.clone();
	        	}catch (CloneNotSupportedException cnse) {
	                System.out.println("Cannot be cloned");
	            }
	        	tvls.add(tlvgAux);
	        	bandrow2=0;
	        	sizerow12=0;
			}
	        aux2 = br2.readLine();
	    }
	    return tvls;
}
	
private static ArrayList<TVLSettings> getTLVsRemote(String ip, String user, String pass, String dfsName) throws IOException {
		
		String Name= dfsName.replace("(", "-f");
		Name	=Name.replace(")", "-fc") ;
		 char singleQuotesChar = '"'; 
		Name = singleQuotesChar+Name+singleQuotesChar;
		System.out.println(Name);
		List<String> list = new ArrayList<>();
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		list.clear();
		list.add("ssh");
		list.add("root@"+ ip);
		list.add("/root/ipcli.sh");
/*		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("show");
		list.add("DOCSISFS");*/
		list.add(Name);
		//list.add("list");
		//list.add("tlvsettings");
		
		
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		String aux2 = br2.readLine();
		LogWriter.getLogger().info( aux2);
	    int bandrow2=0;
	    int sizerow12=0;
	    ArrayList<TVLSettings> tvls = new ArrayList<TVLSettings>();
	    TVLSettings tlv = new TVLSettings();
	    while (aux2!=null){
	    	StringTokenizer tokens2=new StringTokenizer(aux2,":");
	        if (!tokens2.hasMoreTokens()&&(aux2.length()==0)) {
	        	sizerow12=1;
	        }else {
	        
	            String option2 = tokens2.nextToken();
	            //String value = tokens2.nextToken();
	            switch(option2.trim()){
	    			case "TLVCODE":  tlv.setTlvCode(aux2.replace("TLVCODE:",""));  bandrow2=1; break; 
	    			case "TLVNAME": tlv.setTlvName(aux2.replace(":TLVNAME","")); break; 
	    			case "TLVFLAG": tlv.setTlvFlav(aux2.replace(":TLVFLAG","")); break;
	    			case "TLVDATA": tlv.setTlvData(aux2.replace(":TLVDATA","")); break;
	    			case "LDAPNAME": tlv.setLdapName(aux2.replace(":LDAPNAME","")); break;
	    			case "LDAPQUERY": tlv.setLdapquery(aux2.replace(":LDAPQUERY","")); break;
	    			case "LDAPATTR": tlv.setLdapAttr(aux2.replace(":LDAPATTR","")); break;
	    			case "EXTERNALNAME": tlv.setExternalName(aux2.replace(":EXTERNALNAME","")); break;
	    			case "EXTERNALQUERY": tlv.setExternalQuery(aux2.replace(":EXTERNALQUERY","")); break;
	    			case "EXTERNALFIELD": tlv.setExternalField(aux2.replace(":EXTERNALFIELD","")); break;
	    			//case "EXTERNALDEFAULT": tlv.setExternalDefault(aux2.replace(":EXTERNALDEFAULT","")); break;
	    			case "TLVVENDOR": tlv.setTlvVendor(aux2.replace(":TLVVENDOR","")); break;
	    			case "TLVENTRY": tlv.setTlvEntry(aux2.replace(":TLVENTRY","")); break;
	            }
	        }
	        if ((sizerow12==1) && (bandrow2==1)){
	        	TVLSettings tlvgAux = new TVLSettings();
	        	try {
	        		tlvgAux = (TVLSettings) tlv.clone();
	        	}catch (CloneNotSupportedException cnse) {
	                System.out.println("Cannot be cloned");
	            }
	        	tvls.add(tlvgAux);
	        	bandrow2=0;
	        	sizerow12=0;
			}
	        aux2 = br2.readLine();
	    }
	    return tvls;
}

	private static ArrayList<Gateways> getGatewaysRemoto(String ip, String user, String pass, String routeName) throws IOException {
		
		List<String> list = new ArrayList<>();
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		System.out.println("RE + "+routeName);
		list.clear();
		list.add("ssh");
		list.add("root@"+ ip);
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("show");
		list.add("ROUTINGELEMENT");
		list.add("'"+routeName+"'");
		
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		String aux2 = br2.readLine();
		LogWriter.getLogger().info( aux2);
	    int bandrow2=0;
	    int sizerow12=0;
	    ArrayList<Gateways> gateways = new ArrayList<Gateways>();
	    Gateways gateway = new Gateways();
	    while (aux2!=null){
	    	StringTokenizer tokens2=new StringTokenizer(aux2,":");
	        if (!tokens2.hasMoreTokens()&&(aux2.length()==0)) {
	        	sizerow12=1;
	        }else {
	        
	            String option2 = tokens2.nextToken();
	            //String value = tokens2.nextToken();
	            switch(option2.trim()){
	    			case "GATEWAY":  gateway.setGateway(aux2.replace("GATEWAY:",""));  bandrow2=1; break; 
	    			case "NETWORK": gateway.setNetwork(aux2.replace(":FALSE","").replace("NETWORK:", "")); break;    	    			
	            }
	        }
	        if ((sizerow12==1) && (bandrow2==1)){
	        	Gateways gAux = new Gateways();
	        	try {
	        		gAux = (Gateways) gateway.clone();
	        	}catch (CloneNotSupportedException cnse) {
	                System.out.println("Cannot be cloned");
	            }
	        	gateways.add(gAux);
	        	bandrow2=0;
	        	sizerow12=0;
			}
	        aux2 = br2.readLine();
	    }
	    return gateways;
}
	
	private static ArrayList<CNS> getCNS(String ip, String user, String pass, String ClientClassName) throws IOException {
		
		List<String> list = new ArrayList<>();
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		File file = new File("commandAux.txt");
		file.createNewFile();
		PrintWriter fl = new PrintWriter(file);
		fl.println("modify clientclass lookupkey=" + ClientClassName);
		fl.println("list CNS");
		fl.println("exit");
		fl.close();
		list.clear();
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("-f");
		list.add("commandAux.txt");

		
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		String aux2 = br2.readLine();
		LogWriter.getLogger().info( aux2);
	    int bandrow2=0;
	    int sizerow12=0;
	    ArrayList<CNS> cnss = new ArrayList<CNS>();
	    CNS cns = new CNS();
	    while (aux2!=null){
	    	StringTokenizer tokens2=new StringTokenizer(aux2,":");
	        if (!tokens2.hasMoreTokens()&&(aux2.length()==0)) {
	        	sizerow12=1;
	        }else {
	        	
	            String option2 = tokens2.nextToken();
	            //String value = tokens2.nextToken();
	            switch(option2.trim()){
	    			case "LOOKUPKEY":  cns.setLookupkey(aux2.replace("LOOKUPKEY:",""));  bandrow2=1; break; 
	    			case "NAME": cns.setName(aux2.replace("NAME:","")); break;   
	    			case "PRIORITY": cns.setPriority(aux2.replace("PRIORITY:","")); break;
	    			case "NUMBER_OF_V4_OPTIONS": cns.setNumberOptionsv4(aux2.replace("NUMBER_OF_V4_OPTIONS:","")); break;
	    			case "ENABLEDNSV4": cns.setEnableDNSv4(aux2.replace("ENABLEDNSV4:","")); break;
	    			case "DDNSSERVERV4": cns.setDDNSv4(aux2.replace("DDNSSERVERV4:","")); break;
	    			case "INHERITDNSSETTINGSV4": cns.setInheritDnsSetinv4(aux2.replace("INHERITDNSSETTINGSV4:","")); break;
	    			case "NUMBER_OF_V6_OPTIONS": cns.setNumberOptionsv6(aux2.replace("NUMBER_OF_V6_OPTIONS:","")); break;
	    			case "ENABLEDNSV6": cns.setEnableDNSv6(aux2.replace("ENABLEDNSV6:","")); break;
	    			case "DDNSSERVERV6": cns.setDDNSv6(aux2.replace("DDNSSERVERV6:","")); break;
	    			case "INHERITDNSSETTINGSV6": cns.setInheritDnsSetinv6(aux2.replace("INHERITDNSSETTINGSV6:","")); break;
	    			case "PERFORMAAAADNSUPDATES": cns.setPerformanceUpdates(aux2.replace("PERFORMAAAADNSUPDATES:","")); break;
	    			case "TEMPLATE": cns.setTemplate(aux2.replace("TEMPLATE:","")); break;
	            }
	        }
	        if ((sizerow12==1) && (bandrow2==1)){
	        	CNS cnsaux = new CNS();
	        	try {
	        		cnsaux = (CNS) cns.clone();
	        	}catch (CloneNotSupportedException cnse) {
	                System.out.println("Cannot be cloned");
	            }
	        	cnss.add(cnsaux);
	        	bandrow2=0;
	        	sizerow12=0;
			}
	        aux2 = br2.readLine();
	    }
	    return cnss;
}
	
	private static ArrayList<CNS> getCNSRemote(String ip, String user, String pass, String ClientClassName) throws IOException {
		
		List<String> list = new ArrayList<>();
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		File file = new File("commandAux.txt");
		file.createNewFile();
		PrintWriter fl = new PrintWriter(file);
		fl.println("modify clientclass" + ClientClassName);
		fl.println("list CNS");
		fl.println("exit");
		fl.close();
		list.clear();
		list.clear();
		list.add("ssh");
		list.add("root@"+ ip);
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("-f");
		list.add("commandAux.txt");
		
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		String aux2 = br2.readLine();
		LogWriter.getLogger().info( aux2);
	    int bandrow2=0;
	    int sizerow12=0;
	    ArrayList<CNS> cnss = new ArrayList<CNS>();
	    CNS cns = new CNS();
	    while (aux2!=null){
	    	StringTokenizer tokens2=new StringTokenizer(aux2,":");
	        if (!tokens2.hasMoreTokens()&&(aux2.length()==0)) {
	        	sizerow12=1;
	        }else {
	        
	            String option2 = tokens2.nextToken();
	            //String value = tokens2.nextToken();
	            switch(option2.trim()){
		            case "LOOKUPKEY":  cns.setLookupkey(aux2.replace("LOOKUPKEY:",""));  bandrow2=1; break; 
	    			case "NAME": cns.setName(aux2.replace("NAME:","")); break;   
	    			case "PRIORITY": cns.setPriority(aux2.replace("PRIORITY:","")); break;
	    			case "NUMBER_OF_V4_OPTIONS": cns.setNumberOptionsv4(aux2.replace("NUMBER_OF_V4_OPTIONS:","")); break;
	    			case "ENABLEDNSV4": cns.setEnableDNSv4(aux2.replace("ENABLEDNSV4:","")); break;
	    			case "DDNSSERVERV4": cns.setDDNSv4(aux2.replace("DDNSSERVERV4:","")); break;
	    			case "INHERITDNSSETTINGSV4": cns.setInheritDnsSetinv4(aux2.replace("INHERITDNSSETTINGSV4:","")); break;
	    			case "NUMBER_OF_V6_OPTIONS": cns.setNumberOptionsv6(aux2.replace("NUMBER_OF_V6_OPTIONS:","")); break;
	    			case "ENABLEDNSV6": cns.setEnableDNSv6(aux2.replace("ENABLEDNSV6:","")); break;
	    			case "DDNSSERVERV6": cns.setDDNSv6(aux2.replace("DDNSSERVERV6:","")); break;
	    			case "INHERITDNSSETTINGSV6": cns.setInheritDnsSetinv6(aux2.replace("INHERITDNSSETTINGSV6:","")); break;
	    			case "PERFORMAAAADNSUPDATES": cns.setPerformanceUpdates(aux2.replace("PERFORMAAAADNSUPDATES:","")); break;
	    			case "TEMPLATE": cns.setTemplate(aux2.replace("TEMPLATE:","")); break;   	    			
	            }
	        }
	        if ((sizerow12==1) && (bandrow2==1)){
	        	CNS cnsaux = new CNS();
	        	try {
	        		cnsaux = (CNS) cns.clone();
	        	}catch (CloneNotSupportedException cnse) {
	                System.out.println("Cannot be cloned");
	            }
	        	cnss.add(cnsaux);
	        	bandrow2=0;
	        	sizerow12=0;
			}
	        aux2 = br2.readLine();
	    }
	    return cnss;
}
	
	private static ArrayList<DHCPOption> getDHCPOptions(String ip, String user, String pass, String ClientClassName,String CNSName) throws IOException {
		
		List<String> list = new ArrayList<>();
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		File file = new File("commandAux.txt");
		file.createNewFile();
		PrintWriter fl = new PrintWriter(file);
		fl.println("modify clientclass lookupkey=" + ClientClassName);
		fl.println("modify CNS lookupkey=" + CNSName);
		fl.println("list DHCPOPTION");
		fl.println("exit");
		fl.close();
		list.clear();
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("-f");
		list.add("commandAux.txt");

		
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		String aux2 = br2.readLine();
		LogWriter.getLogger().info( aux2);
	    int bandrow2=0;
	    int sizerow12=0;
	    ArrayList<DHCPOption> options = new ArrayList<DHCPOption>();
	    DHCPOption option = new DHCPOption();
	    while (aux2!=null){
	    	StringTokenizer tokens2=new StringTokenizer(aux2,":");
	        if (!tokens2.hasMoreTokens()&&(aux2.length()==0)) {
	        	sizerow12=1;
	        }else {
	        	
	            String option2 = tokens2.nextToken();
	            //String value = tokens2.nextToken();
	            switch(option2.trim()){
	    			case "OPTNUM":  option.setOption(aux2.replace("OPTNUM:",""));  bandrow2=1; break; 
	    			case "NAME": option.setName(aux2.replace("NAME:","")); break;   
	    			case "DATATYPE": option.setDataType(aux2.replace("DATATYPE:","")); break;
	    			case "DATA": option.setData(aux2.replace("DATA:","")); break;
	    			case "SIZE": option.setSize(aux2.replace("SIZE:","")); break;
	    			
	            }
	        }
	        if ((sizerow12==1) && (bandrow2==1)){
	        	DHCPOption optionaux = new DHCPOption();
	        	try {
	        		optionaux = (DHCPOption) option.clone();
	        	}catch (CloneNotSupportedException cnse) {
	                System.out.println("Cannot be cloned");
	            }
	        	options.add(optionaux);
	        	bandrow2=0;
	        	sizerow12=0;
			}
	        aux2 = br2.readLine();
	    }
	    return options;
}

private static ArrayList<DHCPOption> getDHCPOptionsCC(String ip, String user, String pass, String ClientClassName) throws IOException {
		
		List<String> list = new ArrayList<>();
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		File file = new File("commandAux.txt");
		file.createNewFile();
		PrintWriter fl = new PrintWriter(file);
		fl.println("modify clientclass lookupkey=" + ClientClassName);
		fl.println("list DHCPOPTION");
		fl.println("exit");
		fl.close();
		list.clear();
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("-f");
		list.add("commandAux.txt");

		
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		String aux2 = br2.readLine();
		LogWriter.getLogger().info( aux2);
	    int bandrow2=0;
	    int sizerow12=0;
	    ArrayList<DHCPOption> options = new ArrayList<DHCPOption>();
	    DHCPOption option = new DHCPOption();
	    while (aux2!=null){
	    	StringTokenizer tokens2=new StringTokenizer(aux2,":");
	        if (!tokens2.hasMoreTokens()&&(aux2.length()==0)) {
	        	sizerow12=1;
	        }else {
	        	
	            String option2 = tokens2.nextToken();
	            //String value = tokens2.nextToken();
	            switch(option2.trim()){
	    			case "OPTNUM":  option.setOption(aux2.replace("OPTNUM:",""));  bandrow2=1; break; 
	    			case "NAME": option.setName(aux2.replace("NAME:","")); break;   
	    			case "DATATYPE": option.setDataType(aux2.replace("DATATYPE:","")); break;
	    			case "DATA": option.setData(aux2.replace("DATA:","")); break;
	    			case "SIZE": option.setSize(aux2.replace("SIZE:","")); break;
	    			
	            }
	        }
	        if ((sizerow12==1) && (bandrow2==1)){
	        	DHCPOption optionaux = new DHCPOption();
	        	try {
	        		optionaux = (DHCPOption) option.clone();
	        	}catch (CloneNotSupportedException cnse) {
	                System.out.println("Cannot be cloned");
	            }
	        	options.add(optionaux);
	        	bandrow2=0;
	        	sizerow12=0;
			}
	        aux2 = br2.readLine();
	    }
	    return options;
}
private static ArrayList<DHCPOption> getDHCPOptionsRemote(String ip, String user, String pass, String ClientClassName,String CNSName) throws IOException {
		
		List<String> list = new ArrayList<>();
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		File file = new File("commandAux.txt");
		file.createNewFile();
		PrintWriter fl = new PrintWriter(file);
		fl.println("modify clientclass " + ClientClassName);
		fl.println("modify CNS " + CNSName);
		fl.println("list DHCPOPTION");
		fl.println("exit");
		fl.close();
		list.clear();
		list.add("ssh");
		list.add("root@"+ ip);
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		list.add("-f");
		list.add("commandAux.txt");

		
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		String aux2 = br2.readLine();
		LogWriter.getLogger().info( aux2);
	    int bandrow2=0;
	    int sizerow12=0;
	    ArrayList<DHCPOption> options = new ArrayList<DHCPOption>();
	    DHCPOption option = new DHCPOption();
	    while (aux2!=null){
	    	StringTokenizer tokens2=new StringTokenizer(aux2,":");
	        if (!tokens2.hasMoreTokens()&&(aux2.length()==0)) {
	        	sizerow12=1;
	        }else {
	        	
	            String option2 = tokens2.nextToken();
	            //String value = tokens2.nextToken();
	            switch(option2.trim()){
	    			case "OPTNUM":  option.setOption(aux2.replace("OPTNUM:",""));  bandrow2=1; break; 
	    			case "NAME": option.setName(aux2.replace("NAME:","")); break;   
	    			case "DATATYPE": option.setDataType(aux2.replace("DATATYPE:","")); break;
	    			case "DATA": option.setData(aux2.replace("DATA:","")); break;
	    			case "SIZE": option.setSize(aux2.replace("SIZE:","")); break;
	    			
	            }
	        }
	        if ((sizerow12==1) && (bandrow2==1)){
	        	DHCPOption optionaux = new DHCPOption();
	        	try {
	        		optionaux = (DHCPOption) option.clone();
	        	}catch (CloneNotSupportedException cnse) {
	                System.out.println("Cannot be cloned");
	            }
	        	options.add(optionaux);
	        	bandrow2=0;
	        	sizerow12=0;
			}
	        aux2 = br2.readLine();
	    }
	    return options;
}
	/**
	 * This function is to get Static Address and Templates from BCC and built a object with them and save in Files
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileDest: Is the name of the File where the function will save the export object 
	 * @return int Constant with Error o No Error.
	 *  **/	
		public static int getStaticIPs(String ip, String user, String pass, String FileDest) {
			ArrayList<Templates> templates = new ArrayList<Templates>();
			ArrayList<Rules> rules = new ArrayList<Rules>();
			ArrayList<StaticAddress> staticAdds = new ArrayList<StaticAddress>();		
			String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list TEMPLATE ";
			try {
				getObjectTemplates(templates, command);
				command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULE ";
				getObjectRules(rules,  command);
				command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list STATICADDRESS ";
				getObjectStaticAddess(staticAdds,command,rules,templates);
	            WriteDiskObjetc WDO = new WriteDiskObjetc();
	            if (WDO.WriteObjectListStaticAddress(templates,staticAdds,FileDest)==Constants.ERROR) {
	            	System.out.println("Error write to disk");
	            	return Constants.ERROR;
	            }       
	            /**
	            ArrayList<StaticAddress> staticA = new ArrayList<StaticAddress>();
	            ObjectInputStream leyendoFicheros2 = new ObjectInputStream( 
	    	            new FileInputStream("StaticAddress"+FileDest) );
	            staticA = ( ArrayList<StaticAddress> )leyendoFicheros2.readObject();
	    	            leyendoFicheros2.close();
	    	            
	            if (staticA.size()> 0) {
		            Iterator<StaticAddress> itrStaAddAux = staticA.iterator();
		            while(itrStaAddAux.hasNext()){
		            	StaticAddress ruleta = itrStaAddAux.next();
		            	System.out.println(ruleta.getName() + "-" +ruleta.getClientdata() + "-" + ruleta.getClientdatatype() + "-" + ruleta.getDdnsserver() + "-" + ruleta.getDescription() + "-" + ruleta.getEnabledns() + "-" + ruleta.getHardwaretype() + "-" + 
		            			ruleta.getInheritdnssettings() + "-" + ruleta.getLookupkey() + "-" + ruleta.getManual() + "-" +  ruleta.getRule() + "-" + ruleta.getStaticipaddress() + "-" + ruleta.getTemplate());
		            }
	            
	            }else {
	            	System.out.println("Error to read file or file is empty");
	            }
	            **/
	            System.out.println("Importing " + staticAdds.size() + " Static Address and " +  templates.size() +" templates to both Files " + FileDest);
				return Constants.NO_ERROR;
			}catch(Exception e) {
				System.out.println("Error getting data of routing elements" + e.getMessage());
				e.printStackTrace();
				return Constants.ERROR;
			}
		}
	
		public static int compareStaticIPs(String ip, String user, String pass,String ip2, String user2, String pass2, String FileDest, boolean OptionDebug, boolean remotessh ) {
			ArrayList<Templates> templates = new ArrayList<Templates>();
			ArrayList<Templates> templates2 = new ArrayList<Templates>();
			ArrayList<Rules> rules = new ArrayList<Rules>();
			ArrayList<Rules> rules2 = new ArrayList<Rules>();
			ArrayList<StaticAddress> staticAdds = new ArrayList<StaticAddress>();
			ArrayList<StaticAddress> staticAdds2 = new ArrayList<StaticAddress>();
			Integer maxlength=0;
			OptionDebug=false;
			String command2="";
			try {
				String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list TEMPLATE SORTBY NAME ";
				if (remotessh)
					command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list TEMPLATE SORTBY NAME ";
				else
					command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list TEMPLATE SORTBY NAME ";
				getObjectTemplates(templates, command);
				getObjectTemplates(templates2, command2);
				
				command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULE SORTBY NAME";
				if (remotessh)
					command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list RULE SORTBY NAME";
				else
					command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list RULE SORTBY NAME";
				getObjectRules(rules,  command);
				getObjectRules(rules2,  command2);
				
				command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list STATICADDRESS SORTBY NAME";
				if (remotessh)
					command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + "  list STATICADDRESS SORTBY NAME";
				else
					command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + "  list STATICADDRESS SORTBY NAME";
				getObjectStaticAddess(staticAdds,command,rules,templates);
				getObjectStaticAddess(staticAdds2,command2,rules2,templates2);
				
				if (staticAdds.equals(staticAdds2)) {
	            	System.out.println("There is not diferences between the static address " + staticAdds.size() + " 2" + staticAdds2.size());
	            }else {
	            	System.out.println("There is  diferences between the static address  " + staticAdds.size() + " 2" + staticAdds2.size());
	            	if (staticAdds.size()<=staticAdds2.size()) {
	            		maxlength = staticAdds2.size();
	            	}else {
	            		maxlength = staticAdds.size();
	            	}
	            	for (int i =0; i<maxlength; i++) {
	            		if (!(staticAdds.get(i).equals(staticAdds2.get(i)))) {
	            			System.out.println("There are differences between static Address. "  + staticAdds.get(i).getName() + " and " +  staticAdds2.get(i).getName());
	            			Utils.CompateTwoString("Clientdata",staticAdds.get(i).getClientdata(),staticAdds2.get(i).getClientdata(),OptionDebug);
	            			Utils.CompateTwoString("Clientdatatype",staticAdds.get(i).getClientdatatype(),staticAdds2.get(i).getClientdatatype(),OptionDebug);
	            			Utils.CompateTwoString("Ddnsserver",staticAdds.get(i).getDdnsserver(),staticAdds2.get(i).getDdnsserver(),OptionDebug);
	            			Utils.CompateTwoString("Description",staticAdds.get(i).getDescription(),staticAdds2.get(i).getDescription(),OptionDebug);
	            			Utils.CompateTwoString("Enabledns",staticAdds.get(i).getEnabledns(),staticAdds2.get(i).getEnabledns(),OptionDebug);
	            			Utils.CompateTwoString("Hardwaretype",staticAdds.get(i).getHardwaretype(),staticAdds2.get(i).getHardwaretype(),OptionDebug);
	            			Utils.CompateTwoString("Inheritdnssettings",staticAdds.get(i).getInheritdnssettings(),staticAdds2.get(i).getInheritdnssettings(),OptionDebug);
	            			Utils.CompateTwoString("Lookupkey",staticAdds.get(i).getLookupkey(),staticAdds2.get(i).getLookupkey(),OptionDebug);
	            			Utils.CompateTwoString("Manual",staticAdds.get(i).getManual(),staticAdds2.get(i).getManual(),OptionDebug);
	            			Utils.CompateTwoString("Name",staticAdds.get(i).getName(),staticAdds2.get(i).getName(),OptionDebug);
	            			Utils.CompateTwoString("Rule",staticAdds.get(i).getRule(),staticAdds2.get(i).getRule(),OptionDebug);
	            			Utils.CompateTwoString("Staticipaddress",staticAdds.get(i).getStaticipaddress(),staticAdds2.get(i).getStaticipaddress(),OptionDebug);
	            			Utils.CompateTwoString("Template",staticAdds.get(i).getTemplate(),staticAdds2.get(i).getTemplate(),OptionDebug);

	            		}
	            	}
	            }
	           
				return Constants.NO_ERROR;
			}catch(Exception e) {
				System.out.println("Error getting data of routing elements" + e.getMessage());
				e.printStackTrace();
				return Constants.ERROR;
			}
		}
	
	public static int assignParams(String[] args,int argsNum) {

		Options options = new Options();
		options.addOption("s", true, "DHCP server IP address");
		options.addOption("u", true,"Login user name");
		options.addOption("p", true, "Login password");
		options.addOption("fs", true, "File Source");
		options.addOption("s2", true, "DHCP server IP address");
		options.addOption("u2", true,"Login user name");
		options.addOption("p2", true, "Login password");
		options.addOption("filter", true, "Filter File Name");
		options.addOption("fd", true, "File destination");
		options.addOption("Rules","rules", false, "Rules Option");
		options.addOption("Rules6","rules6", false, "Rules IPv6 Option");
		options.addOption("RE","re", false, "Routing Elements Option");
		options.addOption("SP","sp", false, "Static IP Option");
		options.addOption("CC","cc", false, "Client Class Option");
		options.addOption("DFS","dfs", false, "Docsis File Settings");
		options.addOption("DC","dc", false, "Device Classifier");
		options.addOption("CCG","dc", false, "Client Class Group");
		options.addOption("CNS","cns", false, "CLassified Network Settings");
		options.addOption("Read","read", false, "Read from file (CMTS config) Option");
		options.addOption("NamesRules","namesrules", false, "Rules names in CMTS Files");
		options.addOption("D","d", true, "Directory with CMTS files with extension txt");		
		options.addOption("Export","export", false, "Export Option");
		options.addOption("Import","import", false, "Import Option");
		options.addOption("Report","report", true, "Report Option");
		options.addOption("Compare","compare", false, "Compare Option");
		options.addOption("Add","add", false, "Add Option");
		options.addOption("Overwrite","overwrite", false, "Overwrite Option");
		options.addOption("Delete","delete", false, "Delete All and restore Option");
		options.addOption("Complement","complement", false, "Complement information for gateways");
		options.addOption("h","help", false, "Help argument detected");
		options.addOption("Debug","debug", false, "Debug Mode On");
		options.addOption("Man","man", false,"Show how to file");
		options.addOption("Templates","templates", false,"Enable template import");
		options.addOption("Ssh","ssh", false,"Remote ssh");
		
		try {
			CommandLineParser parser  = new BasicParser();  
			CommandLine cmdLine = parser.parse(options, args); 
			if (cmdLine.hasOption("h")){  
                new HelpFormatter().printHelp("BCCUtils 1.0.6", options );  
                return Constants.NO_ERROR;  
            }
			if (cmdLine.hasOption("Man")){
				Option=Constants.MAN;
				return Constants.NO_ERROR; 
			}
			/**Option**/

			if (cmdLine.hasOption("Export")){
				Option=Constants.EXPORT;
			}
			if (cmdLine.hasOption("Import")){
				Option=Constants.IMPORT;
			}
			if (cmdLine.hasOption("Read")){
				Option=Constants.READ;
			}
			if (cmdLine.hasOption("Report")){
				Option=Constants.REPORT;
				if  (cmdLine.getOptionValue("Report").equals("Statics"))
					ReportType=Constants.CCStatics;
				if  (cmdLine.getOptionValue("Report").equals("Detail"))
					ReportType=Constants.CCSDetails;
			}
			if (cmdLine.hasOption("Compare")){
				Option=Constants.COMPARE;
			}
			/**Type Option**/
			if (cmdLine.hasOption("Rules")){
				TypeOption=Constants.RULES;
			}
			if (cmdLine.hasOption("Rules6")){
				TypeOption=Constants.RULESV6;
			}
			if (cmdLine.hasOption("RE")){
				TypeOption=Constants.ROUTES;
			}
			if (cmdLine.hasOption("SP")){
				TypeOption=Constants.STATICIP;
			}
			if (cmdLine.hasOption("CC")){
				TypeOption=Constants.CLIENTCLASS;
			}
			
			if (cmdLine.hasOption("Templates")){
				templateOpt=true;
				TypeOption=Constants.TEMPLATES;
			}
			if (cmdLine.hasOption("DFS")){
				TypeOption=Constants.DFS;
			}
			if (cmdLine.hasOption("DC")){
				TypeOption=Constants.DC;
			}
			
			if (cmdLine.hasOption("CCG")){
				TypeOption=Constants.CCG;
			}
			
			if (cmdLine.hasOption("CNS")){
				TypeOption=Constants.CNS;
			}
			
			/**Action**/
			
			if (cmdLine.hasOption("Add")){
				Action=Constants.ADD;
			}
			if (cmdLine.hasOption("Overwrite")){
				Action=Constants.OVERWRITE;
			}
			if (cmdLine.hasOption("Delete")){
				Action=Constants.DELETEALL;
			}
			if (cmdLine.hasOption("Complement")){
				Action=Constants.COMPLEMENT;
			}
			
			if (cmdLine.hasOption("s")){
				ipBcc = cmdLine.getOptionValue("s");
				NetworkUtils nu = new NetworkUtils();
				if (!(nu.validIP(ipBcc))) {
					System.out.println("The parameter -s is not IP");
					ipBcc="";
					return Constants.ERROR;
				}
			}
			
			if (cmdLine.hasOption("s2")){
				ipBcc2 = cmdLine.getOptionValue("s2");
				NetworkUtils nu = new NetworkUtils();
				if (!(nu.validIP(ipBcc2))) {
					System.out.println("The parameter -s is not IP");
					ipBcc2="";
					return Constants.ERROR;
				}
			}
			
			if (cmdLine.hasOption("u")){
				usrBcc = cmdLine.getOptionValue("u");
			}
			
			if (cmdLine.hasOption("u2")){
				usrBcc2 = cmdLine.getOptionValue("u2");
			}
			
			if (cmdLine.hasOption("p")){
				passBcc = cmdLine.getOptionValue("p");
			}
			
			if (cmdLine.hasOption("p2")){
				passBcc2 = cmdLine.getOptionValue("p2");
			}
			
			if (cmdLine.hasOption("ssh")){
				System.out.println("Turn on ssh remote");
				ssh = true;
			}
			if (cmdLine.hasOption("fs")){
				FileNameSrc = cmdLine.getOptionValue("fs");
			}
			
			if (cmdLine.hasOption("fd")){
				FileNameDst = cmdLine.getOptionValue("fd");
			}
			if (cmdLine.hasOption("D")){
				Directory = cmdLine.getOptionValue("D");
			}
			if (cmdLine.hasOption("NamesRules")){
				nameOption=true;
			}
			
			if (cmdLine.hasOption("Debug")){ 
				logOption=Constants.LOGON;
			}
			if ("".equals(logOption)) {
				logOption="A";
			}
			
			if (cmdLine.hasOption("filter")){
				FilterFileName = cmdLine.getOptionValue("filter");
			}
			
			
			if (("".equals(passBcc))&&((!"".equals(ipBcc))  || (!"".equals(usrBcc)))) {			
				Console console = null;
				
				console = System.console();
				console.printf("Introduce BCC Login password:");
				 if (console != null) {
					 char[] passwordChars = console.readPassword();
				     String passwordString = new String(passwordChars);
				     passBcc = passwordString;
				     if (logOption==Constants.LOGON)
				    	 System.out.println("Your password is: " + passBcc);
				 }		
			}
			
			if (("".equals(passBcc2))&&((!"".equals(ipBcc2))  || (!"".equals(usrBcc2)))) {			
				Console console = null;
				
				console = System.console();
				console.printf("Introduce BCC 2 Login password:");
				 if (console != null) {
					 char[] passwordChars = console.readPassword();
				     String passwordString = new String(passwordChars);
				     passBcc2 = passwordString;
				     if (logOption==Constants.LOGON)
				    	 System.out.println("Your password is: " + passBcc);
				 }		
			}
			
			if ((("".equals(ipBcc)) || ("".equals(passBcc)) || ("".equals(usrBcc)) )&& (Option.equals(Constants.EXPORT) )) {
				System.out.println("The BCC parameters are necesary");
				return Constants.ERROR;
			}
			
			if ((("".equals(ipBcc2)) || ("".equals(passBcc2)) || ("".equals(usrBcc2)) )&& (Option.equals(Constants.EXPORT) )) {
				System.out.println("The BCC 2 parameters are necesary");
				return Constants.ERROR;
			}
			
			
			if ((Option.equals(Constants.EXPORT)) && ("".equals(FileNameDst))) {
				System.out.println("The File Name Destiation parameter is necesary");
				return Constants.ERROR;
			}
			if ((Option.equals(Constants.READ))&&(TypeOption.equals(Constants.ROUTES))) {
				if (("".equals(FileNameDst))||("".equals(Directory))) {
					System.out.println("File Name Destination or Directory is necesary");
					return Constants.ERROR;
				}
			}
			if (!"A".equals(logOption))
				System.out.println("Executing with option " + Option +" TypeOption " + TypeOption+ " IpBCC " + ipBcc+" Usr " +  
				usrBcc+" Pass " +  passBcc+" FileNameSrc " +  FileNameSrc+" FileNameDst " +  FileNameDst+" logOption " + logOption 
				+" Directory " + Directory+" Action "  +  Action);
			return Constants.NO_ERROR;
			
		}catch (org.apache.commons.cli.ParseException ex){  
            System.out.println(ex.getMessage());  
              
            new HelpFormatter().printHelp(Main.class.getCanonicalName(), options );    // Error, imprimimos la ayuda  
            return Constants.ERROR;
		}
	}
	
	public static ArrayList<String> ReadFilterFile(String FileName) {
		String line="";
		ArrayList<String> filter= new ArrayList<String>();
		try {
			FileReader fr = new FileReader (FileName);
			BufferedReader br = new BufferedReader(fr);
	        while((line=br.readLine())!=null) {
	        	filter.add(line);
	        }
	        br.close();
	        fr.close();
		}catch(Exception e) {
			System.out.println("Error opening the filter file : " + e.getMessage());
			return filter;
		}
		return filter;
	}
	
	/**
	 * The main function, is for know what operation execute in accordance with the input parameters.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String[] args : It's array with the parameters that receive from console
	 *  **/	
	public static void main(String[] args) {
		ArrayList<String> filter = new ArrayList<String>();
				
		if (assignParams(args, args.length)==Constants.ERROR){
			System.out.println("Insuficient Parameters..");
			return;
		}
		if (Option.equals(Constants.MAN)) {
			
			try {
				
				ClassLoader cl = Thread.currentThread().getContextClassLoader();
				InputStream is = cl.getResourceAsStream("command");
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String linea="";
				while((linea=br.readLine())!=null)
					System.out.println(linea);

			}catch(Exception e) {
				System.out.println(e.getMessage());
			}
			return;
		}
		if (!"".equals(FilterFileName)) {
			filter = ReadFilterFile(FilterFileName);
			if (filter.size()== 0) {
				System.out.println("The filter files doesn't exist or doesn't contain information");
				return;
			}
		}
		LogWriter.loglevel= logOption;
		
		/****Exporting data ******/
		/**Rules**/
		if ((Option.equals(Constants.EXPORT))&&(TypeOption.equals(Constants.RULES))) {
			System.out.println("Exporting rules IPv4 from " + ipBcc );
			if (get_function_rules(ipBcc,usrBcc,passBcc,FileNameDst,logOption)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		if ((Option.equals(Constants.EXPORT))&&(TypeOption.equals(Constants.RULESV6))) {
			System.out.println("Exporting rules IPv6 from " + ipBcc );
			if (get_function_rulesV6(ipBcc,usrBcc,passBcc,FileNameDst,logOption)==Constants.ERROR) {
				System.out.println("Error executing Rules6 function");
			}
		}
		/**Routing Elements***/
		if ((Option.equals(Constants.EXPORT))&&(TypeOption.equals(Constants.ROUTES))) {
			System.out.println("Exporting Routing Elements from " + ipBcc);
			if (getRoutingElements(ipBcc,usrBcc,passBcc,FileNameDst)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/**Read CMTS File and creates RE & Rules***/
		if ((Option.equals(Constants.READ))) { //&&(TypeOption.equals(Constants.ROUTES))) {
			System.out.println("Reading Routing Elements and Rules from " + Directory + " Folder");
			if (readFilesCMTS(FileNameDst,Directory,nameOption)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/**Static Address and Templates***/
		if ((Option.equals(Constants.EXPORT))&&(TypeOption.equals(Constants.STATICIP))) {
			System.out.println("Exporting Static IP and Templates from " + ipBcc );
			if (getStaticIPs(ipBcc,usrBcc,passBcc,FileNameDst)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/****Importing data ******/
		
		/**Rules IPv4**/
		if ((Option.equals(Constants.IMPORT))&&(TypeOption.equals(Constants.RULES))) {
			System.out.println("Importing rules to " + ipBcc);
			if (set_functon_rules(ipBcc,usrBcc,passBcc,FileNameSrc,Action,filter,logOption)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/**Rules IPv6**/
		if ((Option.equals(Constants.IMPORT))&&(TypeOption.equals(Constants.RULESV6))) {
			System.out.println("Importing rules IPv6 to " + ipBcc);
			if (set_functon_rules6(ipBcc,usrBcc,passBcc,FileNameSrc,Action,filter,logOption)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/**Routing Elements***/
		if ((Option.equals(Constants.IMPORT))&&(TypeOption.equals(Constants.ROUTES))) {
			System.out.println("Importing routing elements to " + ipBcc );
			if (setRoutingElements(ipBcc,usrBcc,passBcc,FileNameSrc,Action,filter,logOption)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/**Static Address and Templates***/
		if ((Option.equals(Constants.IMPORT))&&(TypeOption.equals(Constants.STATICIP))) {
			System.out.println("Importing static address and templates to " + ipBcc);
			if (setStaticAddressTemplates(ipBcc,usrBcc,passBcc,FileNameSrc,Action,filter,logOption)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/**Reports Client Classes***/
		if ((Option.equals(Constants.REPORT))&&(TypeOption.equals(Constants.CLIENTCLASS)&&(ReportType==Constants.CCStatics))) {
			System.out.println("Reading Clients Class from " + ipBcc );
			if (getClientClass(ipBcc,usrBcc,passBcc,FileNameDst)==Constants.ERROR) {
				System.out.println("Error executing ClientClass function");
			}
		}
		
		/**Reports Client Classes with DFS***/
		if ((Option.equals(Constants.REPORT))&&(TypeOption.equals(Constants.CLIENTCLASS)&&(ReportType==Constants.CCSDetails))) {
			System.out.println("Reading Clients Class from " + ipBcc );
			if (getClientClassDetail(ipBcc,usrBcc,passBcc,FileNameDst)==Constants.ERROR) {
				System.out.println("Error executing ClientClass function");
			}
		}
		
		/******************Compare section*********************/
		/**Compare Client Classes***/
		if ((Option.equals(Constants.COMPARE))&&(TypeOption.equals(Constants.CLIENTCLASS))) {
			System.out.println("Reading Clients Class from " + ipBcc + " against " + ipBcc2 );
			System.out.println("BCC 1: " + ipBcc + " " + usrBcc + " " + passBcc );
			System.out.println("BCC 2: " + ipBcc2 + " " + usrBcc2 + " " + passBcc2 );
			if (compareClientClass(ipBcc,usrBcc,passBcc,ipBcc2,usrBcc2,passBcc2,FileNameDst,false,ssh)==Constants.ERROR) {
				System.out.println("Error executing ClientClass function");
			}
		}
		
		/**Compare Routing Elements***/
		if ((Option.equals(Constants.COMPARE))&&(TypeOption.equals(Constants.ROUTES))) {
			System.out.println("Reading Clients Class from " + ipBcc + " against " + ipBcc2 );
			if (compareRoutingElements(ipBcc,usrBcc,passBcc,ipBcc2,usrBcc2,passBcc2,FileNameDst,false,ssh)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/**Compare RulesIpv4***/
		if ((Option.equals(Constants.COMPARE))&&(TypeOption.equals(Constants.RULES))) {
			System.out.println("Reading Clients Class from " + ipBcc + " against " + ipBcc2);
			if (compare_function_rules(ipBcc,usrBcc,passBcc,ipBcc2,usrBcc2,passBcc2,FileNameDst, false,ssh)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/**Compare RulesIpv6***/
		if ((Option.equals(Constants.COMPARE))&&(TypeOption.equals(Constants.RULESV6))) {
			System.out.println("Reading Clients Class from " + ipBcc + " against " + ipBcc2);
			if (compare_function_rulesV6(ipBcc,usrBcc,passBcc,ipBcc2,usrBcc2,passBcc2,FileNameDst, false,ssh)==Constants.ERROR) {
				System.out.println("Error executing Rules6 function");
			}
		}
		
		/**Compare StaticAddress***/
		if ((Option.equals(Constants.COMPARE))&&(TypeOption.equals(Constants.STATICIP))) {
			System.out.println("Reading Clients Class from " + ipBcc + " against " + ipBcc2);
			if (compareStaticIPs(ipBcc,usrBcc,passBcc,ipBcc2,usrBcc2,passBcc2,FileNameDst,false,ssh)==Constants.ERROR) {
				System.out.println("Error executing Rules function");
			}
		}
		
		/**Compare Templates***/
		if ((Option.equals(Constants.COMPARE))&&(TypeOption.equals(Constants.TEMPLATES))) {
			System.out.println("Reading Clients Class from " + ipBcc + " against " + ipBcc2);
			if (compareStaticAddressTemplates(ipBcc,usrBcc,passBcc,ipBcc2,usrBcc2,passBcc2,FileNameDst, false,ssh)==Constants.ERROR) {
				System.out.println("Error executing ClientClass function");
			}
		}
		
		/**Compare DFS***/
		if ((Option.equals(Constants.COMPARE))&&(TypeOption.equals(Constants.DFS))) {
			System.out.println("Reading Clients Class from " + ipBcc + " against " + ipBcc2);
			if (comparefunctionDFS(ipBcc,usrBcc,passBcc,ipBcc2,usrBcc2,passBcc2,FileNameDst,false,ssh)==Constants.ERROR) {
				System.out.println("Error executing ClientClass function");
			}
		}
		
		/**Compare Device Classifier***/
		if ((Option.equals(Constants.COMPARE))&&(TypeOption.equals(Constants.DC))) {
			System.out.println("Reading Clients Class from " + ipBcc + " against " + ipBcc2);
			if (comparefunctionDC(ipBcc,usrBcc,passBcc,ipBcc2,usrBcc2,passBcc2,FileNameDst,false,ssh)==Constants.ERROR) {
				System.out.println("Error executing ClientClass function");
			}
		}
		
		/**Compare Client Class Group***/
		if ((Option.equals(Constants.COMPARE))&&(TypeOption.equals(Constants.CCG))) {
			System.out.println("Reading Clients Class from " + ipBcc + " against " + ipBcc2);
			if (comparefunctionCCG(ipBcc,usrBcc,passBcc,ipBcc2,usrBcc2,passBcc2,FileNameDst,false,ssh)==Constants.ERROR) {
				System.out.println("Error executing ClientClass function");
			}
		}
		
	}
	
	
	/**
	 * This function lookup files in a directory
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param List<String> CmdAdd: Array with commands to related rules
	 * @param List<String> list: Array with all commands to execute separated for each word
	 * 
	 *  **/		
	private static int readFilesCMTS(String FileOutName, String directory,boolean nameOption){
		ArrayList<Routing> routeEle = new ArrayList<Routing>();
		ArrayList<Rules> rules = new ArrayList<Rules>();
		ArrayList<RulesIPv6> rules6 = new ArrayList<RulesIPv6>();
		ArrayList<String> filter = new ArrayList<String>();  
		Routing cmts = new Routing();
		try {
			File f = new File ("./" + directory);
			FilenameFilter textFilter = new FilenameFilter() {
				public boolean accept(File dir, String name) {
					String lowercaseName = name.toLowerCase();
					if (lowercaseName.endsWith(".txt")) {
						return true;
					} else {
						return false;
					}
				}
			};

			File[] files = f.listFiles(textFilter);
			for (File file : files) {
				LogWriter.getLogger().info("Reading File " + file.getCanonicalPath());
				cmts = readFileCmts(file.getCanonicalPath(),rules,rules6,nameOption);
				if (cmts != null)
					routeEle.add(cmts);
			}
			
			WriteDiskObjetc WDO = new WriteDiskObjetc();
            if (WDO.WriteObjectListRoutes(routeEle,FileOutName)==Constants.ERROR) {
            	System.out.println("Error write to disk");
            	return Constants.ERROR;
            }else {
            	LogWriter.getLogger().info("Saving RE in file " + routeEle.size());
            }

            
            ArrayList<Routing> routesAux = new ArrayList<Routing>();
            ReadDiskObject RDO = new ReadDiskObject();
            routesAux = RDO.getObjecRoutestDisk(FileOutName,filter);
            LogWriter.getLogger().info("Saving Rules in file " + routesAux.size());
            if (routesAux.size()> 0) {
	            Iterator<Routing> itrRoutesAux = routesAux.iterator();
	            while(itrRoutesAux.hasNext()){
	            	Routing ruleta = itrRoutesAux.next();
	            	LogWriter.getLogger().info(ruleta.toString());
	            	ArrayList<Gateways> g = ruleta.getGateways();
	            	Iterator<Gateways> itrRoutesAuxg = g.iterator();
		            while(itrRoutesAuxg.hasNext()){
		            	Gateways ga = itrRoutesAuxg.next();
		            	LogWriter.getLogger().info("*" + ga.getGateway() + " - "  + ga.getNetwork());
		            }
	            }
	            
            }else {
            	System.out.println("Error to get the object or the file doesn't contain information");
            }
            
           
           
            if (WDO.WriteObjectListRules(rules,"Rules" + FileOutName)==Constants.ERROR) {
            	System.out.println("Error write to disk");
            	return Constants.ERROR;
            }

            
            ArrayList<Rules> rulesAux = new ArrayList<Rules>();
            rulesAux = RDO.getObjecRulestDisk("Rules" + FileOutName,filter);
            if (rulesAux.size()> 0) {
	            Iterator<Rules> itrRulesAux = rulesAux.iterator();
	            while(itrRulesAux.hasNext()){
	            	Rules ruleta = itrRulesAux.next();
	            	LogWriter.getLogger().info("Rule Ipv4" + ruleta.toString());
	            	
	            }
	            
            }else {
            	System.out.println("Error to get the object or the file doesn't contain informationn");
            }
            
            LogWriter.getLogger().info("Size rulesIPv6 " + rules6.size());
            if (WDO.WriteObjectListRulesIPv6(rules6,"RulesIPv6" + FileOutName)==Constants.ERROR) {
            	System.out.println("Error write to disk");
            	return Constants.ERROR;
            }

            
            ArrayList<RulesIPv6> rulesAux6 = new ArrayList<RulesIPv6>();
            rulesAux6 = RDO.getObjecRulesIPv6tDisk("RulesIPv6" + FileOutName,filter);
            if (rulesAux.size()> 0) {
	            Iterator<RulesIPv6> itrRulesAux = rulesAux6.iterator();
	            while(itrRulesAux.hasNext()){
	            	RulesIPv6 ruleta = itrRulesAux.next();
	            	LogWriter.getLogger().info("Rule Ipv6" + ruleta.toString());
	            	
	            }
	            
            }else {
            	System.out.println("Error to get the object or the file doesn't contain information");
            }
            System.out.println("Read files " +  f.listFiles(textFilter).length);
            System.out.println("Exporting Routing elements " +routeEle.size());
            System.out.println("Exporting Rules IPv4 " +rules.size());
            System.out.println("Exporting Rules IPv6 " +rules6.size());
            
		}catch(Exception e) {
			System.out.println("Error Importing data:" + e.getMessage());
			e.printStackTrace();
			return Constants.ERROR;
		}
		return Constants.NO_ERROR;
	}
	
	/**
	 * This function read a CMTS file and create RE and rules Object in a File.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName : CMTS file name
	 * @param ArrayList<Rules> rules: Array Rules object by reference
	 *  **/		
	private static Routing readFileCmts(String FileName,ArrayList<Rules> rules, ArrayList<RulesIPv6> rules6, boolean nameOption  ) {
		boolean bundle=false;
		Routing route = new Routing();
		Gateways gateway = new Gateways();
		Rules rule = new Rules();
		RulesIPv6 rule6 = new RulesIPv6();
		int bandipv6=0;
		NetworkUtils nu = new NetworkUtils();
		ArrayList<Gateways> gateways = new ArrayList<Gateways>();
		HashMap<String, String> hashMapNets = new HashMap<String, String>();
		String CMTSName="";
		String NameRule="";
		int NumberTokns=0;
		if (nameOption)
			NumberTokns=5;
		else
			NumberTokns=4;
		try {
			File file = new File (FileName);
			CMTSName=file.getName().replace(".txt", "");
			route.setName(CMTSName);
			route.setDescription(CMTSName);
			route.setDocsisMajorVersion("3");
			route.setDocsisMinorVersion("1");
			route.setManagementIp("0.0.0.0");
			route.setAuthkey("admin");
			route.setConfirmAuthkey("admin");
			route.setSpreadipAllocation("FALSE");
			route.setExtendedCmtsmicType("MD5");
			//route.setExtendedCmtsmicSharedSecret("admin");
			//route.setConfirmExtendedmicSharedSecret("admin");
			FileReader fr = new FileReader (file);
			BufferedReader br = new BufferedReader(fr);

	        String linea;
	 		String gw="";
	 		String gw6="";
			boolean isGw=false;
			boolean isGw6=false;
			String CidrNotation="";
			String net="";
			String Network="";
			String Network6="";
			int contNets=0;
	         while((linea=br.readLine())!=null) {
	        	 if ((linea.toUpperCase().indexOf("BUNDLE")> -1)||(linea.toUpperCase().indexOf("CABLE-MAC")> -1)){
	        		 bundle=true;
	        		 bandipv6=0;
	        	 }else if ((linea.toUpperCase().indexOf("IP ADDRESS")> -1)&&(bundle)) {
	        		 StringTokenizer tokens=new StringTokenizer(linea, " ");
	        		 int elements=tokens.countTokens();
	        		 while(tokens.hasMoreTokens()){
	        			 rule.clean();
	        			 rule6.clean();
	        			 String str=tokens.nextToken();
	        			 if (nu.validIP(str)&&(elements>NumberTokns)&&(!nu.isValidSubnetMask(str))) {
	        				 net=str;
	        				 contNets++;
	        				 
	        			 }
	        			 if (nu.validIP(str)&&(elements<=NumberTokns)&&(!nu.isValidSubnetMask(str))) {
	        				 isGw=true;
	        				 net =str;
	        				 contNets++;
	        				 
	        			 }
	        				 
	        			 if (nu.isValidSubnetMask(str)) {
	        				 final SubnetUtils utils = new SubnetUtils(net,str);
	        				 final SubnetInfo info = utils.getInfo();
	        				 CidrNotation= info.getCidrSignature();
	        				 
	        			 }
	        			 if (isGw&&!("".equals(CidrNotation))) {
	        				 gw = CidrNotation;
	        				 if (nameOption) {
        						 String[] splitString = linea.split(" ");
        						 NameRule=CMTSName+ "_" + splitString[splitString.length-1];
        					 }else {
        						 NameRule=CMTSName+ "_" + contNets; 
        					 }
	        				 hashMapNets.put(gw, NameRule);
	        				 isGw=false;
	        				 CidrNotation="";
	        			 }else if (!("".equals(CidrNotation))){
	        				 if (!"".equals(Network))
	        					 Network=Network+",";
	        				 Network=Network+CidrNotation+":1:FALSE" ;
	        				 if (nameOption) {
        						 String[] splitString = linea.split(" ");
        						 NameRule=CMTSName+ "_" + splitString[splitString.length-1];
        					 }else {
        						 NameRule=CMTSName+ "_" + contNets; 
        					 }
	        				 hashMapNets.put(CidrNotation, NameRule);
	        				 net="";
	        				 CidrNotation="";
	        			 }
	        		 }
	        	 }else if ((linea.toUpperCase().indexOf("IPV6 ADDRESS")> -1)&&(bundle)) {
	        		 bandipv6++;
	        		 
	        		 StringTokenizer tokens=new StringTokenizer(linea, " ");
	        		 while(tokens.hasMoreTokens()){
	        			 rule.clean();
	        			 String str=tokens.nextToken();
	        			 if ((str.indexOf(":")>-1)&&(bandipv6>1)) {
	        				 IPv6Network net6 = IPv6Network.fromString(str);
	        				 if (net6.getNetmask().asPrefixLength()<127) {
	        					 net=str;
	        					 CidrNotation=String.valueOf(net6.getNetmask().asPrefixLength());
	        					 contNets++;
		        				 if (nameOption) {
	        						 String[] splitString = linea.split(" ");
	        						 NameRule=CMTSName+ "_" + splitString[splitString.length-1];
	        					 }else {
	        						 NameRule=CMTSName+ "_" + contNets; 
	        					 }
		        				 hashMapNets.put(str, NameRule);
	        				 }
	        			 }
	        				 
	        			 if ((str.indexOf(":")>-1)&&(bandipv6==1)) {
	        				 IPv6Network net6 = IPv6Network.fromString(str);
	        				 if (net6.getNetmask().asPrefixLength()<127) {
	        					 isGw6=true;
	        					 net =str;
	        					 CidrNotation=String.valueOf(net6.getNetmask().asPrefixLength());
	        					 contNets++;
		        				 if (nameOption) {
	        						 String[] splitString = linea.split(" ");
	        						 NameRule=CMTSName+ "_" + splitString[splitString.length-1];
	        					 }else {
	        						 NameRule=CMTSName+ "_" + contNets; 
	        					 }
		        				 hashMapNets.put(str, NameRule);
	        				 }
	        			 }
	        				 
	        			 if (isGw6&&!("".equals(CidrNotation))) {
	        				 gw6 = net;
	        				 isGw6=false;
	        				 CidrNotation="";
	        			 }else if (!("".equals(CidrNotation))){
	        				 if (!"".equals(Network6))
	        					 Network6=Network6+",";
	        				 Network6=Network6+net+":*1" ;
	        				 net="";
	        				 CidrNotation="";
	        			 }
	        		 }
	        		 
	         	 }else if (linea.toUpperCase().indexOf("HELPER-ADDRESS")>-1) {
	        		 bundle=false;
	        		 if (!"".equals(gw)) {
		        		 gateway.setGateway(gw);
		        		 gateway.setNetwork(Network);
		        		 Gateways gAux = new Gateways();
	                 	 try {
	                 		gAux = (Gateways) gateway.clone();
	                 	 }catch (CloneNotSupportedException cnse) {
	                         System.out.println("Cannot be cloned");
	                     }
	                 	 gateways.add(gAux);
	        		 }
	        		 if (!"".equals(gw6)) {
		        		 gateway.setGateway(gw6);
		        		 gateway.setNetwork(Network6.replace('*', ' ').replace(" ", ""));
		        		 Gateways gAux = new Gateways();
	                 	 try {
	                 		gAux = (Gateways) gateway.clone();
	                 	 }catch (CloneNotSupportedException cnse) {
	                         System.out.println("Cannot be cloned");
	                     }
	                 	 gateways.add(gAux);
	        		 }
	        		 
                 	if ((!"".equals(gw.trim()))) {
                 		addRulesCMTS(rules, rule, gw,hashMapNets.get(gw));
                 	}
                 	LogWriter.getLogger().info("Gw" + gw6 + " Index" + gw.indexOf(":"));
                 	
                 	if ((!"".equals(gw6.trim()))&&(gw6.indexOf(":")>-1)) {
                 		
                 		addRulesCMTSV6(rules6, rule6, gw6,hashMapNets.get(gw6));
                 	}
                 	
       			     StringTokenizer tokens=new StringTokenizer(Network, ",");
	        		 while(tokens.hasMoreTokens()){
	        			 String str=tokens.nextToken();
	        			 str =str.replace(":1:FALSE", "");
	        			 LogWriter.getLogger().info("-"+str+"-"+str.indexOf(":"));
	        			 if ((!"".equals(str.trim()))&&(str.indexOf(":")<1)) {
	        				 addRulesCMTS(rules, rule, str,hashMapNets.get(str));
	        			 }
	        			 if ((!"".equals(str.trim()))&&(str.indexOf(":")>1)) {
	        				 addRulesCMTSV6(rules6, rule6, str.replace(":*1", ""),hashMapNets.get(str.replace(":*1", "")));
	        			 }
	        		 }
                 	 Network="";
	        		 gw="";
	        		 StringTokenizer tokens6=new StringTokenizer(Network6, ",");
	        		 while(tokens6.hasMoreTokens()){
	        			 String str=tokens6.nextToken();
	        			 addRulesCMTSV6(rules6, rule6, str.replace(":*1", ""),hashMapNets.get(str.replace(":*1", "")));
	        		 }
	        	 }
	         }
	         route.setGateways(gateways);
	         br.close();
	      }catch(Exception e) {
				System.out.println("Error Importing data:" + e.getMessage());
				e.printStackTrace();
				return null;
			}
	return route;
	}

	/**
	 * This function add dummy data to rule object
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules
	 * @param Rules rule
	 * @param String gw
	 *  **/		
	private static void addRulesCMTS(ArrayList<Rules> rules, Rules rule, String gw, String Name) {
		
		final SubnetUtils utils = new SubnetUtils(gw);
		final SubnetInfo info = utils.getInfo();
		String ipFrom= info.getAllAddresses()[1];
		String ipto=info.getHighAddress();
		 rule.setName(Name);
		 rule.setDescription(gw);
		 rule.setIpfrom(ipFrom);
		 rule.setIpto(ipto);
		 rule.setDefaultgw(info.getAddress());
		 rule.setSubnetmask(info.getNetmask());
		 rule.setIsgiaddrreservable("FALSE");
		 rule.setReservedtogiaddr("0.0.0.0");
		 rule.setRequireslegalhostname("FALSE");
		 rule.setRequireslegalhostnamebyinheritance("FALSE");
		 rule.setDonotsendhost("FALSE");
		 rule.setDonotsendhostbyinheritance("FALSE");
		 rule.setDisabled("FALSE");
		 rule.setDisabledbyinheritance("FALSE");
		 rule.setReserved("FALSE");
		 rule.setReservedbyinheritance("FALSE");
		 rule.setPingbefore("FALSE");
		 rule.setPingbeforebyinheritance("FALSE");
		 rule.setIplimitselector("NONE");
		 rule.setDefaultiplimit("0");
		 rule.setShuffleip("FALSE");
		 rule.setShuffleipbyinheritance("FALSE");
		 rule.setInformonly("FALSE");
		 rule.setInformonlybyinheritance("FALSE");
		 rule.setIpspoofing("FALSE");
		 rule.setIpspoofingbyinheritance("FALSE");
		 rule.setSuppressnakresponse("FALSE");
		 rule.setSuppressnakresponsebyinheritance("FALSE");
		 rule.setCriteria("");
		 rule.setAllocable("FALSE");
		 rule.setBrokenpolicy("FALSE");
		 rule.setLeasetime("10800");
		 rule.setEnabledns("FALSE");
		 rule.setInheritdnssettings("FALSE");
		 Rules ruleAux = new Rules();
		 try {
			 ruleAux = (Rules) rule.clone();
		 }catch (CloneNotSupportedException cnse) {
			System.out.println("Cannot be cloned");
		 }
		 rules.add(ruleAux);
	}
	
	/**
	 * This function add dummy data to rule object
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules
	 * @param Rules rule
	 * @param String gw
	 *  **/		
	private static void addRulesCMTSV6(ArrayList<RulesIPv6> rules, RulesIPv6 rule, String gw, String name) {
		IPv6Network net = IPv6Network.fromString(gw);
		IPv6AddressRange range = net.fromFirstAndLast(net.getFirst(), net.getLast());
		range.iterator().next();
		String ipFrom= range.iterator().next().add(0).toLongString();
		String ipto=net.getLast().add(-1).toLongString();
		rule.setName(name);
		rule.setDescription(gw);
		rule.setStartiIpAddress(ipFrom);
		rule.setEndIpAddress(ipto);
		rule.setDisabled("FALSE");
		rule.setCriteria("");
		rule.setNonSequential("TRUE");
		rule.setPreferredLifeTime("604800");
		rule.setRapidCommit("FALSE");
		rule.setValidlifeTime("2592000");
		RulesIPv6 ruleAux = new RulesIPv6();
		try {
			 ruleAux = (RulesIPv6) rule.clone();
		}catch (CloneNotSupportedException cnse) {
			System.out.println("Cannot be cloned");
		}
		rules.add(ruleAux);
	}
	

	
	/**
	 * This function related rules in BCC through commands
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param List<String> CmdAdd: Array with commands to related rules
	 * @param List<String> list: Array with all commands to execute separated for each word
	 * 
	 *  **/		
	private static void relateRuleBCC(String ip, String user, String pass, List<String> CmdAdd, List<String> list)
			throws IOException {
		ProcessBuilder builder;
		Process proc;
		InputStream is2;
		BufferedReader br2;
		list.add("ipcli");
		list.add("-n");
		list.add(user);
		list.add("-p");
		list.add(pass);
		list.add( "-s");
		list.add(ip);
		for (int n=0; n < CmdAdd.size(); n++) {
			list.add(CmdAdd.get(n));
		} 
		builder = new ProcessBuilder(list);
		proc = builder.start();                	
		is2 = proc.getInputStream();
		br2 = new BufferedReader (new InputStreamReader (is2));
		LogWriter.getLogger().info( br2.readLine());
		list.clear();
	}
	
	/**
	 * This function searches a route in ArrayList and returns if the name exists in the object.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Ruoutes
	 * @param String Name: Route name to search
	 *  **/	
	private static long searchRouteByName(ArrayList<Routing> routes, String Name) {
		long found=0;
		Iterator<Routing> itrRules = routes.iterator();
        while(itrRules.hasNext()){
        	Routing rulet = itrRules.next();
        	if (rulet.getName().trim().equals(Name.trim())) {
        		found= Long.parseLong(rulet.getLookUpKey());
        		break;
        	}      	
        }
        return found;
	}
	
	/**
	 * This function searches a rule in ArrayList and returns if the name exists in the object.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Rules
	 * @param String Name: Rule name to search
	 *  **/
	private static String searchRuleByLookUpKey(ArrayList<Rules> rules, String lookupKey) {
		String name="";
		Iterator<Rules> itrRules = rules.iterator();
        while(itrRules.hasNext()){
        	Rules rulet = itrRules.next();
        	if (rulet.getLookupkey().trim().equals(lookupKey.trim())) {
        		name = rulet.getName();
        		break;
        	}      	
        }
        return name;
	}

	/**
	 * This function searches a rule in ArrayList and returns if the name exists in the object.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Rules
	 * @param String Name: Rule name to search
	 *  **/
	private static long searchRuleByName(ArrayList<Rules> rules, String Name) {
		long found=0;
		Iterator<Rules> itrRules = rules.iterator();
        while(itrRules.hasNext()){
        	Rules rulet = itrRules.next();
        	if (rulet.getName().trim().equals(Name.trim())) {
        		found= Long.parseLong(rulet.getLookupkey());
        		break;
        	}      	
        }
        return found;
	}
	
	/**
	 * This function searches a rule in ArrayList and returns if the name exists in the object.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Rules
	 * @param String Name: Rule name to search
	 *  **/
	private static long searchRuleV6ByName(ArrayList<RulesIPv6> rules, String Name) {
		long found=0;
		Iterator<RulesIPv6> itrRules = rules.iterator();
        while(itrRules.hasNext()){
        	RulesIPv6 rulet = itrRules.next();
        	if (rulet.getName().trim().equals(Name.trim())) {
        		found= Long.parseLong(rulet.getLookupkey());
        		break;
        	}      	
        }
        return found;
	}
	/**
	 * This function searches a staticAddress in ArrayList and returns if the name exists in the object.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Rules
	 * @param String Name: Rule name to search
	 *  **/
	private static long searchStaticAddByName(ArrayList<StaticAddress> staticAdds, String Name) {
		long found=0;
		Iterator<StaticAddress> itrStaticAdd = staticAdds.iterator();
        while(itrStaticAdd.hasNext()){
        	StaticAddress rulet = itrStaticAdd.next();
        	if (rulet.getName().trim().equals(Name.trim())) {
        		found= Long.parseLong(rulet.getLookupkey());
        		break;
        	}      	
        }
        return found;
	}
	/**
	 * This function searches a rule in ArrayList and returns if the name exists in the object.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Rules
	 * @param String Name: Rule name to search
	 *  **/
	private static String searchTemplateByLookUpKay(ArrayList<Templates> templates, String lookupKey) {
		String name="";
		Iterator<Templates> itrTemplates = templates.iterator();
        while(itrTemplates.hasNext()){
        	Templates rulet = itrTemplates.next();
        	if (rulet.getLookupkey().trim().equals(lookupKey.trim())) {
        		name = rulet.getName();
        		break;
        	}       	
        }
        return name;
	}
	
	/**
	 * This function searches a template in ArrayList and returns if the name exists in the object.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param ArrayList<Rules> rules : ArrayList with the object Rules
	 * @param String Name: Rule name to search
	 *  **/
	private static long searchTemplateByName(ArrayList<Templates> templates, String name) {
		long lookupkey=0;
		Iterator<Templates> itrTemplates = templates.iterator();
        while(itrTemplates.hasNext()){
        	Templates rulet = itrTemplates.next();
        	if (rulet.getName().trim().equals(name.trim())) {
        		lookupkey = Long.valueOf(rulet.getLookupkey());
        		break;
        	}       	
        }
        return lookupkey;
	}
	
	
	/**
	 * This function contains all logic for know if it's necessary delete or add a rules IPv6
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileOrig: Is the name of the read File where is save the object previously export. 
	 * @return int Constant with Error o No Error.
	 *  **/		
	public static int set_functon_rules6(String ip, String user, String pass, String FileOrig, int option, ArrayList<String> filter,String Debug) {
		ArrayList<RulesIPv6> rules = new ArrayList<RulesIPv6>();
		ArrayList<RulesIPv6> rulesBcc = new ArrayList<RulesIPv6>();
		ArrayList<RulesIPv6> rulesAdd = new ArrayList<RulesIPv6>();
		int ruleDele=0; int ruladd=0; int ruleFile=0; int ruleBcc=0;
		String auxString="";
		
		try {
			rulesAdd.clear();
			String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULEV6 ";
			getObjectRulesV6(rulesBcc, command);
			ruleBcc = rulesBcc.size();
			if (option == Constants.DELETEALL) {
				Iterator<RulesIPv6> itrRules = rulesBcc.iterator();
	            while(itrRules.hasNext()){      
	            	RulesIPv6 rulet = itrRules.next();
	            	auxString='"'+rulet.getName().trim() +'"'+ "|lookupkey=" + rulet.getLookupkey().trim();
	            	delRuleV6BCC(ip, user, pass, auxString);
	            	ruleDele++;
	            }
	            rules.clear();
			}
			
	        ReadDiskObject RDO = new ReadDiskObject();
	        rules = RDO.getObjecRulesIPv6tDisk(FileOrig,filter);
	        List<String> CmdAdd= new ArrayList<>();
	        ruleFile=rules.size();
	        if (rules.size()> Constants.NODATA) {
	            Iterator<RulesIPv6> itrRulesAux = rules.iterator();
	            while(itrRulesAux.hasNext()){
	            	RulesIPv6 ruleta = itrRulesAux.next();
	            	CmdAdd.clear();
	            	CmdAdd.add(ruleta.getName().trim());
	            	CmdAdd.add("DESCRIPTION");CmdAdd.add(ruleta.getDescription());
	            	if (!"".equals(ruleta.getCriteria())) {
	            		CmdAdd.add("CRITERIA");CmdAdd.add( ruleta.getCriteria());}
	            	CmdAdd.add("DISABLED");CmdAdd.add(ruleta.getDisabled());
	            	CmdAdd.add("ENDIPADDRESS");CmdAdd.add( ruleta.getEndIpAddress());
	            	CmdAdd.add("STARTIPADDRESS");CmdAdd.add(ruleta.getStartiIpAddress());
	            	//CmdAdd.add("NONSEQUENTIAL");CmdAdd.add(ruleta.getNonSequential());
	            	CmdAdd.add("PREFERREDLIFETIME");CmdAdd.add(ruleta.getPreferredLifeTime());
	            	CmdAdd.add("RAPIDCOMMIT");CmdAdd.add(ruleta.getRapidCommit());
	            	CmdAdd.add("VALIDLIFETIME");CmdAdd.add(ruleta.getValidlifeTime());
	            	long LookupKey=searchRuleV6ByName(rulesBcc, ruleta.getName());
					List<String> list = new ArrayList<>();
					if (LookupKey> Constants.NODATA) {               	
	                	if (option==Constants.ADD) {
	                		LogWriter.getLogger().info("Nothing to do for " + ruleta.getName());
	                	}
	                	if (option==Constants.OVERWRITE) {
	                		/**Deleting rule**/
	                		auxString='"'+ruleta.getName().trim() +'"'+ "|lookupkey=" + LookupKey;
	                		delRuleV6BCC(ip, user, pass, auxString);
	                		ruleDele++;
	                		list.clear();
	        	            /**Creating rule**/
	                		addRuleV6BCC(ip, user, pass, CmdAdd, list);
	                    	rulesAdd.add(ruleta);
	                    	ruladd++;

	                	}
	                	if (option==3) {
	                		addRuleV6BCC(ip, user, pass, CmdAdd, list);
	                		rulesAdd.add(ruleta);
	                		ruladd++;
	                	}
	                }else {
	                	//System.out.println(ruleta.getName() + "|NOTFound" );
	                	addRuleV6BCC(ip, user, pass, CmdAdd, list);
	                	rulesAdd.add(ruleta);
	                	ruladd++;
	                }
	            }
	        }else {
	        	System.out.println("Error to obtain object or the file does not contain information");
	        }
	        /**Relating Rules**/
/**	        List<String> list = new ArrayList<>();
	        int ruleRela=0;
	        for (int j=0; j<rulesAdd.size();j++) {
	        	if (!"".equals(rulesAdd.get(j).getParentName())){
		        	CmdAdd.clear();
		        	list.clear();
		        	CmdAdd.add("RELATE");
		        	CmdAdd.add("RULE");
		        	CmdAdd.add(rulesAdd.get(j).getParentName());
		        	CmdAdd.add("RULE");
		        	CmdAdd.add(rulesAdd.get(j).getName());
		        	relateRuleBCC(ip, user, pass, CmdAdd, list);
		        	ruleRela++;
	        	}
			}
**/	        
	        String commandP="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULEV6 ";
			rulesBcc.clear();
			getObjectRulesV6(rulesBcc, commandP);
			int ruleBccPos = rulesBcc.size();
	        System.out.println("++Import Summary++");
	        System.out.println("Rules IPv6 in BCC previuos import " +  ruleBcc);
	        System.out.println("Rules IPv6 delete " + ruleDele);
	        System.out.println("Rules IPv6 add " + ruladd);
//	        System.out.println("Related rules " + ruleRela );
	        System.out.println("Rules IPv6 in File " + ruleFile);
	        System.out.println("Rules IPv6 in BCC later import " +  ruleBccPos);
	        
	        return Constants.NO_ERROR;
		}catch(Exception e) {
			System.out.println("Error Importing data:" + e.getMessage());
			e.printStackTrace();
			return Constants.ERROR;
		}
	}
	
	
	
	/**
	 * This function contains all logic for know if it's necessary delete or add a rules
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileOrig: Is the name of the read File where is save the object previously export. 
	 * @return int Constant with Error o No Error.
	 *  **/		
	public static int set_functon_rules(String ip, String user, String pass, String FileOrig, int option,ArrayList<String>  filter,String Debug) {
		ArrayList<Rules> rules = new ArrayList<Rules>();
		ArrayList<Rules> rulesBcc = new ArrayList<Rules>();
		ArrayList<Rules> rulesAdd = new ArrayList<Rules>();
		int ruleDele=0; int ruladd=0; int ruleFile=0; int ruleBcc=0;
		String auxString="";
		
		try {
			rulesAdd.clear();
			String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULE ";
			getObjectRules(rulesBcc, command);
			ruleBcc = rulesBcc.size();
			if (option == Constants.DELETEALL) {
				Iterator<Rules> itrRules = rulesBcc.iterator();
	            while(itrRules.hasNext()){      
	            	Rules rulet = itrRules.next();
	            	auxString='"'+rulet.getName().trim() +'"'+ "|lookupkey=" + rulet.getLookupkey().trim();
	            	delRuleBCC(ip, user, pass, auxString);
	            	ruleDele++;
	            }
	            rules.clear();
			}
			
	        ReadDiskObject RDO = new ReadDiskObject();
	        rules = RDO.getObjecRulestDisk(FileOrig,filter);
	        List<String> CmdAdd= new ArrayList<>();
	        ruleFile=rules.size();
	        if (rules.size()> Constants.NODATA) {
	            Iterator<Rules> itrRulesAux = rules.iterator();
	            while(itrRulesAux.hasNext()){
	            	Rules ruleta = itrRulesAux.next();
	            	CmdAdd.clear();
	            	CmdAdd.add(ruleta.getName().trim());
	            	CmdAdd.add("DESCRIPTION");CmdAdd.add(ruleta.getDescription());
	            	CmdAdd.add("IPFROM");CmdAdd.add( ruleta.getIpfrom());
	            	CmdAdd.add("IPTO");CmdAdd.add(ruleta.getIpto());
	            	CmdAdd.add("ISGIADDRRESERVABLE");CmdAdd.add(ruleta.getIsgiaddrreservable());
	            	CmdAdd.add("GIADDRRESERVEDTIMEREMAINING");CmdAdd.add(ruleta.getGiaddrreservedtimeremaining());
	            	CmdAdd.add("REQUIRESLEGALHOSTNAME");CmdAdd.add(ruleta.getRequireslegalhostname());
	            	CmdAdd.add("DONOTSENDHOST");CmdAdd.add(ruleta.getDonotsendhost());
	            	CmdAdd.add("DISABLED");CmdAdd.add(ruleta.getDisabled());
	            	CmdAdd.add("RESERVED");CmdAdd.add(ruleta.getReserved());
	            	CmdAdd.add("PINGBEFORE");CmdAdd.add(ruleta.getPingbefore());
	            	CmdAdd.add("IPLIMITSELECTOR");CmdAdd.add(ruleta.getIplimitselector());
					if (!"NONE".equals(ruleta.getIplimitselector())) {
						CmdAdd.add("DEFAULTIPLIMIT");
						CmdAdd.add( ruleta.getDefaultiplimit());
					}
					CmdAdd.add("SHUFFLEIP");CmdAdd.add(ruleta.getShuffleip());
					CmdAdd.add("INFORMONLY");CmdAdd.add(ruleta.getInformonly());
					CmdAdd.add("IPSPOOFING");CmdAdd.add(ruleta.getIpspoofing());
					CmdAdd.add("SUPPRESSNAKRESPONSE");CmdAdd.add(ruleta.getSuppressnakresponse());
					CmdAdd.add("CRITERIA");CmdAdd.add( ruleta.getCriteria());
					CmdAdd.add("LEASETIME");CmdAdd.add(ruleta.getLeasetime());
					if (!"0.0.0.0".equals(ruleta.getDefaultgw())) {
						CmdAdd.add("DEFAULTGW");
						CmdAdd.add(ruleta.getDefaultgw());
					}
					CmdAdd.add("SUBNETMASK");CmdAdd.add(ruleta.getSubnetmask());
					CmdAdd.add("ENABLEDNS");CmdAdd.add( ruleta.getEnabledns());
					if (!"".equals(ruleta.getDdnsserver())) {
						CmdAdd.add("DDNSSERVER");
						CmdAdd.add( ruleta.getDdnsserver());
					}
					CmdAdd.add("INHERITDNSSETTINGS");CmdAdd.add( ruleta.getInheritdnssettings());
					long LookupKey=searchRuleByName(rulesBcc, ruleta.getName());
					List<String> list = new ArrayList<>();
					if (LookupKey> Constants.NODATA) {               	
	                	//System.out.println(ruleta.getName() + "|Found");
	                	if (option==Constants.ADD) {
	                		LogWriter.getLogger().info("Nothing to do for " + ruleta.getName());
	                	}
	                	if (option==Constants.OVERWRITE) {
	                		/**Deleting rule**/
	                		auxString='"'+ruleta.getName().trim() +'"'+ "|lookupkey=" + LookupKey;
	                		delRuleBCC(ip, user, pass, auxString);
	                		ruleDele++;
	                		list.clear();
	        	            /**Creating rule**/
	                    	addRuleBCC(ip, user, pass, CmdAdd, list);
	                    	rulesAdd.add(ruleta);
	                    	ruladd++;

	                	}
	                	if (option==3) {
	                		addRuleBCC(ip, user, pass, CmdAdd, list);
	                		rulesAdd.add(ruleta);
	                		ruladd++;
	                	}
	                }else {
	                	//System.out.println(ruleta.getName() + "|NOTFound" );
	                	addRuleBCC(ip, user, pass, CmdAdd, list);
	                	rulesAdd.add(ruleta);
	                	ruladd++;
	                }
	            }
	        }else {
	        	System.out.println("Error to obtain object or the file does not contain information");
	        }
	        /**Relating Rules**/
	        List<String> list = new ArrayList<>();
	        int ruleRela=0;
	        for (int j=0; j<rulesAdd.size();j++) {
	        	if (!"".equals(rulesAdd.get(j).getParentName())){
		        	CmdAdd.clear();
		        	list.clear();
		        	CmdAdd.add("RELATE");
		        	CmdAdd.add("RULE");
		        	CmdAdd.add(rulesAdd.get(j).getParentName());
		        	CmdAdd.add("RULE");
		        	CmdAdd.add(rulesAdd.get(j).getName());
		        	relateRuleBCC(ip, user, pass, CmdAdd, list);
		        	ruleRela++;
	        	}
			}
	        
	        String commandP="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list RULE ";
			rulesBcc.clear();
			getObjectRules(rulesBcc, commandP);
			int ruleBccPos = rulesBcc.size();
	        System.out.println("++Import Summary++");
	        System.out.println("Rules in BCC previuos import " +  ruleBcc);
	        System.out.println("Rules delete " + ruleDele);
	        System.out.println("Rules add " + ruladd);
	        System.out.println("Related rules " + ruleRela );
	        System.out.println("Rules in File " + ruleFile);
	        System.out.println("Rules in BCC later import " +  ruleBccPos);
	        
	        return Constants.NO_ERROR;
		}catch(Exception e) {
			System.out.println("Error Importing data:" + e.getMessage());
			e.printStackTrace();
			return Constants.ERROR;
		}
	}
	

	/**
	 * This function contains all logic for know if it's necessary delete or add a Route and his gateways for each routing element
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileOrig: Is the name of the read File where is save the object previously export. 
	 * @return int Constant with Error o No Error.
	 *  **/		
	public static int setRoutingElements(String ip, String user, String pass, String FileOrig, int option,ArrayList<String> filter, String Debug) {
		ArrayList<Routing> routesBcc = new ArrayList<Routing>();
		ArrayList<Routing> routesAdd = new ArrayList<Routing>();
		int routeAdd=0;
		int routeBcc=0;
		int routesDel=0;
		int contGw=0;
		/**First is obtained the information before import**/
		
		String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list ROUTINGELEMENT ";
		try {
			getObjectRoutes(routesBcc, command);
			routeBcc = routesBcc.size();
			/**Option 3 Delete All**/
			if (option == 3) {
				List<String> CmdAdd= new ArrayList<>();
				for (int i=0; i<routesBcc.size();i++) {
					CmdAdd.clear();
					CmdAdd.add(routesBcc.get(i).getName() );
					CmdAdd.add("|lookupkey=");
					CmdAdd.add(routesBcc.get(i).getLookUpKey());
					String aux = '"'+routesBcc.get(i).getName() + '"' +  " |lookupkey=" +routesBcc.get(i).getLookUpKey() ;
					delRuoteBCC(ip,user,pass,aux,CmdAdd);
					routesDel++;
				}
				
			}
			/**It reads the object from the file**/
			List<String> CmdAdd= new ArrayList<>();
			ArrayList<Routing> routesFile = new ArrayList<Routing>();
	        ReadDiskObject RDO = new ReadDiskObject();
	        routesFile = RDO.getObjecRoutestDisk(FileOrig,filter);
	        if (routesFile.size()> 0) {
	            Iterator<Routing> itrRoutesAux = routesFile.iterator();
	            while(itrRoutesAux.hasNext()){
	            	Routing ruleta = itrRoutesAux.next();
	            	CmdAdd.clear();
	            	CmdAdd.add(ruleta.getName().trim());
	            	CmdAdd.add("DESCRIPTION");CmdAdd.add(ruleta.getDescription());
	            	CmdAdd.add("AUTHKEY");CmdAdd.add(ruleta.getAuthkey());
	            	CmdAdd.add("CONFIRMAUTHKEY");CmdAdd.add(ruleta.getConfirmAuthkey());
	            	CmdAdd.add("DOCSISMAJORVERSION");CmdAdd.add(ruleta.getDocsisMajorVersion());
	            	CmdAdd.add("DOCSISMINORVERSION");CmdAdd.add(ruleta.getDocsisMinorVersion());
	            	CmdAdd.add("EXTENDEDCMTSMICTYPE");CmdAdd.add(ruleta.getExtendedCmtsmicType());
	            	CmdAdd.add("EXTENDEDCMTSMICSHAREDSECRET");CmdAdd.add(ruleta.getExtendedCmtsmicSharedSecret());
	            	CmdAdd.add("CONFIRMEXTENDEDMICSHAREDSECRET");CmdAdd.add(ruleta.getConfirmExtendedmicSharedSecret());
	            	CmdAdd.add("MANAGEMENTIP");CmdAdd.add(ruleta.getManagementIp());
	            	CmdAdd.add("SPREADIPALLOCATION");CmdAdd.add(ruleta.getSpreadipAllocation());
	            	long LookupKey=searchRouteByName(routesBcc, ruleta.getName());
					List<String> list = new ArrayList<>();
					if (LookupKey> 0) {
						/**If option 1 only add the routing elements missing on BCC**/
						if (option==Constants.ADD) {
							LogWriter.getLogger().info("Nothing to do for " + ruleta.getName());
						}
						/**if option 2 then delete and create again the route with the gateways**/
						if (option==Constants.OVERWRITE) {
							List<String> CmdAdd2= new ArrayList<>();
							CmdAdd2.clear();
							CmdAdd2.add(ruleta.getName()  );
							CmdAdd2.add("|lookupkey=");
							CmdAdd2.add(ruleta.getLookUpKey());
							String aux2 = '"'+ruleta.getName() + '"' +  " |lookupkey=" +ruleta.getLookUpKey() ;
							delRuoteBCC(ip,user,pass,aux2,CmdAdd2);
							routesDel++;
							
							addRouteBCC(ip, user, pass, CmdAdd, list);
		                	routesAdd.add(ruleta);
		                	String cmdGw="";
		                	for(int i=0; i< ruleta.getGateways().size();i++) {
		                		cmdGw = "ADD GATEWAYLIST " + ruleta.getGateways().get(i).getGateway() ;
		                		if (!"".equals(ruleta.getGateways().get(i).getNetwork().trim()))
		                			cmdGw = cmdGw  + " network " + ruleta.getGateways().get(i).getNetwork();
		                		addGwBCC(ip, user,pass,ruleta.getName(), cmdGw);
		                		contGw++;
		                	}
		                	routeAdd++;
							
						}
						/**With the option 3 create the routing previously delete perhaps never enter on these option**/
						if (option==Constants.DELETEALL) {
							addRouteBCC(ip, user, pass, CmdAdd, list);
		                	routesAdd.add(ruleta);
		                	String cmdGw="";
		                	for(int i=0; i< ruleta.getGateways().size();i++) {
		                		cmdGw = "ADD GATEWAYLIST " + ruleta.getGateways().get(i).getGateway() ;
		                		if (!"".equals(ruleta.getGateways().get(i).getNetwork().trim()))
		                			cmdGw = cmdGw  + " network " + ruleta.getGateways().get(i).getNetwork();
		                		addGwBCC(ip, user,pass,ruleta.getName(), cmdGw);
		                		contGw++;
		                	}
		                	routeAdd++;
						}
						if (option ==Constants.COMPLEMENT) {
							boolean gwexist=false;
							int index=0;
							//System.out.println("Opcion Compleemento++++++++++++++"   );
							routesAdd.add(ruleta);
		                	String cmdGw="";
		                	ArrayList<Gateways> gwBcc = new ArrayList<Gateways>();
	                		gwBcc = getGateways(ip, user, pass, ruleta.getName());
		                	for(int i=0; i< ruleta.getGateways().size();i++) {
		                		//System.out.println("Buscando ..." + ruleta.getGateways().get(i).getGateway() + " sixe " + gwBcc.size());
		                		for (int j=0; j< gwBcc.size(); j++) {
		                			//System.out.println("Buscando ..." + ruleta.getGateways().get(i).getGateway() ,  gwBcc.get(j).getGateway());
			                		if (gwBcc.get(j).getGateway().trim().equals(ruleta.getGateways().get(i).getGateway().trim())) {
			                			//System.out.println("<<<<<<<<<<<<<<Existe GW"  +  ruleta.getGateways().get(i).getGateway() );
			                			gwexist=true;
			                			index=j;
			                			continue;
			                		}
		                		}
		                		if (!gwexist) {
		                			cmdGw = "ADD GATEWAYLIST " + ruleta.getGateways().get(i).getGateway() ;
		                			if (!"".equals(ruleta.getGateways().get(i).getNetwork().trim()))
		                			cmdGw = cmdGw  + " network " + ruleta.getGateways().get(i).getNetwork();
		                			addGwBCC(ip, user,pass,ruleta.getName(), cmdGw);
		                			contGw++;
		                		}else {
		                			String netf="";
		                			if (gwBcc.get(index).getNetwork().indexOf("::")<0)
		                				netf=  gwBcc.get(index).getNetwork().replaceAll(",", ":FALSE,") + ":FALSE" ;
		                			
		          
		                			//System.out.println("Buscando ..." + ruleta.getGateways().get(i).getNetwork() ,  netf );
		                			
		                			if (ruleta.getGateways().get(i).getNetwork().trim().equals(netf.trim())) {
		                				LogWriter.getLogger().info("Nothing to do for " + ruleta.getGateways().get(i).getGateway());
		                			}else {
		                				String net = gwBcc.get(index).getNetwork().replace(ruleta.getGateways().get(i).getNetwork(), "");
		                				//System.out.println("<<<<<<<<<<<<<<Add Network"  + net);
		                			}
		                		}
		                		gwexist=false;
		                	}
		                	routeAdd++;
						}
						
					}else {
						/**If the object does not exist on BCC then it create it **/
	                	addRouteBCC(ip, user, pass, CmdAdd, list);
	                	routesAdd.add(ruleta);
	                	String cmdGw="";
	                	for(int i=0; i< ruleta.getGateways().size();i++) {
	                		cmdGw = "ADD GATEWAYLIST " + ruleta.getGateways().get(i).getGateway() ;
	                		if (!"".equals(ruleta.getGateways().get(i).getNetwork().trim()))
	                			cmdGw = cmdGw  + " network " + ruleta.getGateways().get(i).getNetwork();
	                		addGwBCC(ip, user,pass,ruleta.getName(), cmdGw);
	                		contGw++;
	                	}
	                	routeAdd++;
					}
	            	if (Constants.LOGON.equals(Debug)) {
	            		System.out.println(ruleta.getName() + " - "  + ruleta.getLookUpKey());
	            		ArrayList<Gateways> g = ruleta.getGateways();
	            		Iterator<Gateways> itrRoutesAuxg = g.iterator();
	            		while(itrRoutesAuxg.hasNext()){
	            			Gateways ga = itrRoutesAuxg.next();
	            			System.out.println("*GW " + ga.getGateway() + " - NET "  + ga.getNetwork());
	            		}
	            	}
	            }
	            
	        }else {
	        	System.out.println("Error al obtener objeto o el arhcivo no contiene informacion");
	        	return Constants.ERROR;
	        }
	        String commandP="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list ROUTINGELEMENT ";
			routesBcc.clear();
			getObjectRoutes(routesBcc, commandP);
			int routesBccPos = routesBcc.size();
	        System.out.println("++Import Summary++");
	        System.out.println("Routes Elements in BCC previuos import " +  routeBcc);
	        System.out.println("Routes Elements delete " + routesDel);
	        System.out.println("Routes Elements add " + routeAdd);
	        System.out.println("Gateways add " + contGw );
	        System.out.println("Routes Elements in File " + routesFile.size());
	        System.out.println("Routes Elements in BCC later import " +  routesBccPos);
	        
		}catch(Exception e) {
			System.out.println("Error importing routes elements " + e.getMessage());
			e.printStackTrace();
			return Constants.ERROR;
		}
		
		return Constants.NO_ERROR;
	}
	
	/**
	 * This function contains all logic for know if it's necessary delete or add a static address and templates
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String ip : Is the BCC server IP and it is used to connect to BCC
	 * @param String user: Is the BCC server user and it is used to connect to BCC
	 * @param String pass: Is the BCC server Password and it is used to connect to BCC
	 * @param String FileOrig: Is the name of the read File where is save the object previously export. 
	 * @return int Constant with Error o No Error.
	 *  **/		
	public static int setStaticAddressTemplates(String ip, String user, String pass, String FileOrig, int option, ArrayList<String> filter,String Debug) {
		ArrayList<Templates> templates = new ArrayList<Templates>();
		ArrayList<Templates> templatesBcc = new ArrayList<Templates>();
		ArrayList<Templates> templateAdd = new ArrayList<Templates>();
		ArrayList<StaticAddress> staticBcc = new ArrayList<StaticAddress>();
		ArrayList<StaticAddress> staticAdd = new ArrayList<StaticAddress>();
		ArrayList<StaticAddress> staticAddressAdd = new ArrayList<StaticAddress>();
		ArrayList<Rules> rules = new ArrayList<Rules>();
		ArrayList<String> filterTemp = new ArrayList<String>();
		
		int templaDele=0; int templateadd=0; int templatesFile=0; int templBcc=0;
		int staticAddDele=0; int staticAddFile=0;int staticAddrAdd=0; int statBCC=0;
		String auxString="";
		String command="";
		try {
			staticAdd.clear();
/**			if (1>2) {
				command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list TEMPLATE ";
				getObjectTemplates(templatesBcc, command);
			}
**/			templBcc = templatesBcc.size();
			command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list STATICADDRESS";
			getObjectStaticAddess(staticBcc, command,rules,templatesBcc);
			statBCC = staticBcc.size();
			
			if (option == Constants.DELETEALL) {
				List<String> CmdAdd= new ArrayList<>();
				if (templateOpt) {
					Iterator<Templates> itrTemplate = templatesBcc.iterator();
		            while(itrTemplate.hasNext()){      
		            	Templates rulet = itrTemplate.next();
		            	CmdAdd.clear();
						CmdAdd.add(rulet.getName().trim());
						CmdAdd.add("|lookupkey=");
						CmdAdd.add(rulet.getLookupkey().trim());
						auxString='"'+rulet.getName().trim() +'"'+ " |lookupkey=" + rulet.getLookupkey().trim();
		            	delTemplateBCC(ip, user, pass, auxString,CmdAdd);
		            	templaDele++;
		            }
				}
	            templates.clear();
	            
	            Iterator<StaticAddress> itrStaticAdd = staticBcc.iterator();
	            while(itrStaticAdd.hasNext()){      
	            	StaticAddress rulet = itrStaticAdd.next();
	            	CmdAdd.clear();
	            	CmdAdd.add(rulet.getName().trim());
					CmdAdd.add("|lookupkey=");
					CmdAdd.add(rulet.getLookupkey().trim());
	            	auxString='"'+rulet.getName().trim() +'"'+ " |lookupkey=" + rulet.getLookupkey().trim();
	            	delStaticAddBCC(ip, user, pass, auxString,CmdAdd);
	            	staticAddDele++;
	            }
	            templates.clear();
	            
			}
			
	        ReadDiskObject RDO = new ReadDiskObject();
	        if (1<2) {
	        	templates = RDO.getObjecTemplatesDisk(FileOrig,filterTemp);
	        }
	        staticAdd = RDO.getObjecStaticAddDisk(FileOrig,filter);
	        List<String> CmdAdd= new ArrayList<>();
	        templatesFile=templates.size();
	        /*First review Templates*/
	        if (templateOpt) {
		        if (templates.size()> Constants.NODATA) {
		            Iterator<Templates> itrTemplateAux = templates.iterator();
		            while(itrTemplateAux.hasNext()){
		            	Templates ruleta = itrTemplateAux.next();
		            	CmdAdd.clear();
		            	CmdAdd.add(ruleta.getName().trim());
		            	if (!"".equals(ruleta.getDescription().trim())) {
		            		CmdAdd.add("DESCRIPTION");CmdAdd.add(ruleta.getDescription());
		            	}
		            	CmdAdd.add("ENABLEDNS");CmdAdd.add( ruleta.getEnabledns());
		            	CmdAdd.add("INHERITDNSSETTINGS");CmdAdd.add(ruleta.getInheritdnssettings());
		            	//CmdAdd.add("GLOBALTEMPLATE");CmdAdd.add(ruleta.getGlobaltemplate());
		            	if ((!Constants.ipZero.equals(ruleta.getDdnsserver().trim()))&&(!"".equals(ruleta.getDdnsserver().trim()))) {
		            		CmdAdd.add("DDNSSERVER");CmdAdd.add(ruleta.getDdnsserver());
		            	}	
		            	long LookupKey=searchTemplateByName(templatesBcc, ruleta.getName());
						List<String> list = new ArrayList<>();
						if (LookupKey> Constants.NODATA) {               	
		                	if (option==Constants.ADD) {
		                		LogWriter.getLogger().info("Nothing to do for " + ruleta.getName());
		                	}
		                	if (option==Constants.OVERWRITE) {
		                		List<String> CmdAdd2= new ArrayList<>();
		                		/**Deleting template**/
		                		CmdAdd2.clear();
		                		CmdAdd2.add(ruleta.getName().trim());
		                		CmdAdd2.add("|lookupkey=");
		                		CmdAdd2.add(String.valueOf(LookupKey));
		                		auxString='"'+ruleta.getName().trim() +'"'+ " |lookupkey=" + LookupKey;
		                		delTemplateBCC(ip, user, pass, auxString,CmdAdd2);
		                		templaDele++;
		                		list.clear();
		        	            /**Creating rule**/
		                		addTemplateBCC(ip, user, pass, CmdAdd, list);
		                    	templateAdd.add(ruleta);
		                    	templateadd++;
	
		                	}
		                	if (option==3) {
		                		addTemplateBCC(ip, user, pass, CmdAdd, list);
		                		templateAdd.add(ruleta);
		                    	templateadd++;
		                	}
		                }else {
		                	addTemplateBCC(ip, user, pass, CmdAdd, list);
	                		templateAdd.add(ruleta);
	                    	templateadd++;
		                }
		            }
		        }else {
		        	System.out.println("Error to obtain object template or the file does not contain information");
		        }
	        }
	        staticAddFile=staticAdd.size();
	        /*Second review Static Address*/
	        if (staticAdd.size()> Constants.NODATA) {
	            Iterator<StaticAddress> itrStaticAddAux = staticAdd.iterator();
	            while(itrStaticAddAux.hasNext()){
	            	StaticAddress ruleta = itrStaticAddAux.next();
	            	CmdAdd.clear();
	            	CmdAdd.add(ruleta.getName().trim());
	            	if (!"".equals(ruleta.getDescription())) {
	            		CmdAdd.add("DESCRIPTION");CmdAdd.add('"'+ruleta.getDescription()+'"');
	            	}
	            	CmdAdd.add("CLIENTDATATYPE");CmdAdd.add( ruleta.getClientdatatype());
	            	CmdAdd.add("STATICIPADDRESS");CmdAdd.add(ruleta.getStaticipaddress());
	            	CmdAdd.add("MANUAL");CmdAdd.add(ruleta.getManual());
	            	CmdAdd.add("CLIENTDATA");CmdAdd.add(ruleta.getClientdata());
	            	CmdAdd.add("ENABLEDNS");CmdAdd.add(ruleta.getEnabledns());
	            	CmdAdd.add("INHERITDNSSETTINGS");CmdAdd.add(ruleta.getInheritdnssettings());
	            	CmdAdd.add("HARDWARETYPE");CmdAdd.add(ruleta.getHardwaretype());
	            	if ((!Constants.ipZero.equals(ruleta.getDdnsserver().trim()))&&(!"".equals(ruleta.getDdnsserver().trim()))) {
	            		CmdAdd.add("DDNSSERVER");CmdAdd.add(ruleta.getDdnsserver());
	            	}
	            	long LookupKey=searchStaticAddByName(staticBcc, ruleta.getName());
					List<String> list = new ArrayList<>();
					if (LookupKey> Constants.NODATA) {               	
	                	if (option==Constants.ADD) {
	                		LogWriter.getLogger().info("Nothing to do for " + ruleta.getName());
	                	}
	                	if (option==Constants.OVERWRITE) {
	                		List<String> CmdAdd2= new ArrayList<>();
	                		/**Deleting static Address**/
	                		CmdAdd2.clear();
	                		CmdAdd2.add(ruleta.getName().trim());
	                		CmdAdd2.add("|lookupkey=");
	                		CmdAdd2.add(String.valueOf(LookupKey));
	                		auxString='"'+ruleta.getName().trim() +'"'+ "|lookupkey=" + LookupKey;
	                		delStaticAddBCC(ip, user, pass, auxString,CmdAdd2);
	                		staticAddDele++;
	                		list.clear();
	        	            /**Creating rule**/
	                		addStaticAddressBCC(ip, user, pass, CmdAdd, list);
	                		staticAddressAdd.add(ruleta);
	                		staticAddrAdd++;

	                	}
	                	if (option==3) {
	                		addStaticAddressBCC(ip, user, pass, CmdAdd, list);
	                		staticAddressAdd.add(ruleta);
	                		staticAddrAdd++;
	                	}
	                }else {
	                	addStaticAddressBCC(ip, user, pass, CmdAdd, list);
	                	staticAddressAdd.add(ruleta);
	                	staticAddrAdd++;
	                }
	            }
	        }else {
	        	System.out.println("Error to obtain object static address or the file does not contain information");
	        }
	        List<String> list = new ArrayList<>();
	        int RelaTemStat=0;
	        if (templateOpt) {
		        /**Relating templates to static address**/
		        
		        
		        for (int j=0; j<staticAddressAdd.size();j++) {
		        	if (!"".equals(staticAddressAdd.get(j).getTemplate())){
			        	CmdAdd.clear();
			        	list.clear();
			        	CmdAdd.add("RELATE");
			        	CmdAdd.add("STATICADDRESS");
			        	CmdAdd.add(staticAddressAdd.get(j).getName());
			        	CmdAdd.add("TEMPLATE");
			        	CmdAdd.add(staticAddressAdd.get(j).getTemplate());
			        	relateRuleBCC(ip, user, pass, CmdAdd, list);
			        	RelaTemStat++;
		        	}
				}
	        }
	        
	        /**Relating rules to static address**/
	        List<String> list2 = new ArrayList<>();
	        int RelaTemStatR=0;
	        for (int j=0; j<staticAddressAdd.size();j++) {
	        	if (!"".equals(staticAddressAdd.get(j).getRule())){
		        	CmdAdd.clear();
		        	list.clear();
		        	CmdAdd.add("RELATE");
		        	CmdAdd.add("RULE");
		        	CmdAdd.add(staticAddressAdd.get(j).getRule());
		        	CmdAdd.add("STATICADDRESS");
		        	CmdAdd.add(staticAddressAdd.get(j).getName());
		        	relateRuleBCC(ip, user, pass, CmdAdd, list2);
		        	RelaTemStatR++;
	        	}
			}
	        
	        templatesBcc.clear();
	        command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list TEMPLATE ";
			getObjectTemplates(templatesBcc, command);
			int templBccPos = templatesBcc.size();
			command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list STATICADDRESS";
			staticBcc.clear();
			rules.clear();
			getObjectStaticAddess(staticBcc, command,rules,templatesBcc);
			int templBccPosS = staticBcc.size();
	        System.out.println("++Import Summary++");
	        System.out.println("Templates in BCC previuos import " +  templBcc);
	        System.out.println("Static Address in BCC previuos import " +  statBCC);
	        System.out.println("Templates delete " + templaDele);
	        System.out.println("Static Address delete " + staticAddDele);
	        System.out.println("Templates add " + templateadd);
	        System.out.println("Static Address add " + staticAddrAdd);
	        System.out.println("Related templates and static address " + RelaTemStat );
	        System.out.println("Related rules and static address " + RelaTemStatR );
	        System.out.println("Templates in File " + templatesFile);
	        System.out.println("Static Address in File " + staticAddFile);
	        System.out.println("Templates in BCC later import " +  templBccPos);
	        System.out.println("Static Address in BCC later import " +  templBccPosS);
	        return Constants.NO_ERROR;
		}catch(Exception e) {
			System.out.println("Error Importing data:" + e.getMessage());
			e.printStackTrace();
			return Constants.ERROR;
		}
	}


public static int compareStaticAddressTemplates(String ip, String user, String pass,String ip2, String user2, String pass2, String File, boolean OptionDebug, boolean remotessh) {
	ArrayList<Templates> templates = new ArrayList<Templates>();
	ArrayList<Templates> templates2 = new ArrayList<Templates>();
	Integer maxlength=0;
	OptionDebug=false;
	String command2="";
	try {
		String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list TEMPLATE SORTBY NAME ";
		if (remotessh)
			command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list TEMPLATE SORTBY NAME ";
		else
			command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list TEMPLATE SORTBY NAME ";
		getObjectTemplates(templates, command);
		getObjectTemplates(templates2, command2);
		
		
		
		if (templates.equals(templates2)) {
        	System.out.println("There is not diferences between the static address " + templates.size() + " 2" + templates2.size());
        }else {
        	System.out.println("There is  diferences between the static address  " + templates.size() + " 2" + templates2.size());
        	if (templates.size()<=templates2.size()) {
        		maxlength = templates2.size();
        	}else {
        		maxlength = templates.size();
        	}
        	for (int i =0; i<maxlength; i++) {
        		if (!(templates.get(i).equals(templates2.get(i)))) {
        			System.out.println("There are differences between static Address. "  + templates.get(i).getName() + " and " +  templates2.get(i).getName());
        			Utils.CompateTwoString("Ddnsserver ",templates.get(i).getDdnsserver() ,templates2.get(i).getDdnsserver() ,OptionDebug);
        			Utils.CompateTwoString("Description",templates.get(i).getDescription(),templates2.get(i).getDescription(),OptionDebug);
        			Utils.CompateTwoString("Enabledns",templates.get(i).getEnabledns(),templates2.get(i).getEnabledns(),OptionDebug);
        			Utils.CompateTwoString("Globaltemplate",templates.get(i).getGlobaltemplate(),templates2.get(i).getGlobaltemplate(),OptionDebug);
        			Utils.CompateTwoString("Inheritdnssettings",templates.get(i).getInheritdnssettings(),templates2.get(i).getInheritdnssettings(),OptionDebug);
        			Utils.CompateTwoString("Lookupkey",templates.get(i).getLookupkey(),templates2.get(i).getLookupkey(),OptionDebug);
        			Utils.CompateTwoString("Name",templates.get(i).getName(),templates2.get(i).getName(),OptionDebug);
        		}
        	}
        }
       
		return Constants.NO_ERROR;
	}catch(Exception e) {
		System.out.println("Error getting data of routing elements" + e.getMessage());
		e.printStackTrace();
		return Constants.ERROR;
	}
}
public static int comparefunctionDFS(String ip, String user, String pass,String ip2, String user2, String pass2, String FileDest, boolean OptionDebug, boolean remotessh)  {
	ArrayList<DocsisFS> dfs = new ArrayList<DocsisFS>();
	ArrayList<DocsisFS> dfs2 = new ArrayList<DocsisFS>();
	
	OptionDebug=false;
	int maxlength=0;
	int maxlengthtlv=0;
	String command2="";
	String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list DOCSISFS SORTBY NAME";
	if (remotessh)
		command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list DOCSISFS SORTBY NAME";
	else
		command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list DOCSISFS SORTBY NAME";
	try{
		getObjectDFS(dfs,command);
		
		getObjectDFS(dfs2,  command2);

		
        if (dfs.equals(dfs2)) {
        	System.out.println("There is not diferences between the routing elements " + dfs.size() + " and  " + dfs2.size());
        }else {
        	System.out.println("There is  diferences between the routing elements " + dfs.size() + " and " + dfs2.size());
        	if (dfs.size()<=dfs2.size()) {
        		maxlength = dfs2.size();
        	}else {
        		maxlength = dfs.size();
        	}
        	for (int i =0; i<maxlength; i++) {
        		if (!(dfs.get(i).equals(dfs2.get(i)))) {            			
        			System.out.println("There are differences between DFS "  + dfs.get(i).getName() + " and " +  dfs2.get(i).getName());
        			Utils.CompateTwoString("NAME" , dfs.get(i).getName(), dfs2.get(i).getName(), OptionDebug);
        			Utils.CompateTwoString("DESCRIPTION" ,dfs.get(i).getDescription() , dfs2.get(i).getDescription(),OptionDebug);
        			Utils.CompateTwoString("LOOKUPKEY" , dfs.get(i).getLookupkey(), dfs2.get(i).getLookupkey(),OptionDebug);
        			Utils.CompateTwoString("MANAGEMENTLOOKUPKEY", dfs.get(i).getManagmentLookupkey(),dfs2.get(i).getManagmentLookupkey(),OptionDebug);
        			
        			ArrayList<TVLSettings> ruleta = new ArrayList<TVLSettings>();
					ruleta=getTLVs(ip, user, pass, dfs.get(i).getName());

					ArrayList<TVLSettings> ruleta2 = new ArrayList<TVLSettings>();
					String Name = dfs2.get(i).getName();
					if (remotessh) 
						ruleta2=getTLVsRemote(ip2, user2, pass2, Name);
					else
						ruleta2=getTLVs(ip2, user2, pass2, Name);

        			System.out.println(" Sizes " + ruleta.size() + " + " + ruleta2.size());
        			if (ruleta.size() != ruleta2.size()) {
        				if (ruleta.size()<=ruleta2.size()) {
        					maxlengthtlv = ruleta2.size();
    	            	}else {
    	            		maxlengthtlv = ruleta.size();
    	            	}
        				
            			for(int j=0; j< maxlengthtlv;j++) {
            				//if (!(ruleta.get(j).equals(ruleta2.get(j)))) {
            					System.out.println("There are differences between Tlvs. "  + ruleta.get(j).getTlvCode() + " and " +  ruleta2.get(j).getTlvCode());
            					Utils.CompateTwoString("ExternalDefault ",ruleta.get(j).getExternalDefault() ,ruleta2.get(j).getExternalDefault() ,OptionDebug);
            					Utils.CompateTwoString("ExternalField ",ruleta.get(j).getExternalField() ,ruleta2.get(j).getExternalField() ,OptionDebug);
            					Utils.CompateTwoString("ExternalName ",ruleta.get(j).getExternalName() ,ruleta2.get(j).getExternalName() ,OptionDebug);
            					Utils.CompateTwoString("ExternalQuery ",ruleta.get(j).getExternalQuery() ,ruleta2.get(j).getExternalQuery() ,OptionDebug);
            					Utils.CompateTwoString("LdapAttr",ruleta.get(j).getLdapAttr(),ruleta2.get(j).getLdapAttr(),OptionDebug);
            					Utils.CompateTwoString("LdapName ",ruleta.get(j).getLdapName() ,ruleta2.get(j).getLdapName() ,OptionDebug);
            					Utils.CompateTwoString("Ldapquery ",ruleta.get(j).getLdapquery() ,ruleta2.get(j).getLdapquery() ,OptionDebug);
            					Utils.CompateTwoString("TlvCode ",ruleta.get(j).getTlvCode() ,ruleta2.get(j).getTlvCode() ,OptionDebug);
            					Utils.CompateTwoString("TlvData",ruleta.get(j).getTlvData(),ruleta2.get(j).getTlvData(),OptionDebug);
            					Utils.CompateTwoString("TlvEntry",ruleta.get(j).getTlvEntry(),ruleta2.get(j).getTlvEntry(),OptionDebug);
            					Utils.CompateTwoString("TlvFlag",ruleta.get(j).getTlvFlag(),ruleta2.get(j).getTlvFlag(),OptionDebug);
            					Utils.CompateTwoString("TlvName",ruleta.get(j).getTlvName(),ruleta2.get(j).getTlvName(),OptionDebug);
            					Utils.CompateTwoString("TlvVendor ",ruleta.get(j).getTlvVendor() ,ruleta2.get(j).getTlvVendor() ,OptionDebug);

            				//}
	                	}
        			}	
        		}	
        	}
        }
        
        
        return Constants.NO_ERROR;
        
	}catch(Exception e) {
		System.out.println("EXCEPTION: " + e.getMessage());
		 e.printStackTrace();
		return Constants.ERROR;
	}
}

public static int comparefunctionDC(String ip, String user, String pass,String ip2, String user2, String pass2, String FileDest, boolean OptionDebug, boolean remotessh)  {
	ArrayList<DeviceClassifier> dcs = new ArrayList<DeviceClassifier>();
	ArrayList<DeviceClassifier> dcs2 = new ArrayList<DeviceClassifier>();
	
	OptionDebug=false;
	int maxlength=0;
	String command2="";
	String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list DEVICECLASSIFIER SORTBY NAME";
	if (remotessh)
		command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list DEVICECLASSIFIER SORTBY NAME";
	else
		command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list DEVICECLASSIFIER SORTBY NAME";
	try{
		getObjectDCS(dcs,command);
		
		getObjectDCS(dcs2,  command2);

		
        if (dcs.equals(dcs2)) {
        	System.out.println("There is not diferences between device classifiers " + dcs.size() + " and  " + dcs2.size());
        }else {
        	System.out.println("There is  diferences between the device classifiers " + dcs.size() + " and " + dcs2.size());
        	if (dcs.size()<=dcs2.size()) {
        		maxlength = dcs2.size();
        	}else {
        		maxlength = dcs.size();
        	}
        	for (int i =0; i<maxlength; i++) {
        		if (!(dcs.get(i).equals(dcs2.get(i)))) {            			
        			System.out.println("There are differences between Device Classifiers "  + dcs.get(i).getName() + " and " +  dcs2.get(i).getName());
        			Utils.CompateTwoString("NAME" , dcs.get(i).getName(), dcs2.get(i).getName(), OptionDebug);
        			Utils.CompateTwoString("DESCRIPTION" ,dcs.get(i).getDescription() , dcs2.get(i).getDescription(),OptionDebug);
        			Utils.CompateTwoString("CRITERIA" , dcs.get(i).getCriteria(), dcs2.get(i).getCriteria(),OptionDebug);
        			Utils.CompateTwoString("COPYIDTOACS", dcs.get(i).getCopyIdTacs(),dcs2.get(i).getCopyIdTacs(),OptionDebug);
        			Utils.CompateTwoString("LOOKUPKEY", dcs.get(i).getLookupKey(),dcs2.get(i).getLookupKey(),OptionDebug);
        			Utils.CompateTwoString("MANAGEMENTLOOKUPKEY", dcs.get(i).getManagmentLookUpkey(),dcs2.get(i).getManagmentLookUpkey(),OptionDebug);
        			Utils.CompateTwoString("ACLLOOKUPKEY", dcs.get(i).getAclLookupKey(),dcs2.get(i).getAclLookupKey(),OptionDebug);    			
        			
        		}	
        	}
        }
        
        
        return Constants.NO_ERROR;
        
	}catch(Exception e) {
		System.out.println("EXCEPTION: " + e.getMessage());
		 e.printStackTrace();
		return Constants.ERROR;
	}
}

public static int comparefunctionCCG(String ip, String user, String pass,String ip2, String user2, String pass2, String FileDest, boolean OptionDebug, boolean remotessh)  {
	ArrayList<ClientClassGroup> ccg = new ArrayList<ClientClassGroup>();
	ArrayList<ClientClassGroup> ccg2 = new ArrayList<ClientClassGroup>();
	
	OptionDebug=false;
	int maxlength=0;
	String command2="";
	String command="ipcli -n "+ user + " -p " + pass + " -s " +  ip + " list CCGROUP SORTBY NAME";
	if (remotessh)
		command2="ssh root@" + ip2+ "  ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list CCGROUP SORTBY NAME";
	else
		command2="ipcli -n "+ user2 + " -p " + pass2 + " -s " +  ip2 + " list CCGROUP SORTBY NAME";
	try{
		getObjectCCG(ccg,command);
		
		getObjectCCG(ccg2,  command2);

		
        if (ccg.equals(ccg2)) {
        	System.out.println("There is not diferences between client class group " + ccg.size() + " and  " + ccg2.size());
        }else {
        	System.out.println("There is  diferences between the client class group " + ccg.size() + " and " + ccg2.size());
        	if (ccg.size()<=ccg2.size()) {
        		maxlength = ccg2.size();
        	}else {
        		maxlength = ccg.size();
        	}
        	for (int i =0; i<maxlength; i++) {
        		if (!(ccg.get(i).equals(ccg2.get(i)))) {            			
        			System.out.println("There are differences between client class groups "  + ccg.get(i).getName() + " and " +  ccg2.get(i).getName());
        			Utils.CompateTwoString("NAME" , ccg.get(i).getName(), ccg2.get(i).getName(), OptionDebug);
        			Utils.CompateTwoString("DESCRIPTION" ,ccg.get(i).getDescription() , ccg2.get(i).getDescription(),OptionDebug);
        			Utils.CompateTwoString("LOOKUPKEY", ccg.get(i).getLookup(),ccg2.get(i).getLookup(),OptionDebug);
        			Utils.CompateTwoString("ACLLOOKUPKEY", ccg.get(i).getAcclokupkey(),ccg2.get(i).getAcclokupkey(),OptionDebug);
        			Utils.CompateTwoString("CLIENTCLASS" , ccg.get(i).getClientclass(), ccg2.get(i).getClientclass(),OptionDebug);
        			Utils.CompateTwoString("MANAGEMENTLOOKUPKEY", ccg.get(i).getManagmentLookup(),ccg2.get(i).getManagmentLookup(),OptionDebug);
        			Utils.CompateTwoString("MOVEDUPLICATES", ccg.get(i).getMoveDuplicates(),ccg2.get(i).getMoveDuplicates(),OptionDebug);
        			Utils.CompateTwoString("QOS", ccg.get(i).getQos(),ccg2.get(i).getQos(),OptionDebug);
        			Utils.CompateTwoString("TEMPLATELOOKUPKEY", ccg.get(i).getTemplateLookupKey(),ccg2.get(i).getTemplateLookupKey(),OptionDebug);

        		}	
        	}
        }
        
        
        return Constants.NO_ERROR;
        
	}catch(Exception e) {
		System.out.println("EXCEPTION: " + e.getMessage());
		 e.printStackTrace();
		return Constants.ERROR;
	}
}


}

