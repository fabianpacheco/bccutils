import java.io.Serializable;

public class Templates implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5497007649335935767L;

	private String ddnsserver = "";

	private String description = "";
	
	private String enabledns = "";

	private String globaltemplate = "";

	private String inheritdnssettings = "";

	private String lookupkey = "";

	private String name = "";

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		Templates clonObj = (Templates) super.clone();
		return clonObj;
	}

	public String getDdnsserver() {
		return ddnsserver;
	}

	public String getDescription() {
		return description;
	}

	public String getEnabledns() {
		return enabledns;
	}

	public String getGlobaltemplate() {
		return globaltemplate;
	}

	public String getInheritdnssettings() {
		return inheritdnssettings;
	}

	public String getLookupkey() {
		return lookupkey;
	}

	public String getName() {
		return name;
	}
	public void setDdnsserver(String ddnsserver) {
		this.ddnsserver = ddnsserver;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setEnabledns(String enabledns) {
		this.enabledns = enabledns;
	}
	public void setGlobaltemplate(String globaltemplate) {
		this.globaltemplate = globaltemplate;
	}

	public void setInheritdnssettings(String inheritdnssettings) {
		this.inheritdnssettings = inheritdnssettings;
	}

	public void setLookupkey(String lookupkey) {
		this.lookupkey = lookupkey;
	}

	public void setName(String name) {
		this.name = name;
	}

}
