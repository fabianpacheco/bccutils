import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class ReadDiskObject {
	@SuppressWarnings("unchecked")
	/**
	 * This function read the File and create the object Rules.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName: File name to read
	 * @return ArrayList<Rules> rules : ArrayList with the object Ruoutes
	 *  **/
	public ArrayList<Rules> getObjecRulestDisk(String FileName, ArrayList<String> filter){
		ArrayList<Rules> rules = new ArrayList<Rules>();
		try {
		
		 ObjectInputStream leyendoFichero = new ObjectInputStream( 
		            new FileInputStream(FileName) );
		 rules = ( ArrayList<Rules> )leyendoFichero.readObject();
		            leyendoFichero.close();
		 LogWriter.getLogger().info("rules size before filter " + rules.size());
		 if (filter.size()>0) {
			 Iterator<Rules> itrRules = rules.iterator();
		     while(itrRules.hasNext()){
		    	 Rules rulet = itrRules.next();
		    	 if (!filter.contains(rulet.getName())){
		    		 	itrRules.remove();
						LogWriter.getLogger().info("removing rule " + rulet.getName());
				 }
		     }
		 }
		 LogWriter.getLogger().info("rules size after filter " + rules.size());
		}catch (Exception e) {
			System.out.println( e.getMessage() );
			return rules;
		}
		return rules;
	}
	@SuppressWarnings("unchecked")
	/**
	 * This function read the File and create the object Rules.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName: File name to read
	 * @return ArrayList<Rules> rules : ArrayList with the object Ruoutes
	 *  **/
	public ArrayList<RulesIPv6> getObjecRulesIPv6tDisk(String FileName, ArrayList<String> filter){
		ArrayList<RulesIPv6> rules = new ArrayList<RulesIPv6>();
		try {
		
		 ObjectInputStream leyendoFichero = new ObjectInputStream( 
		            new FileInputStream(FileName) );
		 rules = ( ArrayList<RulesIPv6> )leyendoFichero.readObject();
		            leyendoFichero.close();
   		 LogWriter.getLogger().info("rules size before filter " + rules.size());
		 if (filter.size()>0) {
			 Iterator<RulesIPv6> itrRules = rules.iterator();
		     while(itrRules.hasNext()){
		    	 RulesIPv6 rulet = itrRules.next();
		    	 if (!filter.contains(rulet.getName())){
		    		 	itrRules.remove();
						LogWriter.getLogger().info("removing rule " + rulet.getName());
				 }
		     }
		 }
		 LogWriter.getLogger().info("rules size after filter " + rules.size());
		}catch (Exception e) {
			System.out.println( e.getMessage() );
			return rules;
		}
		return rules;
	}
	@SuppressWarnings("unchecked")
	/**
	 * This function read the File and create the object Routes.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName: File name to read
	 * @return ArrayList<Rules> rules : ArrayList with the object Rules
	 *  **/
	public ArrayList<Routing> getObjecRoutestDisk(String FileName, ArrayList<String> filter){
		ArrayList<Routing> routes = new ArrayList<Routing>();
		try {
		
		 ObjectInputStream leyendoFichero = new ObjectInputStream( 
		            new FileInputStream(FileName) );
		 routes = ( ArrayList<Routing> )leyendoFichero.readObject();
		            leyendoFichero.close();
         LogWriter.getLogger().info("rules size before filter " + routes.size());
		 if (filter.size()>0) {
			 Iterator<Routing> itrRules = routes.iterator();
		     while(itrRules.hasNext()){
		    	 Routing rulet = itrRules.next();
		    	 if (!filter.contains(rulet.getName())){
		    		 	itrRules.remove();
						LogWriter.getLogger().info("removing rule " + rulet.getName());
				 }
		     }
		 }
		 LogWriter.getLogger().info("rules size after filter " + routes.size());		 
		}catch (Exception e) {
			System.out.println( e.getMessage() );
			return routes;
		}
		return routes;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * This function read the File and create the object Templates.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName: File name to read
	 * @return ArrayList<Rules> rules : ArrayList with the object Rules
	 *  **/
	public ArrayList<Templates> getObjecTemplatesDisk(String FileName, ArrayList<String> filter){
		 ArrayList<Templates> templ = new ArrayList<Templates>();
		try {
		 ObjectInputStream leyendoFichero = new ObjectInputStream( 
		            new FileInputStream("Templates"+FileName) );
		 templ = ( ArrayList<Templates> )leyendoFichero.readObject();
		            leyendoFichero.close();
		 LogWriter.getLogger().info("rules size before filter " + templ.size());
		 if (filter.size()>0) {
			 Iterator<Templates> itrRules = templ.iterator();
			 while(itrRules.hasNext()){
				 Templates rulet = itrRules.next();
				 if (!filter.contains(rulet.getName())){
	    		 	itrRules.remove();
					LogWriter.getLogger().info("removing rule " + rulet.getName());
				 }
			 }
		 }
		 LogWriter.getLogger().info("rules size after filter " + templ.size());	
		 return templ;
		}catch (Exception e) {
			System.out.println( e.getMessage() );
			return templ;
		}
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * This function read the File and create the object Templates.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName: File name to read
	 * @return ArrayList<Rules> rules : ArrayList with the object Rules
	 *  **/
	public ArrayList<StaticAddress> getObjecStaticAddDisk(String FileName, ArrayList<String> filter){
		 ArrayList<StaticAddress> staticA = new ArrayList<StaticAddress>();
		try {
		
        ObjectInputStream leyendoFicheros2 = new ObjectInputStream( 
	            new FileInputStream("StaticAddress"+FileName) );
        staticA = ( ArrayList<StaticAddress> )leyendoFicheros2.readObject();
	            leyendoFicheros2.close();
	    LogWriter.getLogger().info("rules size before filter " + staticA.size());
		if (filter.size()>0) {
			Iterator<StaticAddress> itrRules = staticA.iterator();
			while(itrRules.hasNext()){
				StaticAddress rulet = itrRules.next();
				if (!filter.contains(rulet.getName())){
					itrRules.remove();
					LogWriter.getLogger().info("removing rule " + rulet.getName());
				}
			}
		}
		LogWriter.getLogger().info("rules size after filter " + staticA.size());		            
	    return staticA;
		}catch (Exception e) {
			System.out.println( e.getMessage() );
			return staticA;
		}
	}

}
