import java.io.Serializable;
import java.util.ArrayList;
/**
 * Object serializable and clonable contains all information about the routing elements.
 * @author Fabian Pacheco
 * @version: 31/12/2020
 *  **/
public class Routing implements Cloneable, Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7490950104091920544L;
	
	public Routing() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	 @Override
	  public String toString() {
		 String a=Name +"," +  LookUpKey+"," +Description+"," +Authkey+"," +ConfirmAuthkey+"," +DocsisMajorVersion+"," +
	  DocsisMinorVersion+"," +ExtendedCmtsmicType+"," +ExtendedCmtsmicSharedSecret +"," + ConfirmExtendedmicSharedSecret +"," +
				 ManagementIp +"," +SpreadipAllocation;
	    return a;
	  }

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		Routing clonObj = (Routing)super.clone();
		return clonObj;
	}
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getLookUpKey() {
		return LookUpKey;
	}
	public void setLookUpKey(String lookUpKey) {
		LookUpKey = lookUpKey;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getAuthkey() {
		return Authkey;
	}
	public void setAuthkey(String authkey) {
		Authkey = authkey;
	}
	public String getConfirmAuthkey() {
		return ConfirmAuthkey;
	}
	public void setConfirmAuthkey(String confirmAuthkey) {
		ConfirmAuthkey = confirmAuthkey;
	}
	public String getDocsisMajorVersion() {
		return DocsisMajorVersion;
	}
	public void setDocsisMajorVersion(String docsisMajorVersion) {
		DocsisMajorVersion = docsisMajorVersion;
	}
	public String getDocsisMinorVersion() {
		return DocsisMinorVersion;
	}
	public void setDocsisMinorVersion(String docsisMinorVersion) {
		DocsisMinorVersion = docsisMinorVersion;
	}
	public String getExtendedCmtsmicType() {
		return ExtendedCmtsmicType;
	}
	public void setExtendedCmtsmicType(String extendedCmtsmicType) {
		ExtendedCmtsmicType = extendedCmtsmicType;
	}
	public String getExtendedCmtsmicSharedSecret() {
		return ExtendedCmtsmicSharedSecret;
	}
	public void setExtendedCmtsmicSharedSecret(String extendedCmtsmicSharedSecret) {
		ExtendedCmtsmicSharedSecret = extendedCmtsmicSharedSecret;
	}
	public String getConfirmExtendedmicSharedSecret() {
		return ConfirmExtendedmicSharedSecret;
	}
	public void setConfirmExtendedmicSharedSecret(String confirmExtendedmicSharedSecret) {
		ConfirmExtendedmicSharedSecret = confirmExtendedmicSharedSecret;
	}
	public String getManagementIp() {
		return ManagementIp;
	}
	public void setManagementIp(String managementIp) {
		ManagementIp = managementIp;
	}
	public String getSpreadipAllocation() {
		return SpreadipAllocation;
	}
	public void setSpreadipAllocation(String spreadipAllocation) {
		SpreadipAllocation = spreadipAllocation;
	}
	public ArrayList<Gateways> getGateways() {
		return gateways;
	}
	public void setGateways(ArrayList<Gateways> gateways) {
		this.gateways = gateways;
	}
	
	private String Name="";
	private String LookUpKey="";
	private String Description="";
	private String Authkey="";
	private String ConfirmAuthkey="";
	private String DocsisMajorVersion="";
	private String DocsisMinorVersion="";
	private String ExtendedCmtsmicType="";
	private String ExtendedCmtsmicSharedSecret="";
	private String ConfirmExtendedmicSharedSecret="";
	private String ManagementIp="";
	private String SpreadipAllocation="";
	private ArrayList<Gateways> gateways;

}
