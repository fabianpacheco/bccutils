import java.io.Serializable;
import java.util.ArrayList;

public class DocsisFS implements Cloneable, Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5379376600970480401L;
	public DocsisFS() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		DocsisFS clonObj = (DocsisFS)super.clone();
		return clonObj;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getLookupkey() {
		return Lookupkey;
	}
	public void setLookupkey(String lookupkey) {
		Lookupkey = lookupkey;
	}
	public String getManagmentLookupkey() {
		return ManagmentLookupkey;
	}
	public void setManagmentLookupkey(String managmentLookupkey) {
		ManagmentLookupkey = managmentLookupkey;
	}
	public ArrayList<TVLSettings> getTVLSettings() {
		return TVLSettings;
	}
	public void setTVLSettings(ArrayList<TVLSettings> tVLSettings) {
		TVLSettings = tVLSettings;
	}
	private String Name="";
	private String Description="";
	private String Lookupkey="";
	private String ManagmentLookupkey="";
	private ArrayList<TVLSettings> TVLSettings; 
}
