
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * This class is for write log in file and controls if write or not .
 * @author Fabian Pacheco
 * @version: 31/12/2020
 *  **/
public class LogWriter {

    public static String FILE_NAME_LOGGER = Constants.LOGFILENAME;
    private static Logger logger = null;
    public static String loglevel="";



	public static synchronized Logger getLogger() {

        if (logger == null) {
            LogManager logManager = LogManager.getLogManager();
            logger = Logger.getLogger(LogWriter.class.getName());
            logManager.addLogger(logger);
            if (!Constants.LOGON.equals(loglevel.trim()))
            	logger.setLevel(Level.OFF);
            try {
                FileHandler fileHandler =
                    new FileHandler(FILE_NAME_LOGGER, Constants.LOGSIZE, 1);
                SimpleFormatter sf = new SimpleFormatter();
                fileHandler.setFormatter(sf);
                logger.addHandler(fileHandler);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return logger;
    }
    
}
