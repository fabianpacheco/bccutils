import java.io.Serializable;

public class TVLSettings  implements Cloneable, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -761992514573395082L;
	private String ExternalDefault="";

	private String ExternalField="";
	private String ExternalName="";
	private String ExternalQuery="";
	private String ldapAttr="";
	private String LdapName="";
	private String Ldapquery="";
	private String TlvCode="";
	private String TlvData="";
	private String TlvEntry="";
	private String TlvFlag="";
	private String TlvName="";
	private String TlvVendor="";
	public TVLSettings() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		TVLSettings clonObj = (TVLSettings)super.clone();
		return clonObj;
	}
	public String getExternalDefault() {
		return ExternalDefault;
	}
	public String getExternalField() {
		return ExternalField;
	}
	public String getExternalName() {
		return ExternalName;
	}
	public String getExternalQuery() {
		return ExternalQuery;
	}
	public String getLdapAttr() {
		return ldapAttr;
	}
	public String getLdapName() {
		return LdapName;
	}
	public String getLdapquery() {
		return Ldapquery;
	}
	public String getTlvCode() {
		return TlvCode;
	}
	public String getTlvData() {
		return TlvData;
	}
	public String getTlvEntry() {
		return TlvEntry;
	}
	public String getTlvFlag() {
		return TlvFlag;
	}
	public String getTlvName() {
		return TlvName;
	}
	public String getTlvVendor() {
		return TlvVendor;
	}
	public void setExternalDefault(String externalDefault) {
		ExternalDefault = externalDefault;
	}

	public void setExternalField(String externalField) {
		ExternalField = externalField;
	}
	public void setExternalName(String externalName) {
		ExternalName = externalName;
	}
	public void setExternalQuery(String externalQuery) {
		ExternalQuery = externalQuery;
	}
	public void setLdapAttr(String ldapAttr) {
		this.ldapAttr = ldapAttr;
	}
	public void setLdapName(String ldapName) {
		LdapName = ldapName;
	}
	public void setLdapquery(String ldapquery) {
		Ldapquery = ldapquery;
	}
	public void setTlvCode(String tlvCode) {
		TlvCode = tlvCode;
	}
	public void setTlvData(String tlvData) {
		TlvData = tlvData;
	}
	public void setTlvEntry(String tlvEntry) {
		TlvEntry = tlvEntry;
	}
	public void setTlvFlav(String tlvFlag) {
		TlvFlag = tlvFlag;
	}
	public void setTlvName(String tlvName) {
		TlvName = tlvName;
	}
	public void setTlvVendor(String tlvVendor) {
		TlvVendor = tlvVendor;
	}

}
