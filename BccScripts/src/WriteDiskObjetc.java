import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class WriteDiskObjetc {
	
	/**
	 * This function write the object Rules in a file.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName: File name to read
	 * @return Error or not
	 *  **/
	public int WriteObjectListRules(ArrayList<Rules> rules, String FileName) {
		try {
			ObjectOutputStream escribiendoFichero = new ObjectOutputStream( 
					new FileOutputStream(FileName) );
			escribiendoFichero.writeObject(rules);
			escribiendoFichero.close();
			return Constants.NO_ERROR;
		}catch (Exception e) {
			System.out.println( e.getMessage() );
			e.printStackTrace();
			return Constants.ERROR;
		}
	}
	
	/**
	 * This function write the object RulesIPv6 in a file.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName: File name to read
	 * @return Error or not
	 *  **/
	public int WriteObjectListRulesIPv6(ArrayList<RulesIPv6> rules, String FileName) {
		try {
			ObjectOutputStream escribiendoFichero = new ObjectOutputStream( 
					new FileOutputStream(FileName) );
			escribiendoFichero.writeObject(rules);
			escribiendoFichero.close();
			return Constants.NO_ERROR;
		}catch (Exception e) {
			System.out.println( e.getMessage() );
			e.printStackTrace();
			return Constants.ERROR;
		}
	}
	
	/**
	 * This function write the object Routes in a file.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName: File name to read
	 * @return Error or not
	 *  **/
	public int WriteObjectListRoutes(ArrayList<Routing> routes, String FileName) {
		try {
			ObjectOutputStream escribiendoFichero = new ObjectOutputStream( 
					new FileOutputStream(FileName) );
			escribiendoFichero.writeObject(routes);
			escribiendoFichero.close();
			return Constants.NO_ERROR;
		}catch (Exception e) {
			System.out.println( e.getMessage() );
			e.printStackTrace();
			return Constants.ERROR;
		}
	}
	
	/**
	 * This function write the object Templates and StaticAddress in a file.
	 * @author Fabian Pacheco
	 * @version: 31/12/2020
	 * @param String FileName: File name to read
	 * @return Error or not
	 *  **/
	public int WriteObjectListStaticAddress(ArrayList<Templates> templates,ArrayList<StaticAddress> staticAdd, String FileName) {
		try {
			String FileNameTemplates="Templates"+FileName;
			String FileNameStaticAdd="StaticAddress" + FileName;
			ObjectOutputStream escribiendoFicheroT = new ObjectOutputStream( 
					new FileOutputStream(FileNameTemplates) );
			escribiendoFicheroT.writeObject(templates);
			escribiendoFicheroT.close();
			ObjectOutputStream escribiendoFicheroS = new ObjectOutputStream( 
					new FileOutputStream(FileNameStaticAdd) );
			escribiendoFicheroS.writeObject(staticAdd);
			escribiendoFicheroS.close();
			return Constants.NO_ERROR;
		}catch (Exception e) {
			System.out.println( e.getMessage() );
			e.printStackTrace();
			return Constants.ERROR;
		}
	}

}
