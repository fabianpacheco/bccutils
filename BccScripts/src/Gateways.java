import java.io.Serializable;

/**
 * Object serializable and clonable contains all information about the gateways.
 * @author Fabian Pacheco
 * @version: 31/12/2020
 *  **/
public class Gateways implements Cloneable, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4649976499223079906L;
	
	public Gateways() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		Gateways clonObj = (Gateways)super.clone();
		return clonObj;
	}
	
	public String getGateway() {
		return Gateway;
	}
	public void setGateway(String gateway) {
		Gateway = gateway;
	}
	public String getNetwork() {
		return Network;
	}
	public void setNetwork(String network) {
		Network = network;
	}
	
	private String Gateway="";
	private String Network="";
}
