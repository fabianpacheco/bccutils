import java.io.Serializable;

public class StaticAddress implements Cloneable, Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7628970847096601421L;

	private String clientdata="";
	
	private String clientdatatype="";
	private String ddnsserver="";
	private String description="";
	private String enabledns="";
	private String hardwaretype="";
	private String inheritdnssettings="";
	private String lookupkey="";
	private String manual="";
	private String name="";
	private String rule="";
	private String staticipaddress="";
	private String template="";
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		StaticAddress clonObj = (StaticAddress)super.clone();
		return clonObj;
	}
	public String getClientdata() {
		return clientdata;
	}
	public String getClientdatatype() {
		return clientdatatype;
	}
	public String getDdnsserver() {
		return ddnsserver;
	}
	public String getDescription() {
		return description;
	}
	public String getEnabledns() {
		return enabledns;
	}
	public String getHardwaretype() {
		return hardwaretype;
	}
	public String getInheritdnssettings() {
		return inheritdnssettings;
	}
	public String getLookupkey() {
		return lookupkey;
	}
	public String getManual() {
		return manual;
	}
	public String getName() {
		return name;
	}
	public String getRule() {
		return rule;
	}

	public String getStaticipaddress() {
		return staticipaddress;
	}
	public String getTemplate() {
		return template;
	}
	public void setClientdata(String clientdata) {
		this.clientdata = clientdata;
	}
	public void setClientdatatype(String clientdatatype) {
		this.clientdatatype = clientdatatype;
	}
	public void setDdnsserver(String ddnsserver) {
		this.ddnsserver = ddnsserver;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setEnabledns(String enabledns) {
		this.enabledns = enabledns;
	}
	public void setHardwaretype(String hardwaretype) {
		this.hardwaretype = hardwaretype;
	}
	public void setInheritdnssettings(String inheritdnssettings) {
		this.inheritdnssettings = inheritdnssettings;
	}
	public void setLookupkey(String lookupkey) {
		this.lookupkey = lookupkey;
	}

	public void setManual(String manual) {
		this.manual = manual;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public void setStaticipaddress(String staticipaddress) {
		this.staticipaddress = staticipaddress;
	}
	public void setTemplate(String template) {
		this.template = template;
	}

}
