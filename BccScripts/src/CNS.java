import java.io.Serializable;

public class CNS implements Cloneable, Serializable{


	/**
	 * 
	 */
	
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		CNS clonObj = (CNS)super.clone();
		return clonObj;
	}
	
	
	
	public boolean equals(CNS obj) {
	    return (this == obj);
	}
	
	private String Name="";
	public String getName() {
		return Name;
	}



	public void setName(String name) {
		Name = name;
	}



	public String getLookupkey() {
		return Lookupkey;
	}



	public void setLookupkey(String lookupkey) {
		Lookupkey = lookupkey;
	}



	public String getPriority() {
		return Priority;
	}



	public void setPriority(String priority) {
		Priority = priority;
	}



	public String getNumberOptionsv4() {
		return NumberOptionsv4;
	}



	public void setNumberOptionsv4(String numberOptionsv4) {
		NumberOptionsv4 = numberOptionsv4;
	}



	public String getEnableDNSv4() {
		return EnableDNSv4;
	}



	public void setEnableDNSv4(String enableDNSv4) {
		EnableDNSv4 = enableDNSv4;
	}



	public String getInheritDnsSetinv4() {
		return InheritDnsSetinv4;
	}



	public void setInheritDnsSetinv4(String inheritDnsSetinv4) {
		InheritDnsSetinv4 = inheritDnsSetinv4;
	}



	public String getNumberOptionsv6() {
		return NumberOptionsv6;
	}



	public void setNumberOptionsv6(String numberOptionsv6) {
		NumberOptionsv6 = numberOptionsv6;
	}



	public String getEnableDNSv6() {
		return EnableDNSv6;
	}



	public void setEnableDNSv6(String enableDNSv6) {
		EnableDNSv6 = enableDNSv6;
	}



	public String getInheritDnsSetinv6() {
		return InheritDnsSetinv6;
	}



	public void setInheritDnsSetinv6(String inheritDnsSetinv6) {
		InheritDnsSetinv6 = inheritDnsSetinv6;
	}



	public String getPerformanceUpdates() {
		return PerformanceUpdates;
	}



	public void setPerformanceUpdates(String performanceUpdates) {
		PerformanceUpdates = performanceUpdates;
	}



	public String getTemplate() {
		return Template;
	}



	public void setTemplate(String template) {
		Template = template;
	}

	private String Lookupkey="";
	private String Priority="";
	private String NumberOptionsv4="";
	private String EnableDNSv4="";
	public String getDDNSv4() {
		return DDNSv4;
	}



	public void setDDNSv4(String dDNSv4) {
		DDNSv4 = dDNSv4;
	}



	public String getDDNSv6() {
		return DDNSv6;
	}



	public void setDDNSv6(String dDNSv6) {
		DDNSv6 = dDNSv6;
	}

	private String DDNSv4="";
	private String InheritDnsSetinv4="";
	private String NumberOptionsv6="";
	private String EnableDNSv6="";
	private String DDNSv6="";
	private String InheritDnsSetinv6=""; 
	private String PerformanceUpdates="";
	private String Template="";
}
