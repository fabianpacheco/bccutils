
public class ClientClass implements Cloneable{
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		ClientClass clonObj = (ClientClass)super.clone();
		return clonObj;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLookup() {
		return lookup;
	}
	public void setLookup(String lookup) {
		this.lookup = lookup;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	private String name="";
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAcclokupkey() {
		return acclokupkey;
	}

	public void setAcclokupkey(String acclokupkey) {
		this.acclokupkey = acclokupkey;
	}

	public String getAntiroaming() {
		return antiroaming;
	}

	public void setAntiroaming(String antiroaming) {
		this.antiroaming = antiroaming;
	}

	public int getClienClassEntry() {
		return clienClassEntry;
	}

	public void setClienClassEntry(int clienClassEntry) {
		this.clienClassEntry = clienClassEntry;
	}

	public String getClientDataType() {
		return clientDataType;
	}

	public void setClientDataType(String clientDataType) {
		this.clientDataType = clientDataType;
	}

	public String getSubDataType() {
		return subDataType;
	}

	public void setSubDataType(String subDataType) {
		this.subDataType = subDataType;
	}

	public String getDdnsServer4() {
		return ddnsServer4;
	}

	public void setDdnsServer4(String ddnsServer4) {
		this.ddnsServer4 = ddnsServer4;
	}

	public String getDdnsServer6() {
		return ddnsServer6;
	}

	public void setDdnsServer6(String ddnsServer6) {
		this.ddnsServer6 = ddnsServer6;
	}

	public String getDefault_() {
		return default_;
	}

	public void setDefault_(String default_) {
		this.default_ = default_;
	}

	public String getDefaultPrefixlength() {
		return defaultPrefixlength;
	}

	public void setDefaultPrefixlength(String defaultPrefixlength) {
		this.defaultPrefixlength = defaultPrefixlength;
	}

	public String getDefaultIpLimit() {
		return defaultIpLimit;
	}

	public void setDefaultIpLimit(String defaultIpLimit) {
		this.defaultIpLimit = defaultIpLimit;
	}

	public String getDhcpOptionNumber() {
		return DhcpOptionNumber;
	}

	public void setDhcpOptionNumber(String dhcpOptionNumber) {
		DhcpOptionNumber = dhcpOptionNumber;
	}

	public String getDhcpOptionVendor() {
		return DhcpOptionVendor;
	}

	public void setDhcpOptionVendor(String dhcpOptionVendor) {
		DhcpOptionVendor = dhcpOptionVendor;
	}

	public String getDhcpOptionVersion() {
		return DhcpOptionVersion;
	}

	public void setDhcpOptionVersion(String dhcpOptionVersion) {
		DhcpOptionVersion = dhcpOptionVersion;
	}

	public String getDhcpSubOptionNumber() {
		return DhcpSubOptionNumber;
	}

	public void setDhcpSubOptionNumber(String dhcpSubOptionNumber) {
		DhcpSubOptionNumber = dhcpSubOptionNumber;
	}

	public String getEnableDns4() {
		return enableDns4;
	}

	public void setEnableDns4(String enableDns4) {
		this.enableDns4 = enableDns4;
	}

	public String getEnableDns6() {
		return enableDns6;
	}

	public void setEnableDns6(String enableDns6) {
		this.enableDns6 = enableDns6;
	}

	public String getInheritDns4() {
		return inheritDns4;
	}

	public void setInheritDns4(String inheritDns4) {
		this.inheritDns4 = inheritDns4;
	}

	public String getInheritDns6() {
		return inheritDns6;
	}

	public void setInheritDns6(String inheritDns6) {
		this.inheritDns6 = inheritDns6;
	}

	public String getIpLimitfield() {
		return ipLimitfield;
	}

	public void setIpLimitfield(String ipLimitfield) {
		this.ipLimitfield = ipLimitfield;
	}

	public String getShuffleIp() {
		return shuffleIp;
	}

	public void setShuffleIp(String shuffleIp) {
		this.shuffleIp = shuffleIp;
	}

	public String getManagmentLookup() {
		return managmentLookup;
	}

	public void setManagmentLookup(String managmentLookup) {
		this.managmentLookup = managmentLookup;
	}

	public String getMinimunDelegation() {
		return minimunDelegation;
	}

	public void setMinimunDelegation(String minimunDelegation) {
		this.minimunDelegation = minimunDelegation;
	}

	public String getNumberV6Options() {
		return numberV6Options;
	}

	public void setNumberV6Options(String numberV6Options) {
		this.numberV6Options = numberV6Options;
	}

	public String getPerformDnnsUp() {
		return performDnnsUp;
	}

	public void setPerformDnnsUp(String performDnnsUp) {
		this.performDnnsUp = performDnnsUp;
	}

	public String getPrefixlenghtSize() {
		return prefixlenghtSize;
	}

	public void setPrefixlenghtSize(String prefixlenghtSize) {
		this.prefixlenghtSize = prefixlenghtSize;
	}

	public String getPriority() {
		return Priority;
	}

	public void setPriority(String priority) {
		Priority = priority;
	}

	public String getSystemPriority() {
		return systemPriority;
	}

	public void setSystemPriority(String systemPriority) {
		this.systemPriority = systemPriority;
	}

	public String getQos() {
		return qos;
	}

	public void setQos(String qos) {
		this.qos = qos;
	}

	public String getShallPerformanceUpd() {
		return shallPerformanceUpd;
	}

	public void setShallPerformanceUpd(String shallPerformanceUpd) {
		this.shallPerformanceUpd = shallPerformanceUpd;
	}

	public String getTemplateLookUpkey() {
		return templateLookUpkey;
	}

	public void setTemplateLookUpkey(String templateLookUpkey) {
		this.templateLookUpkey = templateLookUpkey;
	}

	public String getUnprovisionedDevicess() {
		return unprovisionedDevicess;
	}

	public void setUnprovisionedDevicess(String unprovisionedDevicess) {
		this.unprovisionedDevicess = unprovisionedDevicess;
	}
	
	public boolean equals(ClientClass obj) {
	    return (this == obj);
	}
	
	private String description="";
	private String lookup="";
	private String acclokupkey="";
	private String antiroaming="";
	private int clienClassEntry=0;
	private String clientDataType="";
	private String subDataType="";
	private String ddnsServer4="";
	private String ddnsServer6="";
	private String default_="";
	private String defaultPrefixlength="";
	private String defaultIpLimit="";
	private String DhcpOptionNumber="";
	private String DhcpOptionVendor="";
	private String DhcpOptionVersion="";
	private String DhcpSubOptionNumber="";
	private String enableDns4="";
	private String enableDns6="";
	private String inheritDns4="";
	private String inheritDns6="";
	private String ipLimitfield="";
	private String shuffleIp="";
	private String managmentLookup="";
	private String minimunDelegation="";
	private String numberV6Options="";
	private String performDnnsUp="";
	private String prefixlenghtSize="";
	private String Priority="";
	private String systemPriority="";
	private String qos="";
	private String shallPerformanceUpd="";
	private String templateLookUpkey="";
	private String unprovisionedDevicess="";
	private String dateType="";
	
	public String getNoDevices() {
		return noDevices;
	}

	public void setNoDevices(String noDevices) {
		this.noDevices = noDevices;
	}
	private String noDevices="";
	


	
}
