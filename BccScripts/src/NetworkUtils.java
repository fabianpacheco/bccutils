import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class NetworkUtils {

	public  boolean validIP(String ip) {
	    if (ip == null || ip.isEmpty()) return false;
	    ip = ip.trim();
	    if ((ip.length() < 6) & (ip.length() > 15)) return false;

	    try {
	        Pattern pattern = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
	        Matcher matcher = pattern.matcher(ip);
	        return matcher.matches();
	    } catch (PatternSyntaxException ex) {
	        return false;
	    }
	}
	

	 
	public  boolean isValidSubnetMask(String subnetMask) {
		if (subnetMask == null || subnetMask.isEmpty()) return false;
		subnetMask = subnetMask.trim();
	    if ((subnetMask.length() < 6) & (subnetMask.length() > 15)) return false;

	    try {
	        Pattern pattern = Pattern.compile("^(((255\\.){3}(255|254|252|248|240|224|192|128|0+))|((255\\.){2}(255|254|252|248|240|224|192|128|0+)\\.0)|((255\\.)(255|254|252|248|240|224|192|128|0+)(\\.0+){2})|((255|254|252|248|240|224|192|128|0+)(\\.0+){3}))$");
	        Matcher matcher = pattern.matcher(subnetMask);
	        return matcher.matches();
	    } catch (PatternSyntaxException ex) {
	        return false;
	    }
    }
}
