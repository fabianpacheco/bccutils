import java.io.Serializable;

/**
 * Object serializable and clonable contains all information about the rules.
 * @author Fabian Pacheco
 * @version: 31/12/2020
 *  **/
public class Rules implements Cloneable, Serializable  {

	private static final long serialVersionUID = -1617885000928014938L;
	
	private String Acllookupkey="";

	private String Allocable="";

	private String Brokenpolicy="";
	private String Creationtime="";
	private String Criteria="";
	private String Criticalhwm="";
	private String Currenthwm="";
	private String Ddnsserver="";
	private String Defaultgw="";
	private String Defaultgwlookupkey="";
	private String Defaultiplimit="";
	private String Description="";
	private String Disabled="";
	private String Disabledbyinheritance="";
	private String Donotsendhost="";
	private String Donotsendhostbyinheritance="";
	private String Enabledns="";
	private String Giaddrreservedtimeremaining="";
	private String Informonly="";
	private String Informonlybyinheritance="";
	private String Inheritdnssettings="";
	private String Ipfrom="";
	private String Iplimitselector="";
	private String Ipspoofing="";
	private String Ipspoofingbyinheritance="";
	private String Ipto="";
	private String Isgiaddrreservable="";
	private String Leasetime="";
	private String Leasetimelookupkey="";
	private String Lookupkey="";
	private String Managementlookupkey="";
	private String Modifiedtime="";
	private String Name="";
	private String Parentlookupkey="";
	private String ParentName="";
	private String Pingbefore="";
	private String Pingbeforebyinheritance="";
	private String Policylookupkey="";
	private String Policymemberlookupkey="";
	private String Requireslegalhostname="";
	private String Requireslegalhostnamebyinheritance="";
	private String Reserved="";
	private String Reservedbyinheritance="";
	private String Reservedtogiaddr="";
	private String Shuffleip="";
	private String Shuffleipbyinheritance="";
	private String Staticaddresscount="";
	private String Subnetmask="";
	private String Subnetmasklookupkey="";
	private String Subrulecount="";
	private String Suppressnakresponse="";
	private String Suppressnakresponsebyinheritance="";
	private String Templatelookupkey="";
	private String Totalhwm="";
	private String Warninghwm="";
	public Rules() {
		super();
		// TODO Auto-generated constructor stub
	}
	public void clean(){
		Name="";
		Description="";
		Lookupkey="";
		Ipfrom="";
		Ipto="";
		Isgiaddrreservable="";
		Reservedtogiaddr="";
		Giaddrreservedtimeremaining="";
		Totalhwm="";
		Currenthwm="";
		Warninghwm="";
		Criticalhwm="";
		Requireslegalhostname="";
		Requireslegalhostnamebyinheritance="";
		Donotsendhost="";
		Donotsendhostbyinheritance="";
		Disabled="";
		Disabledbyinheritance="";
		Reserved="";
		Reservedbyinheritance="";
		Pingbefore="";
		Pingbeforebyinheritance="";
		Iplimitselector="";
		Defaultiplimit="";
		Shuffleip="";
		Shuffleipbyinheritance="";
		Informonly="";
		Informonlybyinheritance="";
		Ipspoofing="";
		Ipspoofingbyinheritance="";
		Suppressnakresponse="";
		Suppressnakresponsebyinheritance="";
		Criteria="";
		Allocable="";
		Brokenpolicy="";
		Subrulecount="";
		Staticaddresscount="";
		Leasetime="";
		Leasetimelookupkey="";
		Defaultgw="";
		Defaultgwlookupkey="";
		Subnetmask="";
		Subnetmasklookupkey="";
		Enabledns="";
		Ddnsserver="";
		Inheritdnssettings="";
		Parentlookupkey="";
		ParentName="";
		Policylookupkey="";
		Policymemberlookupkey="";
		Acllookupkey="";
		Managementlookupkey="";
		Templatelookupkey="";
		Creationtime="";
		Modifiedtime="";
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		Rules clonObj = (Rules)super.clone();
		return clonObj;
	}
	public String getAcllookupkey() {
		return Acllookupkey;
	}
	public String getAllocable() {
		return Allocable;
	}
	public String getBrokenpolicy() {
		return Brokenpolicy;
	}
	public String getCreationtime() {
		return Creationtime;
	}
	public String getCriteria() {
		return Criteria;
	}
	public String getCriticalhwm() {
		return Criticalhwm;
	}
	public String getCurrenthwm() {
		return Currenthwm;
	}
	public String getDdnsserver() {
		return Ddnsserver;
	}
	public String getDefaultgw() {
		return Defaultgw;
	}
	public String getDefaultgwlookupkey() {
		return Defaultgwlookupkey;
	}
	public String getDefaultiplimit() {
		return Defaultiplimit;
	}
	public String getDescription() {
		return Description;
	}
	public String getDisabled() {
		return Disabled;
	}
	public String getDisabledbyinheritance() {
		return Disabledbyinheritance;
	}
	public String getDonotsendhost() {
		return Donotsendhost;
	}
	public String getDonotsendhostbyinheritance() {
		return Donotsendhostbyinheritance;
	}
	public String getEnabledns() {
		return Enabledns;
	}
	public String getGiaddrreservedtimeremaining() {
		return Giaddrreservedtimeremaining;
	}
	public String getInformonly() {
		return Informonly;
	}
	public String getInformonlybyinheritance() {
		return Informonlybyinheritance;
	}
	public String getInheritdnssettings() {
		return Inheritdnssettings;
	}
	public String getIpfrom() {
		return Ipfrom;
	}
	public String getIplimitselector() {
		return Iplimitselector;
	}
	public String getIpspoofing() {
		return Ipspoofing;
	}
	public String getIpspoofingbyinheritance() {
		return Ipspoofingbyinheritance;
	}
	public String getIpto() {
		return Ipto;
	}
	public String getIsgiaddrreservable() {
		return Isgiaddrreservable;
	}
	public String getLeasetime() {
		return Leasetime;
	}
	public String getLeasetimelookupkey() {
		return Leasetimelookupkey;
	}
	public String getLookupkey() {
		return Lookupkey;
	}
	public String getManagementlookupkey() {
		return Managementlookupkey;
	}
	public String getModifiedtime() {
		return Modifiedtime;
	}
	public String getName() {
		return Name;
	}
	public String getParentlookupkey() {
		return Parentlookupkey;
	}
	public String getParentName() {
		return ParentName;
	}
	public String getPingbefore() {
		return Pingbefore;
	}
	public String getPingbeforebyinheritance() {
		return Pingbeforebyinheritance;
	}
	public String getPolicylookupkey() {
		return Policylookupkey;
	}
	public String getPolicymemberlookupkey() {
		return Policymemberlookupkey;
	}
	public String getRequireslegalhostname() {
		return Requireslegalhostname;
	}
	public String getRequireslegalhostnamebyinheritance() {
		return Requireslegalhostnamebyinheritance;
	}
	public String getReserved() {
		return Reserved;
	}
	public String getReservedbyinheritance() {
		return Reservedbyinheritance;
	}
	public String getReservedtogiaddr() {
		return Reservedtogiaddr;
	}
	public String getShuffleip() {
		return Shuffleip;
	}
	public String getShuffleipbyinheritance() {
		return Shuffleipbyinheritance;
	}
	public String getStaticaddresscount() {
		return Staticaddresscount;
	}
	public String getSubnetmask() {
		return Subnetmask;
	}
	public String getSubnetmasklookupkey() {
		return Subnetmasklookupkey;
	}
	public String getSubrulecount() {
		return Subrulecount;
	}
	public String getSuppressnakresponse() {
		return Suppressnakresponse;
	}
	public String getSuppressnakresponsebyinheritance() {
		return Suppressnakresponsebyinheritance;
	}
	public String getTemplatelookupkey() {
		return Templatelookupkey;
	}

	public String getTotalhwm() {
		return Totalhwm;
	}
	public String getWarninghwm() {
		return Warninghwm;
	}
	public void setAcllookupkey(String acllookupkey) {
		Acllookupkey = acllookupkey;
	}
	public void setAllocable(String allocable) {
		Allocable = allocable;
	}
	public void setBrokenpolicy(String brokenpolicy) {
		Brokenpolicy = brokenpolicy;
	}
	public void setCreationtime(String creationtime) {
		Creationtime = creationtime;
	}
	public void setCriteria(String criteria) {
		Criteria = criteria;
	}
	public void setCriticalhwm(String criticalhwm) {
		Criticalhwm = criticalhwm;
	}
	public void setCurrenthwm(String currenthwm) {
		Currenthwm = currenthwm;
	}
	public void setDdnsserver(String ddnsserver) {
		Ddnsserver = ddnsserver;
	}
	public void setDefaultgw(String defaultgw) {
		Defaultgw = defaultgw;
	}
	public void setDefaultgwlookupkey(String defaultgwlookupkey) {
		Defaultgwlookupkey = defaultgwlookupkey;
	}
	public void setDefaultiplimit(String defaultiplimit) {
		Defaultiplimit = defaultiplimit;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public void setDisabled(String disabled) {
		Disabled = disabled;
	}
	public void setDisabledbyinheritance(String disabledbyinheritance) {
		Disabledbyinheritance = disabledbyinheritance;
	}
	public void setDonotsendhost(String donotsendhost) {
		Donotsendhost = donotsendhost;
	}
	public void setDonotsendhostbyinheritance(String donotsendhostbyinheritance) {
		Donotsendhostbyinheritance = donotsendhostbyinheritance;
	}
	public void setEnabledns(String enabledns) {
		Enabledns = enabledns;
	}
	public void setGiaddrreservedtimeremaining(String giaddrreservedtimeremaining) {
		Giaddrreservedtimeremaining = giaddrreservedtimeremaining;
	}
	public void setInformonly(String informonly) {
		Informonly = informonly;
	}
	public void setInformonlybyinheritance(String informonlybyinheritance) {
		Informonlybyinheritance = informonlybyinheritance;
	}
	public void setInheritdnssettings(String inheritdnssettings) {
		Inheritdnssettings = inheritdnssettings;
	}
	public void setIpfrom(String ipfrom) {
		Ipfrom = ipfrom;
	}
	public void setIplimitselector(String iplimitselector) {
		Iplimitselector = iplimitselector;
	}
	public void setIpspoofing(String ipspoofing) {
		Ipspoofing = ipspoofing;
	}
	public void setIpspoofingbyinheritance(String ipspoofingbyinheritance) {
		Ipspoofingbyinheritance = ipspoofingbyinheritance;
	}
	public void setIpto(String ipto) {
		Ipto = ipto;
	}
	public void setIsgiaddrreservable(String isgiaddrreservable) {
		Isgiaddrreservable = isgiaddrreservable;
	}
	public void setLeasetime(String leasetime) {
		Leasetime = leasetime;
	}
	public void setLeasetimelookupkey(String leasetimelookupkey) {
		Leasetimelookupkey = leasetimelookupkey;
	}
	public void setLookupkey(String lookupkey) {
		Lookupkey = lookupkey;
	}
	public void setManagementlookupkey(String managementlookupkey) {
		Managementlookupkey = managementlookupkey;
	}
	public void setModifiedtime(String modifiedtime) {
		Modifiedtime = modifiedtime;
	}
	public void setName(String name) {
		Name = name;
	}
	public void setParentlookupkey(String parentlookupkey) {
		Parentlookupkey = parentlookupkey;
	}
	public void setParentName(String parentName) {
		ParentName = parentName;
	}
	public void setPingbefore(String pingbefore) {
		Pingbefore = pingbefore;
	}
	public void setPingbeforebyinheritance(String pingbeforebyinheritance) {
		Pingbeforebyinheritance = pingbeforebyinheritance;
	}
	public void setPolicylookupkey(String policylookupkey) {
		Policylookupkey = policylookupkey;
	}
	public void setPolicymemberlookupkey(String policymemberlookupkey) {
		Policymemberlookupkey = policymemberlookupkey;
	}
	public void setRequireslegalhostname(String requireslegalhostname) {
		Requireslegalhostname = requireslegalhostname;
	}
	public void setRequireslegalhostnamebyinheritance(String requireslegalhostnamebyinheritance) {
		Requireslegalhostnamebyinheritance = requireslegalhostnamebyinheritance;
	}
	public void setReserved(String reserved) {
		Reserved = reserved;
	}
	public void setReservedbyinheritance(String reservedbyinheritance) {
		Reservedbyinheritance = reservedbyinheritance;
	}
	public void setReservedtogiaddr(String reservedtogiaddr) {
		Reservedtogiaddr = reservedtogiaddr;
	}
	public void setShuffleip(String shuffleip) {
		Shuffleip = shuffleip;
	}
	public void setShuffleipbyinheritance(String shuffleipbyinheritance) {
		Shuffleipbyinheritance = shuffleipbyinheritance;
	}
	public void setStaticaddresscount(String staticaddresscount) {
		Staticaddresscount = staticaddresscount;
	}
	public void setSubnetmask(String subnetmask) {
		Subnetmask = subnetmask;
	}
	public void setSubnetmasklookupkey(String subnetmasklookupkey) {
		Subnetmasklookupkey = subnetmasklookupkey;
	}
	public void setSubrulecount(String subrulecount) {
		Subrulecount = subrulecount;
	}
	public void setSuppressnakresponse(String suppressnakresponse) {
		Suppressnakresponse = suppressnakresponse;
	}
	public void setSuppressnakresponsebyinheritance(String suppressnakresponsebyinheritance) {
		Suppressnakresponsebyinheritance = suppressnakresponsebyinheritance;
	}
	public void setTemplatelookupkey(String templatelookupkey) {
		Templatelookupkey = templatelookupkey;
	}
	public void setTotalhwm(String totalhwm) {
		Totalhwm = totalhwm;
	}
	public void setWarninghwm(String warninghwm) {
		Warninghwm = warninghwm;
	}

}
